Meeting Agenda

  Date: 22/4

  Facilitator: Simone Damaschke

  Participants: Simone Damaschke, Simon Hallstr�m, Joel Severin, Anton Hallin

1. Objectives
In this meeting many issues that should have been discussed before are to be examined. 
The work of the coming weeks and the form of it is to be decided upon.

2. Reports
The first iteration went well. All goals where meet. 
The game is playable but lacks a lot of the wanted functionality.
A temporary text UI is in place. It is not a well designed UI and is to be removed shortly.

3. Discussion items
1) Testing. What does not have test? What tests are not good enough? Mock ups etc.
Some tests use other classes then the one being tested. Need to be changed so they use mockups instead.
The coverage should be checked. Simple get and set don't need tests.
2) Documentation. It is bad right now! Comments, javadock, SDD and RAD.
To little information is contained in these documents and the charts showing the current state are not done.
SDD and RAD are now to be updated every Wednesday, just before the iterations end at Thursday.
Commenting will be done asap.
3) The coming iterations. What should be done? 
5 iterations to go.
 1) GUI should work correctly. 
 Textures can be used for tiles. 
 Figures can be painted upon tiles.
 2) Figures move the right amount of tiles in one or more steps. 
 Figures attack only in their range. 
 Activefigures are changed correct. 
 Figures can not move over some terrain.
 3) Different boards that are defined in files and not in code.
 Introduce one ranged figure.
 4) Build a network option for the game so that it can be played together without hotseating.
 5) Panicked fixing of bugs and small changes like adding additional units that follow the model that already is in place.

4. Outcomes and assignments 
Anton should make a bigger board to test on and fix the board direction.
Simone should check the tests and work on them.
Simone should try and paint a ninja bunnie for the project.

5. Wrap up 
The next meeting will be held Friday, 26/4 at a time to be determined by the group during the week.

