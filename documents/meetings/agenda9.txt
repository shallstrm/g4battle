Meeting Agenda

  Date: 3/5

  Facilitator: Simon Hallström

  Participants: Simon Hallström, Joel Severin, Anton Hallin, Simone Damaschke
1. Objectives
  Discuss the last meetings resolutions, initial plans for next week

2. Reports
   Joel: Fix networking.
   Joel(, Simon): Actually limit walk range on a round basis.
   Anton: Test GameModel.
   Anton: Fix sidebar with info about tiles etc. (begun)
   Anton: Next turn button talks to GameModel through controller.
   Simone: Menu and Lobby view+controller implementation. (menu started)
   Simon: Improve file reading in map loading. (begun)
   Simon: Develop the game with new maps, figures etc. (one new fgure, without graphics)

3. Discusion Items
  Continuation of unresolved things?
  (Random Blather)
  What more needs to be done?


4. Outcomes
   Continue the last meetings resolutions, kudos to those who have started on stuff
   Joel: Fix networking.
   Joel(, Simon): Actually limit walk range on a round basis.
   Anton: Test GameModel.
   Anton: Fix sidebar with info about tiles etc. (begun)
   Anton: Next turn button talks to GameModel through controller.
   Simone: Menu and Lobby view+controller implementation. (menu started)
   Simon: Improve file reading in map loading. (begun)
   Simon: Develop the game with new maps, figures etc. (one new fgure, without graphics)