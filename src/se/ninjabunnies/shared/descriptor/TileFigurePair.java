package se.ninjabunnies.shared.descriptor;

/**
 * A <code>TileFigurePair</code> describes a tile and possible a figure,
 * and if it does - what player ID is associated with that figure.
 */
public class TileFigurePair {
	/** The tile. */
	private final TileDescriptor tileDescriptor;
	
	/** The figure "standing" on the tile, or null if none. */
	private final FigureDescriptor figureDescriptor;
	
	/** The player ID owning the figure, or undefined if no figure. */
	private final int playerId;
	
	/**
	 * Creates a <code>TileFigurePair</code> with no figure on it.
	 *
	 * @param tileDescriptor the tile associated with this pair.
	 */
	public TileFigurePair(TileDescriptor tileDescriptor) {
		this(tileDescriptor, null, 0);
	}
	
	/**
	 * Creates a <code>TileFigurePair</code> with a figure on it.
	 * 
	 * @param tileDescriptor the tile associated with this pair.
	 * @param figureDescriptor the figure associated with this pair.
	 * @param playerId the owner of the figure associated with this pair.
	 */
	public TileFigurePair(TileDescriptor tileDescriptor, FigureDescriptor
			figureDescriptor, int playerId) {
		if(tileDescriptor == null) {
			throw new NullPointerException("tileDescriptor was null");
		}
		
		this.tileDescriptor = tileDescriptor;
		this.figureDescriptor = figureDescriptor;
		this.playerId = playerId;
	}
	
	/** Needed for Kryo serialization. */
	@SuppressWarnings("unused")
	private TileFigurePair() {
		// Kryo will overwrite these...
		tileDescriptor = null;
		figureDescriptor = null;
		playerId = 0;
	}
	
	/**
	 * Gets the tile descriptor for the tile associated with this pair.
	 * 
	 * @return the <code>TileDescriptor</code>.
	 */
	public TileDescriptor getTileDescriptor() {
		return tileDescriptor;
	}

	/**
	 * Gets the figure descriptor for the tile associated with this pair,
	 * or null if the pair has no figure.
	 * 
	 * @return the <code>FigureDescriptor</code>, or null if none.
	 */
	public FigureDescriptor getFigureDescriptor() {
		return figureDescriptor;
	}

	/**
	 * Gets the player ID owning the figure in this pair, if any.
	 * 
	 * @return the player ID, if any.
	 * @throws IllegalStateException if there is no figure associated.
	 */
	public int getPlayerId() {
		if(!hasFigure()) {
			throw new IllegalStateException(
				"player id undefined for no-figure pairs");
		}
		return playerId;
	}
	
	/**
	 * Checks whether there is a figure associated with this pair or not.
	 * @return true of pair has a figure, false otherwise.
	 */
	public boolean hasFigure() {
		return figureDescriptor != null;
	}
}