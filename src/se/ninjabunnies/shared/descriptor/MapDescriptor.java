package se.ninjabunnies.shared.descriptor;



/**
 * @author Joel Severin
 * @author labbGrupp4
 * @version 0.1
 * A <code>MapDescriptor</code> describes a map with simple means (ENUM-style).
 */
public class MapDescriptor implements IMapDescriptor {
	/** A TileFigurePair representation of the map. */
	private final TileFigurePair[][] map;
	
	/** A cached width of the map. */
	private final int width;
	
	/** A cached height of the map. */
	private final int height;
	
	/** A cached player count of the number of players used in the map. */
	private final int playerCount;
	
	/**
	 * Create a new map descriptor based on the <code>TileFigurePair[][]</code>
	 * parameter <code>map</code>. The map is then validated and parsed where
	 * different properties are calculated. See {{@link MapDescriptor}}.
	 * @param map the map.
	 */
	public MapDescriptor(TileFigurePair[][] map) {
		width = map.length;
		height = map[0].length;// Will check this below...
		
		// Copy over the map, counting the number of players simultaneously.
		int playerCount = -1;
		this.map = new TileFigurePair[width][height];
		for(int x = 0; x < width; x++) {
			if(map[x].length != height) {
				throw new IllegalArgumentException("map height varies on rows");
			}
			for(int y = 0; y < height; y++) {
				TileFigurePair current = map[x][y];
				this.map[x][y] = current;
				
				// This means there is a figure belonging to a player...
				if(current.hasFigure()) {
					int playerId = current.getPlayerId();
					if(playerId > playerCount) {
						// The player with the highest ID sets the playerCount-1
						playerCount = playerId;
					}
				}
			}
		}
		
		// Add one to playerCount to account for 0-indexed player indices above.
		// playerCount started as -1, i.e. playerCount will be 0 if no figures.
		playerCount++;
		this.playerCount = playerCount;
	}
	
	/** Needed for Kryo serialization. */
	@SuppressWarnings("unused")
	private MapDescriptor() {
		// Kryo will overwrite these...
		map = null;
		width = 0;
		height = 0;
		playerCount = 0;
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public int getPlayerCount() {
		return playerCount;
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public int getWidth() {
		return width;
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public int getHeight() {
		return height;
	}
	
	/**
	 * {{@inheritDoc}
	 */
	@Override
	public TileFigurePair[][] getMap() { 
		return map;
	}
}