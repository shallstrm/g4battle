package se.ninjabunnies.shared.descriptor;


public interface IMapDescriptor {

	/**
	 * Get the number of players used in the map.
	 * @return the number of players.
	 */
	public abstract int getPlayerCount();

	/**
	 * Get the width of the map.
	 * @return the width.
	 */
	public abstract int getWidth();

	/**
	 * Get the height of the map.
	 * @return the height.
	 */
	public abstract int getHeight();

	public abstract TileFigurePair[][] getMap();

}