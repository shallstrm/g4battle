package se.ninjabunnies.shared.descriptor;

/**
 * The <code>TileDescriptor</code> enum describes the different tile types.
 */
public enum TileDescriptor {
	GRASS(0x11DD11, "GRASS"),
	WATER(0x2222FF, "WATER"),
	ROCKS(0x777777, "ROCKS"),
	MOUNTAIN(0x333333, "MOUNTAIN"),
	SAND(0x259713, "SAND"),
	DEEP_WATER(0x283747, "DEEP WATER"),
	TREE(0x837763, "TREE"),
	POND(0x824632, "POND"),
	HIGH_GRASS(0x826316, "HIGH GRASS"),
	MUD(0x266673, "MUD"),
	MOSS(0x66757, "MOSS"),
	CRYSTAL(0x62833, "CRYSTAL"),
	FLOOR(0x237462, "FLOOR");
	
	/** Color in 0xRRGGBB for RRed GGreen BBlue */
	private int color;
	
	/** Name of the tile */
	private String name;
	
	TileDescriptor(int color, String name) {
		this.color = color;
		this.name = name;
	}
	
	/**
	 * Returns the color value in 0xRRGGBB with the RRed component in bits 16-23
	 * and the GGreen in bits 8-15 and the BBlue in bits 0-7.
	 * 
	 * @return the 0xRRGGBB color <code>int</code>.
	 */
	public int getColor() {
		return color;
	}
	
	/**
	 * Return the name of the tile
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	
}