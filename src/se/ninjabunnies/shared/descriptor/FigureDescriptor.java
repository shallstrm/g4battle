package se.ninjabunnies.shared.descriptor;

import static se.ninjabunnies.shared.descriptor.TileDescriptor.*;

/**
 * The <code>FigureDescriptor</code> enum describes the different figures.
 */
public enum FigureDescriptor {
 // GRASS MUD WATER SAND
	NINJA_BUNNY("Ninja Bunny", 100, 8, WeaponDescriptor.PLOW, 2,
			new TileDescriptor[] { GRASS, WATER, POND, MUD, SAND, MOSS }), SHOOTING_SHARK(
			"Shooting Shark", 50, 3, WeaponDescriptor.TEETH, 3,
			new TileDescriptor[] { GRASS, WATER, POND, MUD, DEEP_WATER }), TANKY_TURTLE(
			"Tanky Turtle", 200, 13, WeaponDescriptor.TESLA, 1,
			new TileDescriptor[] { GRASS, MUD, SAND, WATER, POND, ROCKS }), DARKWINGED_DUCK(
			"Darkwinged Duck", 130, 3, WeaponDescriptor.SWOOP, 4,
			new TileDescriptor[] { GRASS, WATER, POND, MUD, MOUNTAIN}), CUBAN_KOALA(
			"Cuban Koala", 50, 8, WeaponDescriptor.DRUMSTICK, 2,
			new TileDescriptor[] { GRASS, MUD, WATER, POND, TREE }), HEBREW_HEDGEHOG(
			"Hebrew Hedgehog", 30, 3, WeaponDescriptor.SPORE, 1,
			new TileDescriptor[] { GRASS, WATER, POND, SAND, MUD, HIGH_GRASS});

	/**
	 * The <code>WeaponDescriptor</code> enumerates weapons that
	 * <code>FigureDescriptor</code> entries can have.
	 */
	private static enum WeaponDescriptor {
		PLOW("Fork", 2, 25, 1), TEETH("Teeth", 4, 30, 3), TESLA("Tesla Coil",
				10, 16, 1), SWOOP("Swoop", 2, 30, 2), DRUMSTICK("Drumstick", 1,
				80, 2), SPORE("Spore", 5, 10, 6);

		private final String weaponName;
		private final int weaponSpeed;
		private final int weaponDamage;
		private final int weaponRange;

		private WeaponDescriptor(String weaponName, int weaponSpeed,
				int weaponDamage, int weaponRange) {
			this.weaponName = weaponName;
			this.weaponSpeed = weaponSpeed;
			this.weaponDamage = weaponDamage;
			this.weaponRange = weaponRange;
		}
	}

	private final String name;
	private final int maxHealth;
	private final int armour;
	private final WeaponDescriptor weaponDescriptor;
	private final int movement;
	private final TileDescriptor[] traversableTiles;

	private FigureDescriptor(String name, int maxHealth, int armour,
			WeaponDescriptor weaponDescriptor, int movement,
			TileDescriptor[] traversableTiles) {
		this.name = name;
		this.maxHealth = maxHealth;
		this.armour = armour;
		this.weaponDescriptor = weaponDescriptor;
		this.movement = movement;
		this.traversableTiles = traversableTiles;
	}

	/**
	 * Get an integer of the health from the figure represented.
	 * 
	 * @return the figure health as an <code>int</code>.
	 */
	public int getMaxHealth() {
		return maxHealth;
	}

	/**
	 * Get an integer of the armour of the figure represented.
	 * 
	 * @return the figure armour as an <code>int</code>.
	 */
	public int getArmour() {
		return armour;
	}

	/**
	 * Get the name of the figures weapon.
	 * 
	 * @return the weapon name.
	 */
	public String getWeaponName() {
		return getWeaponDescriptor().weaponName;
	}

	/**
	 * Get the speen of the figures weapon.
	 * 
	 * @return the weapon speed.
	 */
	public int getWeaponSpeed() {
		return getWeaponDescriptor().weaponSpeed;
	}

	/**
	 * Get the damage inflicted by the figures weapon.
	 * 
	 * @return the weapon damage.
	 */
	public int getWeaponDamage() {
		return getWeaponDescriptor().weaponDamage;
	}

	/**
	 * Get the range of the figures weapon.
	 * 
	 * @return the weapon range.
	 */
	public int getWeaponRange() {
		return getWeaponDescriptor().weaponRange;
	}

	/**
	 * Get the movement of the figure. (How many steps it can take at once.)
	 * 
	 * @return the movement (step size).
	 */
	public int getMovement() {
		return movement;
	}

	/**
	 * Checks whether the TileDescriptor can be traversed by this
	 * FigureDescriptor. (Traverse = walk on/to, shoot over/on etc.)
	 * 
	 * @param tile
	 *            the TileDescriptor to test.
	 * @return true if traversable by figure type.
	 */
	public boolean canTraverseTile(TileDescriptor tile) {
		for (TileDescriptor hit : traversableTiles) {
			if (tile.equals(hit)) {
				return true;
			}
		}
		return false;
	}

	public String getName() {
		return name;
	}

	/**
	 * Get the weapon descriptor describing the weapon this figure descriptor
	 * describes has.
	 * 
	 * @return this figure(descriptor)s <code>WeaponDescriptor</code>.
	 */
	public WeaponDescriptor getWeaponDescriptor() {
		return weaponDescriptor;
	}
}