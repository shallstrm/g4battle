package se.ninjabunnies.shared.model;

import java.util.List;

/**
 * A read-only representation of the game model.
 * 
 * <p>It is read-only in the sense data can only be retrieved, but nothing in
 * the interface allows changing the internal state of the object in question.
 * It should however be noted that the internal state, and thus the result of
 * this class' methods, can change from one invocation to another.
 * 
 * @author Joel Severin
 */
public interface IReadonlyGameModel {
	/** The minimum number of players allowed in a game. */
	public static final int MIN_PLAYER_COUNT = 2;
	
	/**
	 * Get the team that is currently active.
	 * 
	 * @return the team that is active.
	 */
	public IReadonlyTeam getActiveTeam();
	
	/**
	 * Get the board.
	 * 
	 * @return the board.
	 */
	public IReadonlyBoard getBoard();
	
	/**
	 * Checks whether there is a winner or not. If there is a winner, there are
	 * losers too!
	 * 
	 * @return true if there is a winner.
	 */
	public boolean hasWinner();
	
	/**
	 * Get a list of the winning teams.
	 * 
	 * @return winning team, or null if there is no winner yet.
	 */
	public IReadonlyTeam getWinner();
	
	/**
	 * Get a list of the losing teams.
	 * 
	 * @return list of losing teams, or null if there is no winner yet.
	 */
	public List<? extends IReadonlyTeam> getLosers();
	
	/**
	 * Returns the active player of the active team. This result of this method
	 * is undefined if <code>this.hasWinner() == true</code>.
	 * 
	 * @return the active player (in the active team).
	 */
	public IReadonlyPlayer getActivePlayer();
	
	/**
	 * Returns the active figure, of the active player, of the active team.
	 * This result of this method is undefined if
	 * <code>this.hasWinner() == true</code>, since there is no active player
	 * then.
	 * 
	 * @return the active figure (of the active player, in the active team).
	 */
	public IReadonlyFigure getActiveFigure();
	
	/**
	 * Get a list of the teams participating in the game.
	 * 
	 * @return list of teams.
	 */
	public List<? extends IReadonlyTeam> getTeams();
}