package se.ninjabunnies.shared.model;

import se.ninjabunnies.shared.util.Position;

/**
 * A read-only representation of a board with tiles on them.
 * 
 * <p>It is read-only in the sense data can only be retrieved, but nothing in
 * the interface allows changing the internal state of the object in question.
 * It should however be noted that the internal state, and thus the result of
 * this class' methods, can change from one invocation to another.
 * 
 * @author Joel Severin
 */
public interface IReadonlyBoard {
	/** The maximum number of neighbors of a hexagon grid. */
	public static final int MAX_NEIGHBORS = 6;
	
	/**
	 * Get the width of the board.
	 * 
	 * @return the board width.
	 */
	public int getWidth();
	
	/**
	 * Get the height of the board.
	 * 
	 * @return the board height.
	 */
	public int getHeight();
	
	/**
	 * Get the tile at <code>position</code>.
	 * @param position the position of the tile to get.
	 * @return the tile at the position requested.
	 */
	public abstract IReadonlyTile getTile(Position position);

	/**
	 * Get the tile of an <code>IFigure</code> on the <code>Board</code>.
	 * 
	 * @param figure the figure to look for.
	 * @return the tile, or null if not found on the <code>Board</code>.
	 */
	public abstract IReadonlyTile getTile(IReadonlyFigure figure);
}