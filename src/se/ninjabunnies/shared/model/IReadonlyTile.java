package se.ninjabunnies.shared.model;

import java.util.List;

import se.ninjabunnies.shared.descriptor.TileDescriptor;
import se.ninjabunnies.shared.util.ICondition;

/**
 * A read-only representation of tiles. Tiles <em>can</em> have 1 figure.
 * 
 * <p>It is read-only in the sense data can only be retrieved, but nothing in
 * the interface allows changing the internal state of the object in question.
 * It should however be noted that the internal state, and thus the result of
 * this class' methods, can change from one invocation to another.
 * 
 * @author Joel Severin
 */
public interface IReadonlyTile {
	/**
	 * Returns figure standing on tile or, if there is none, null.
	 * 
	 * @return The figure standing on tile or <code>null</code>.
	 */
	public IReadonlyFigure getFigure();

	/**
	 * Checks whether the tile has a figure standing on it or not.
	 * @return if tile has a figure on it.
	 */
	public boolean hasFigure();
	
	/**
	 * Gets the neighboring tiles.
	 * @return the neighbors.
	 */
	public IReadonlyTile[] getNeighbors();
	
	/**
	 * Checks if another <code>Tile</code> is a neighbor (direct or indirect),
	 * within a given range. The path between the this and the other Tile has to
	 * meet a <code>Condition</code> (excluding start but including end node).
	 * The distance traveled is returned, or <tt>0</tt> if the other tile is not
	 * found within the allowed <code>length</code>. This is also the case if
	 * <code>this Tile</code> is the same <code>Tile</code> as the
	 * <code>other Tile</code>. The last step (to <code>other</code>) is also
	 * counted (i.e., would the tiles be direct neighbors, 1 would be returned).
	 * 
	 * <p>
	 * The <code>Tile</code>s obviously has to be members of the same
	 * <code>Board</code> to be neighbors in some way.
	 * 
	 * @param other
	 *            the other Tile.
	 * @param condition
	 *            a condition of validness to require for each Tile node between.
	 * @param length
	 *            the range (max number of Tiles in between allowed).
	 * @return the number of steps to the other tile, or 0 if impossible.
	 */
	public int getPathLength(IReadonlyTile other, int maxLength,
			ICondition<IReadonlyTile> condition);

	/**
	 * Get a list of direct neighbors, followed by their neighbors, followed by
	 * a ring of other neighbors. Neighbors that have already been listed, will
	 * not be listed again. That creates the shape of "rings" sandwiched on
	 * each other in the result set.
	 * 
	 * @param condition
	 *        a condition to require for neighbors. This condition will affect
	 *        other neighbors as well; if there is no way to them through tiles
	 *        meeting the condition, whey will be excluded from the result set.
	 *        
	 * @param length
	 *        the number of rings to return. See <strong>return</strong>.
	 *        Note that there might be less rings returned, if they get empty...
	 * 
	 * @return a list of rings, with each ring containing a list of tiles, which
	 *         is counted as being part of that ring. A tile only occurs once.
	 *         <code>this</code> tile is not included, as it is not a neighbor.
	 */
	public List<List<? extends IReadonlyTile>> getNeighborRings(
			ICondition<IReadonlyTile> condition, int length);
	
	/**
	 * @return the tileDescriptor this tile came from.
	 */
	public TileDescriptor getTileDescriptor();
}