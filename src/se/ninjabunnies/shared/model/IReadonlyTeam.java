package se.ninjabunnies.shared.model;

import java.util.List;

/**
 * A read-only representation of a team (having a set of players).
 * 
 * <p>It is read-only in the sense data can only be retrieved, but nothing in
 * the interface allows changing the internal state of the object in question.
 * It should however be noted that the internal state, and thus the result of
 * this class' methods, can change from one invocation to another.
 * 
 * @author Joel Severin
 */
public interface IReadonlyTeam {
	/**
	 * Get the current player in the player rotation.
	 * 
	 * @return the current player.
	 */
	public IReadonlyPlayer getCurrentPlayer();

	/**
	 * Returns all the players in this team.
	 * 
	 * @return a List of all Players.
	 */
	public List<? extends IReadonlyPlayer> getPlayers();

	/**
	 * Returns the number of players in the team.
	 * 
	 * @return the player count.
	 */
	public int getPlayerCount();

	/**
	 * Returns true if at least one player in the team is active.
	 * 
	 * @return true if the team is active, false otherwise.
	 */
	public boolean isActive();
	
	/**
	 * Returns the name of the team.
	 * 
	 * @return the team name.
	 */
	public String getName();
}