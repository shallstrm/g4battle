package se.ninjabunnies.shared.model;

import se.ninjabunnies.shared.util.ColorGenerator.ColorDescriptor;

/**
 * A read-only representation of a player (belonging to a team).
 * 
 * <p>It is read-only in the sense data can only be retrieved, but nothing in
 * the interface allows changing the internal state of the object in question.
 * It should however be noted that the internal state, and thus the result of
 * this class' methods, can change from one invocation to another.
 * 
 * @author Joel Severin
 */
public interface IReadonlyPlayer {
	/**
	 * Get the figure currently active.
	 * 
	 * @return active figure.
	 */
	public IReadonlyFigure getActiveFigure();

	/**
	 * Get the player's name.
	 * 
	 * @return Players name.
	 */
	public String getName();

	/**
	 * Get the player's corresponding team.
	 * 
	 * @return Players team.
	 */
	public IReadonlyTeam getTeam();
	
	/**
	 * Returns true if the player is still active in the game.
	 * A player that is inactive has lost all of its players.
	 * 
	 * @return true if this player is active, false otherwise.
	 */
	public boolean isActive();
	
	/**
	 * Checks whether all the <code>Figure</code>s belonging to a player is used.
	 * 
	 * @return true if all the player's figures are used.
	 */
	public boolean isAllFiguresUsed();
	
	/**
	 * Get the color descriptor representing the player.
	 * 
	 * @return The <code>ColorDescriptor</code> representing the player.
	 */
	public ColorDescriptor getColor();
	
	/**
	 * Gets the starting position the player will have when spawned on a map.
	 * @return the zero-based position index requested. -1 means unset.
	 */
	public int getStartingPosition();
}