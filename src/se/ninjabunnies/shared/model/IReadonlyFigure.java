package se.ninjabunnies.shared.model;

import se.ninjabunnies.shared.descriptor.FigureDescriptor;
import se.ninjabunnies.shared.util.ICondition;

/**
 * A read-only representation of a figure.
 * 
 * <p>It is read-only in the sense data can only be retrieved, but nothing in
 * the interface allows changing the internal state of the object in question.
 * It should however be noted that the internal state, and thus the result of
 * this class' methods, can change from one invocation to another.
 * 
 * @author Joel Severin
 */
public interface IReadonlyFigure {
	/**
	 * Gets the figures health.
	 * <!-- @todo is this current or start health? -->
	 * 
	 * @return the health.
	 */
	public IImmutableHealth getHealth();

	/**
	 * Gets the figures weapon. Figures only have one weapon, that is fixed.
	 * 
	 * @return the weapon.
	 */
	public IImmutableWeapon getWeapon();

	/**
	 * Returns the current armour of the figure.
	 * 
	 * @return the armour.
	 */
	public int getArmour();


	/**
	 * Returns how many steps a figure can take on a new round.
	 * 
	 * @return the maximum steps per round.
	 */
	public int getMovement();

	/**
	 * @return if the figure is used.<br>
	 *         false = ready to be used.<br>
	 *         true = used  this round, has to rest.
	 */
	public boolean isUsed();
	
	/**
	 * @return the player owning this figure.
	 */
	public IReadonlyPlayer getPlayer();
	
	/**
	 * Checks whether the figure is still alive.
	 * @return true if figure alive.
	 */
	public boolean isAlive();
	
	/**
	 * Returns true only if the IFigure belongs to a Player in <code>team</code>
	 * 
	 * @param team the team to check.
	 * @return true if in team.
	 */
	public boolean inTeam(IReadonlyTeam team);
	
	/**
	 * @return the distance the target can still move this turn.
	 */
	public int distanceAbleToMove();
	
	/**
	 * Returns true if the figure can launch attacks between the two Tiles.
	 * 
	 * @param first the first tile.
	 * @param second the second tile.
	 * @return true if attacks can happen between the tiles, false otherwise.
	 */
	public boolean canAttackBetween(IReadonlyTile first, IReadonlyTile second);

	/**
	 * Returns the number of tiles to traverse to be able to move from
	 * <code>first</code> to </code>second</code>, or <tt>0</tt> if impossible.
	 * 
	 * @param first the first tile.
	 * @param second the second tile.
	 * @return move path length, or 0 if not possible to move there.
	 */
	public int getMovePathLength(IReadonlyTile first,
			IReadonlyTile second);
	
	/**
	 * Returns true if the <code>IFigure</code> can traverse the
	 * <code>ITile</code> (based on figure type and tile types).
	 * 
	 * @param tile the <code>ITile</code> to test.
	 * @return true if <code>ITile</code> is traversable by <code>this</code>.
	 */
	public boolean canTraverse(IReadonlyTile tile);
	
	/**
	 * Returns the FigureDescriptor associated with this IFigure.
	 * 
	 * @return returns the figure descriptor.
	 */
	public FigureDescriptor getFigureDescriptor();
	
	/**
	 * Traverse condition (to which tiles can the figure move to).
	 * Used when rendering.
	 * @return the condition
	 */
	public ICondition<IReadonlyTile> getMoveCondition();
}
