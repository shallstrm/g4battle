package se.ninjabunnies.shared.model;

/**
 * An immutable instance of a weapon, describing it's properties.
 */
public interface IImmutableWeapon {
	/**
	 * @return the weaponName
	 */
	public abstract String getWeaponName();

	/**
	 * @return the weaponDamage
	 */
	public abstract int getWeaponDamage();

	/**
	 * @return the weaponSpeed
	 */
	public abstract int getWeaponSpeed();

	/**
	 * @return the weaponRange
	 */
	public abstract int getWeaponRange();
}