package se.ninjabunnies.shared.model;

public interface IImmutableHealth {
	/**
	 * @return the currentHealth
	 */
	public abstract int getCurrentHealth();

	/**
	 * @return the maxHealth
	 */
	public abstract int getMaxHealth();
}