package se.ninjabunnies.shared.changebus;

/**
 * The readable part of a change bus.
 * 
 * @see IChangeBus
 * @author Joel Severin
 */
public interface IReadableChangeBus {
	/**
	 * Receive a list of changes, consisting of the smallest possible change set
	 * (a smaller one would mean a broken change transaction representing
	 *  half-done, broken data). 
	 *  
	 * @return a change set with one valid transaction, or null if none.
	 */
	public IChangeSet receiveOne();
	
	/**
	 * Get a big change set consisting of all the small changes in receiveOne().
	 * 
	 * @return a change set of everything (buffered on the bus), or null if none.
	 */
	public IChangeSet receiveAll();
}