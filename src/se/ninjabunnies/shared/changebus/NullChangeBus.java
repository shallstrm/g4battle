package se.ninjabunnies.shared.changebus;

/**
 * The NullChangeBus does nothing. It can neither be read, nor written to.
 * Everything just gets discarded into the void.
 * 
 * @author Joel Severin
 */
public class NullChangeBus implements IChangeBus {
	/**
	 * Does nothing.
	 */
	@Override
	public void setChangeSetBarrier() {
	}

	/**
	 * Always returns null.
	 */
	@Override
	public IChangeSet receiveAll() {
		return null;
	}

	/**
	 * Does nothing.
	 */
	@Override
	public void send(IChange change) {
	}

	/**
	 * Always returns null.
	 */
	@Override
	public IChangeSet receiveOne() {
		return null;
	}
}