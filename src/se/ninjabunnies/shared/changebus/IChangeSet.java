package se.ninjabunnies.shared.changebus;

import java.util.Collection;

/**
 * A change set represents a batch of changes that make up a transaction. This
 * sets a clear barrier where data is valid and where it is corrupt due to
 * timing issued.
 * 
 * <p>An example would be a system where a must be equal to b:<br>
 * <pre>a = false
 * b = false
 * ChangeSet {
 *  Change: a = true
 *  Change: b = true
 * }</pre><br>
 * Thus, the changes must be encapsulated in a change set, since b is invalid
 * the time between a is set and b is still false.
 * 
 * @author Joel Severin
 */
public interface IChangeSet extends Collection<IChange> {
}