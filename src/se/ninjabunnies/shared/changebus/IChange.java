package se.ninjabunnies.shared.changebus;

/**
 * Holds some sort of change in a bus system defined by its context.
 * 
 * @author Joel Severin
 */
public interface IChange {
}