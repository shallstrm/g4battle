package se.ninjabunnies.shared.changebus;

import java.util.LinkedList;
import java.util.Queue;

/**
 * A change bus acting as a buffer to receive changes and then push them out
 * as a transaction-like change set.
 * 
 * @author Joel Severin
 */
public class ChangeSetChangeBus implements IChangeBus {
	/**
	 * A change set queue consisting of valid change sets (not the
	 * lastChangeSet, which is not considered valid yet since it's being built).
	 */
	private Queue<IChangeSet> changeSets = new LinkedList<IChangeSet>();
	
	/** The change set being built right now in the end of the queue. Invalid */
	private IChangeSet lastChangeSet = new SimpleChangeSet();
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void send(IChange change) {
		lastChangeSet.add(change);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IChangeSet receiveOne() {
		// Poll = pop the first off.
		return changeSets.poll();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IChangeSet receiveAll() {
		SimpleChangeSet bigChangeSet = new SimpleChangeSet();
		
		// While there are things in the queue...
		IChangeSet current;
		while((current = changeSets.poll()) != null) {
			// ...append to the big returned set.
			bigChangeSet.addAll(current);
		}
		
		if(bigChangeSet.size() > 0) {
			return bigChangeSet;
		} else {
			// Contract says this.
			return null;
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setChangeSetBarrier() {
		// Make the current valid and create a new "invalid" being built...
		changeSets.add(lastChangeSet);
		lastChangeSet = new SimpleChangeSet();
	}
}