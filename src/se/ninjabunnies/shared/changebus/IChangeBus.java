package se.ninjabunnies.shared.changebus;

/**
 * The change bus can be written to with changes, and read from in a list of
 * changes (a change set). Changes can be dropped or modified by implementations
 * for optimization (merging changes together) or kept unmodified for
 * consistency/full rollback support.
 * 
 * @author Joel Severin
 */
public interface IChangeBus extends IWritableChangeBus, IReadableChangeBus {
	/**
	 * Send a change to the bus.
	 * 
	 * @param change the change to send.
	 */
	public void send(IChange change);
	
	/**
	 * Receive a list of changes, consisting of the smallest possible change set
	 * (a smaller one would mean a broken change transaction representing
	 *  half-done, broken data).
	 *  
	 * @return a change set with valid a change transaction.
	 */
	public IChangeSet receiveOne();
	
	//public IChangeSet receiveAll();
}