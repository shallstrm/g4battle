package se.ninjabunnies.shared.changebus;

/**
 * The writable part of a change bus.
 * 
 * @see IChangeBus
 * @author Joel Severin
 */
public interface IWritableChangeBus {
	/**
	 * Send a change to the bus.
	 * 
	 * @param change the change to send.
	 */
	public void send(IChange change);
	
	/**
	 * Creates a change set barrier, i.e. make changes queued up until now a
	 * new change set.
	 */
	public void setChangeSetBarrier();
}
