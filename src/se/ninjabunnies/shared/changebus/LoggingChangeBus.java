package se.ninjabunnies.shared.changebus;

import java.io.PrintStream;

/**
 * PrintStream writing bus that logs everything written to a change bus.
 * Notably, System.out is a PrintStream. But you wouldn't, would you? ;)
 * 
 * @author Joel Severin
 */
public class LoggingChangeBus implements IWritableChangeBus {
	/** A name to log for this bus context. */
	private final String contextName;
	
	/** The log stream to write to. */
	private final PrintStream log;
	
	/**
	 * Construct a new logging change bus.
	 * 
	 * @param contextName a (short) name describing the context.
	 * @param log the PrintStream to log on.
	 */
	public LoggingChangeBus(String contextName, PrintStream log) {
		this.contextName = contextName;
		this.log = log;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void send(IChange change) {
		log.println("[CBus " + contextName + "].send: " + change.toString());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setChangeSetBarrier() {
		log.println("[CBus " + contextName + "].setChangeSetBarrier");
	}
}