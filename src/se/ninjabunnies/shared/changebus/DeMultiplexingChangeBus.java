package se.ninjabunnies.shared.changebus;

import java.util.ArrayList;
import java.util.List;

/**
 * A de-multiplexing change bus splits a change bus into 0 or more ones. This
 * simple one just pushes incoming Changes out to all receivers.
 * 
 * @author Joel Severin
 */
public class DeMultiplexingChangeBus implements IWritableChangeBus {
	/** The 0 or more busses that will be mux:ed to. */
	private List<IWritableChangeBus> receivers =
			new ArrayList<IWritableChangeBus>();
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void send(IChange change) {
		for(IWritableChangeBus bus : receivers) {
			bus.send(change);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setChangeSetBarrier() {
		for(IWritableChangeBus bus : receivers) {
			bus.setChangeSetBarrier();
		}
	}
	
	/**
	 * Add a receiving bus that should listen on this bus. Adding no receivers
	 * results in a null-bus.
	 * 
	 * @param bus the change bus to add.
	 */
	public void addReceiver(IWritableChangeBus bus) {
		receivers.add(bus);
	}
}