package se.ninjabunnies.shared.changebus;

import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

/**
 * A simple class that can hold a list of changes. It includes a naive approach
 * to holding the changes. Changes are preserved (i.e. not merged).
 * 
 * @author Joel Severin
 */
public class SimpleChangeSet extends AbstractCollection<IChange>
		implements IChangeSet {
	private Collection<IChange> internal = new ArrayList<IChange>();
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Iterator<IChange> iterator() {
		return Collections.unmodifiableCollection(internal).iterator();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int size() {
		return internal.size();
	}
	
	/**
	 * Add (append) the change to the ChangeSet.
	 * 
	 * @return if it was indeed added (always true).
	 */
	@Override
	public boolean add(IChange change) {
		return internal.add(change);
	}
}