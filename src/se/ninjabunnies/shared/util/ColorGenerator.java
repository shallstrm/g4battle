package se.ninjabunnies.shared.util;

/**
 * A class to generate a color for players
 * @author Anton
 *
 */
public class ColorGenerator {
	/**
	 * Enum describing a color with an ID and an RGB component.
	 * 
	 * <!-- @todo Why do we even have an ID?
	 *            Just kept it while refactoring... /Joel -->
	 * 
	 * @author Joel
	 */
	public enum ColorDescriptor {
		BLUE(0, 0x0000FF),
		RED(1, 0xFF0000),
		ORANGE(2, 0xFF9900),
		CYAN(3, 0x99FFFF);
		
		/** The unique color ID */
		private int id;
		
		/**
		 * The rgb value of this color, with each component occupying one byte,
		 * like so: 0xRRGGBB.
		 */
		private int rgb;
		
		private ColorDescriptor(int id, int rgb) {
			this.id = rgb;
			this.rgb = rgb;
		}
		
		/**
		 * Get the color value in RGB, with each color component occupying one
		 * byte. The value will be in the 0xRRGGBB format (same as AWT Color).
		 * 
		 * @return the 0xRRGGBB int of this color.
		 */
		public int getRgb() {
			return rgb;
		}
		
		// @todo what is this?!
		public int getId() {
			return id;
		}
	}
	
	/**
	 * Return a color when given a number
	 * @param the given number
	 * @return the generated color
	 */
	public static ColorDescriptor generateColor(int count) {
		return ColorDescriptor.values()[count];
	}
}