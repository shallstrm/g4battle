package se.ninjabunnies.shared.util;
import java.awt.Image;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;


import javax.imageio.ImageIO;
/**
 * A singleton class to manage resources, such as images.
 * @author Anton
 *
 */
public class ResourceManager {
	
	// The one instance
	private static ResourceManager instance = null;
	
	// First part of the path
	private static final String RESOURCE_PREFIX = "resources/";

	/** The path where all themes will be found, including trailing slash! */
	private static final String THEME_PATH = "themes/";
	
	// HashMap to assort images
	private Map<String, Image> imageCache = new HashMap<String, Image>();
	
	/** If a custom theme is chosen, the theme is non-null. */
	private String theme = null;
	
	// Private constructor
	private ResourceManager() {
	}
	
	/**
	 * Singleton method
	 * @return The one instance
	 */
	public static ResourceManager getInstance() {
		if(instance == null) {
			instance = new ResourceManager();
		}
		return instance;
	}
	
	/**
	 * Apply a new theme. Themes will be found in THEME_PATH/<em>theme</em>.
	 * Note that aldready loaded images will not change, it must be reloaded.
	 * 
	 * @param theme the theme name, or null if none.
	 */
	public void setTheme(String theme) {
		this.theme = theme;
		flushCaches();
	}
	
	/**
	 * 
	 * @return the current theme
	 */
	public String getTheme() {
		return theme;
	}
	
	/**
	 * Flush all internal caches (since they are no longer valid/other reason).
	 */
	private void flushCaches() {
		imageCache.clear();
	}

	/**
	 * Obtains file from resource folder
	 * @param filename Name of the wanted file
	 * @return The wanted file
	 */
	public File getFileResource(String filename) {
		// Try finding the image in the current theme.
		if(theme != null) {
			File file = new File(RESOURCE_PREFIX
					+ THEME_PATH + theme + "/" + filename);
			if(file.exists()) {
				return file;
			}
		}
		
		// Default
		return new File(RESOURCE_PREFIX + filename);
	}
	
	/**
	 * Obtains images from resource folder
	 * @param filename Name of the wanted image
	 * @return The wanted image
	 */
	public Image getImageResource(String filename) {
		Image image = imageCache.get(filename);
		
		// No image found, load it.
		if(image == null) {
			try {
					image = ImageIO.read(getFileResource(filename));
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			if(image != null) {
				imageCache.put(filename, image);
			} else {
				throw new IllegalStateException("image could not be loaded");
			}
		}
		
		return image;
	}
	/**
	 * 
	 * @return an array containing the names of all maps
	 * grabbing one string from the array and passing it to getMapString should return a valid mapString
	 */
	public String[] getMapNames() {
		File mapFolder = new File(RESOURCE_PREFIX + "definedmaps/");
		File[] mapList = mapFolder.listFiles();
		List<String> mapStringList = new ArrayList<String>();
			for (File map : mapList) {
				mapStringList.add(map.getName());
			}
		return mapStringList.toArray(new String[mapStringList.size()]);
	}
	/**
	 * A method to obtain the mapString from inside the mapfile 
	 * @param mapFile the name of the map, has to be the filename of a file in /definedmaps/
	 * @return the content of the file, in a String format
	 * @throws IOException if the file is not available 
	 */

	public String getMapString(String mapName) throws IOException {
        Scanner s = null;
        StringBuilder content = new StringBuilder();
        try {
            s = new Scanner(new BufferedReader(new FileReader(RESOURCE_PREFIX + "definedmaps/" + mapName)));
            s.useDelimiter(System.getProperty("line.separator"));

            while (s.hasNext()) {
                content.append(s.next());
            }
        } finally {
            if (s != null) {
                s.close();
            }
        }
		return content.toString();
		
	}
}