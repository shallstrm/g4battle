package se.ninjabunnies.shared.util;

import java.util.ArrayList;
import java.util.List;

import se.ninjabunnies.shared.descriptor.FigureDescriptor;
import se.ninjabunnies.shared.descriptor.TileDescriptor;
import se.ninjabunnies.shared.descriptor.TileFigurePair;

public class MapDescriptorTemplate {
	private String mapName;
	private int possiblePlayers;
	private ArrayList<ArrayList<TileFigurePair>> tileList;
	private int addingIndex;
	private String description;
	private ArrayList<ArrayList<TileFigurePair>> playerTileList;
	/*
	 * 
	 * @param mapName
	 * Creates an inital MapDescriptorTemplate with the given name. It does not createa whole template
	 * it needs to be fleshed out with other methods
	 */
	private MapDescriptorTemplate(String mapName) {
		this.mapName = mapName;
		tileList = new ArrayList<ArrayList<TileFigurePair>>();
		tileList.add(new ArrayList<TileFigurePair>());
		addingIndex = 0;
		playerTileList = new ArrayList<ArrayList<TileFigurePair>>();
	}
	
	/**
	 * Defines how many players that can play this map.
	 * @param possiblePlayers
	 */
	public void setPossiblePlayers(int possiblePlayers) {
		this.possiblePlayers = possiblePlayers;
		for (int i = 0; possiblePlayers > i; i++) {
			playerTileList.add(new ArrayList<TileFigurePair>());
		}
	}
	
	/**
	 * Sets the description of the map.
	 * @param description the description of the map
	 */
	private void setMapDescription(String description) {
		this.description = description;
	}
	/**
	 * Returns the amount of players that can play this map.
	 * @return the amount of players that can play this map
	 */
	public int getPossiblePlayers() {
		return possiblePlayers;
	}
	/**
	 * Returns the amount of tiles that the field is made out of.
	 * @return the amount of tiles that the field is made out of
	 */
	public int getAmountOfTiles() {
		return tileList.size() * tileList.get(0).size();
	}
	
	/*
	 * 
	 * @param TFP adds a new TileFigurePair to the field, if the column which it's
	 *  going to be added to doesn't exist, it creates a new one
	 */
	private void addTileFigurePair(TileFigurePair TFP) {
		if (tileList.size() == (addingIndex)) {
			tileList.add(new ArrayList<TileFigurePair>()); }
			tileList.get(addingIndex).add(TFP); 
	}
	
	/*
	 * resets the counter which counts the column that tiles is added to
	 */
	private void resetAddingIndex() {
		addingIndex = 0;
	}
	
	/*
	 * Changes column that tiles get added to
	 */
	private void nextColumn() {
		addingIndex = addingIndex + 1;
	} 
	
	/**
	 * 
	 * @return the name of the map
	 */
	public String getMapName() {
		return mapName;
	}
	
	/** 
	 * Converts the whole field into a format the the common MapDescriptor can handle properly
	 * 
	 * @return a matrix containing the whole game field, can simply be given to MapDescriptor to get a new map
	 */
	public TileFigurePair[][] getMapDescriptorCompatibleFormat() {
		List<TileFigurePair[]> tileFigurePairArrayHolder = new ArrayList<TileFigurePair[]>();
		for(List<TileFigurePair> tileFigurePairListIterator: tileList) {
			TileFigurePair[] tileFigurePairArray = (TileFigurePair[])tileFigurePairListIterator.
					toArray(new TileFigurePair[tileFigurePairListIterator.size()]);
			tileFigurePairArrayHolder.add(tileFigurePairArray);
		}
		TileFigurePair[][] TFPM = (TileFigurePair[][]) tileFigurePairArrayHolder.toArray
				(new TileFigurePair[tileFigurePairArrayHolder.size()][0]);
		return TFPM	;
		
	}
	
	/**
	 *
	 * @return a description of the map
	 */
	public String getMapDescription() {
		return description;
	}
	@Override
	public String toString() {
		return "MapDescriptorTemplate [mapName=" + mapName
				+ ", possiblePlayers=" + possiblePlayers + ", tileList="
				+ tileList +  " addingIndex="
				+ addingIndex + "]";
	}
	
	/**
	 * 
	 * @param mapstring is a String, loaded from a .map file, which follows a defined format
	 * @return a MapDescriptorTemplate based on the string.
	 * Ideally, the template will, with little trouble, be converted to a MapDescriptor 
	 */
	public static MapDescriptorTemplate stringToMapParser(String mapstring) {
		mapstring = mapstring.replace("\r" , "");  //trims the windows line breaks, as those are added for readability
		mapstring = mapstring.replace("\n", ""); //trims the unix line breaks, git doesn't seem to replace them with local ones properly
		String[] stringHolder = mapstring.split(";"); //splits the string into lines
		MapDescriptorTemplate MDR = new MapDescriptorTemplate(stringHolder[0]); //first line is the mapname
		MDR.setMapDescription(stringHolder[1]);
		String tempString = stringHolder[2].replace("players", "");  //second is player
		MDR.setPossiblePlayers(Integer.parseInt(tempString.replace(" ", ""))); //tells the template how many players there are
		for (int i = 3; stringHolder.length > i; i++ ) {
			String[] mapRow = stringHolder[i].split(", "); //splits each line into tile groups, with a tile and maybe figure + player
			for (String tile : mapRow) {
				String tileStats[] = tile.trim().split(" "); //splits the tile, into tile figure and player
					
				if (tileStats.length == 1) { //enters if tile didn't hold a figure and player, creates a tile
					MDR.addTileFigurePair(new TileFigurePair(TileDescriptor.valueOf(tileStats[0])));
					MDR.nextColumn();
				} else if (tileStats.length == 3) { //creates a tile with a tile that belongs to a playerID
					MDR.playerTileList.get(Integer.parseInt(tileStats[2].replace("player", ""))).
					add((new TileFigurePair(TileDescriptor.valueOf(tileStats[0]), 
							FigureDescriptor.valueOf(tileStats[1]),
							Integer.parseInt(tileStats[2].replace("player", "")))));
					MDR.addTileFigurePair(new TileFigurePair(TileDescriptor.valueOf(tileStats[0]), 
											FigureDescriptor.valueOf(tileStats[1]),
											Integer.parseInt(tileStats[2].replace("player", ""))));
					MDR.nextColumn();
				}
			}
			if (stringHolder.length > (i+1)) { //if there's going to be a next row, adds one
			MDR.resetAddingIndex(); }
		}
		return MDR;
	}
	
	/**
	 * a method to ensure that a given player is not playable when a map is started
	 * a player who is removePlayer-ed will not appear on the map
	 * @param the ID of the player to init
	 * 
	 */
	public void removePlayer(int playerID)
	{
		
	}
}
