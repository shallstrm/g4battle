package se.ninjabunnies.shared.util;

/**
 * A <code>Condition</code> for type <code>T</code>
 *  sets some kind of
 * restrictions on <code>T</code> that can be 
 * met or not.
 * 
 * @author Joel Severin
 * @param <T> the type the condition can be 
 * applied on.
 */
public interface ICondition<T> {
	/**
	 * Checks whether this condition is met for
	 *  <code>object</code>.
	 * @param object the object to be tested.
	 * @return true if object conforms to this
	 *  conditions, false otherwise.
	 */
	public boolean isMetFor(T object);
}
