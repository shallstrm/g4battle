package se.ninjabunnies.shared.util;

/**
 * Immutable class describing integer 2D-points.
 * 
 */
public class Position {
	private final int x;
	private final int y;

	/**
	 * Creates an immutable object with coordinates.
	 */
	public Position(final int x, final int y) {
		this.x = x;
		this.y = y;
	}
	
	/** Must be here for Kryo serialization. */
	@SuppressWarnings("unused")
	private Position() {
		// Kryo overwrites these.
		x = 0;
		y = 0;
	}

	/**
	 * @return The x value of the coordinate.
	 */
	public int getX() {
		return this.x;
	}

	/**
	 * @return The x value of the coordinate.
	 */
	public int getY() {
		return this.y;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		Position other = (Position) obj;
		return this.x == other.x && this.y == other.y;
	}
	
	@Override
	public String toString() {
		return "(" + x + ", " + y + ")";
	}
}
