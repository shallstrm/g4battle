package se.ninjabunnies.shared.net;

import se.ninjabunnies.server.model.Board;
import se.ninjabunnies.server.model.Figure;
import se.ninjabunnies.server.model.GameModel;
import se.ninjabunnies.server.model.Health;
import se.ninjabunnies.server.model.Player;
import se.ninjabunnies.server.model.Team;
import se.ninjabunnies.server.model.Tile;
import se.ninjabunnies.server.model.Weapon;
import se.ninjabunnies.server.controller.GameServerController.Game.ServerChangeBus;
import se.ninjabunnies.shared.changebus.SimpleChangeSet;
import se.ninjabunnies.shared.descriptor.FigureDescriptor;
import se.ninjabunnies.shared.descriptor.MapDescriptor;
import se.ninjabunnies.shared.descriptor.TileDescriptor;
import se.ninjabunnies.shared.descriptor.TileFigurePair;
import se.ninjabunnies.shared.net.state.EndTurnRequest;
import se.ninjabunnies.shared.net.state.FigureHealthChange;
import se.ninjabunnies.shared.net.state.GameModelValid;
import se.ninjabunnies.shared.net.state.LegacyGameModelSync;
import se.ninjabunnies.shared.net.state.LegacyMapSetup;
import se.ninjabunnies.shared.net.state.LegacyTeamSetup;
import se.ninjabunnies.shared.net.state.TileActionPerformed;
import se.ninjabunnies.shared.util.ColorGenerator;
import se.ninjabunnies.shared.util.Position;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.serializers.FieldSerializer;
import com.esotericsoftware.kryonet.EndPoint;

/**
 * Class for coordinating networking setup and resources.
 * 
 * @author Joel Severin
 */
public class NinjaBunniesNetwork {
	/** The default port Ninja Bunnies servers run on. */
	public static final int DEFAULT_PORT = 1337;

	/** The networking update rate, in milliseconds. 1/20 = 50 ms. */
	private static int NETWORK_UPDATE_RATE = 50;
	
	/** The client timeout time on connect, ping timeout etc. In ms. */
	private static int CLIENT_TIMEOUT = 5000;
	
	/**
	 * Initialize networking for the Kryo instance used by the KryoNet endPoint.
	 * 
	 * @param endPoint the KryoNet EndPoint to setup.
	 */
	public static void setupNetworkFor(EndPoint endPoint) {
		Kryo kryo = endPoint.getKryo();
		
		//Log.set(Log.LEVEL_TRACE);
		
		// We run this since we have legacy object transfers right now...
		//kryo.setRegistrationRequired(false);
		kryo.setReferences(true);// Don't crash on cyclic dependancies! :S
		
		registerKryoClasses(kryo);
	}
	
	public static void registerKryoClasses(Kryo kryo) {
		// You can do profilation of this by:
		/*
		Log.set(Log.LEVEL_TRACE);
		Kryo kryo = new Kryo();
		kryo.setRegistrationRequired(true);
		NinjaBunniesNetwork.registerKryoClasses(kryo);
        Output output = new Output(new byte[10000000]);
        kryo.writeClassAndObject(output, gameModel);
        System.out.println("Size = " + output.position());
        */
		
		// If adding classes below, be sure to NOT change any number. Rather,
		// use new integers. Don't re-use old ones. Read the docs. if unsure!
		// Oh, btw, ID 0-8 (and possible more) seems to be taken by KryoNet.
		// Totally undocumented...
		
		kryo.register(GameModel.class, 100);
		kryo.register(Board.class, 101);
		kryo.register(Figure.class, 102);
		kryo.register(Tile.class, 103);
		kryo.register(Tile[].class, 115);
		kryo.register(Tile[][].class, 116);
		kryo.register(Team.class, 104);
		kryo.register(Player.class, 105);
		kryo.register(Weapon.class, 106);
		kryo.register(Health.class, 107);
		kryo.register(FigureDescriptor.class, 108);
		kryo.register(MapDescriptor.class, 109);
		kryo.register(TileDescriptor.class, 110);
		kryo.register(TileFigurePair.class, 111);
		kryo.register(TileFigurePair[].class, 112);
		kryo.register(TileFigurePair[][].class, 113);
		kryo.register(Position.class, 114);
		kryo.register(ColorGenerator.ColorDescriptor.class, 117);
		
		// This is some code that registers the AWT Color class, if needed again
		// Register the AWT Color class, which cannot be serialized by default.
		//kryo.register(Color.class, 200);
		//kryo.register(Color.class, new Serializer<Color>() {
		//	@Override
		//    public void write(Kryo kryo, Output output, Color object) {
		//            output.writeInt(object.getRGB());
		//    }
	
		//	@Override
		//    public Color read(Kryo kryo, Input input, Class<Color> type) {
		//            return new Color(input.readInt(), true);
		//    }
		//});
		
		// Just remove ServerChangeBus fields... Hack hack.
		FieldSerializer<ServerChangeBus> removeServerChangeBusLeaks =
				new FieldSerializer<ServerChangeBus>(kryo,
						ServerChangeBus.class);
		removeServerChangeBusLeaks.removeField("out");
		kryo.register(ServerChangeBus.class, removeServerChangeBusLeaks, 201);
		
		kryo.register(SimpleChangeSet.class, 202);
		
		kryo.register(LegacyMapSetup.class, 300);
		kryo.register(LegacyTeamSetup.class, 301);
		kryo.register(GameModelValid.class, 302);
		kryo.register(LegacyGameModelSync.class, 303);
		kryo.register(TileActionPerformed.class, 304);
		kryo.register(FigureHealthChange.class, 305);
		kryo.register(EndTurnRequest.class, 306);
		
		kryo.register(java.util.ArrayList.class, 400);
	}
	
	/** Returns the current update rate of the client. */
	public static int getClientUpdateRate() {
		return NETWORK_UPDATE_RATE;
	}

	/** Returns the client timeout in ms for connect, ping etc. */
	public static int getClientTimeout() {
		return CLIENT_TIMEOUT;
	}
}