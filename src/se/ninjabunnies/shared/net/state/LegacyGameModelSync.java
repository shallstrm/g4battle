package se.ninjabunnies.shared.net.state;

import se.ninjabunnies.shared.model.IReadonlyGameModel;

/**
* A legacy way to transfer the whole game model between server and client. Euw.
*/
public class LegacyGameModelSync implements INinjaBunniesChange {
	/** The GameModel to sync. */
	private IReadonlyGameModel gameModel;
	
	/**
	 * Get a new instance of this change object.
	 */
	public static LegacyGameModelSync getInstance(IReadonlyGameModel gameModel) {
		LegacyGameModelSync instance = new LegacyGameModelSync();
		
		instance.setGameModel(gameModel);
		
		return instance;
	}

	/**
	 * @return the gameModel
	 */
	public IReadonlyGameModel getGameModel() {
		return gameModel;
	}

	/**
	 * @param gameModel the gameModel to set
	 */
	public void setGameModel(IReadonlyGameModel gameModel) {
		this.gameModel = gameModel;
	}
	
	@Override
	public String toString() {
		return "[LegacyGameModelSync: syncing whole game model - watch out!]";
	}
}