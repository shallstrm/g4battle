package se.ninjabunnies.shared.net.state;

import se.ninjabunnies.shared.descriptor.IMapDescriptor;

/**
 * A legacy way to transfer teams with players on game setup.
 */
public class LegacyMapSetup implements INinjaBunniesChange {
	/** The teams with players in them. Note the legacy data types! */
	private IMapDescriptor map;
	
	/**
	 * Get a new instance of this change object.
	 */
	public static LegacyMapSetup getInstance(IMapDescriptor map) {
		LegacyMapSetup instance = new LegacyMapSetup();
		
		instance.setMap(map);
		
		return instance;
	}

	/**
	 * @return the map
	 */
	public IMapDescriptor getMap() {
		return map;
	}

	/**
	 * @param map the map to set
	 */
	public void setMap(IMapDescriptor map) {
		this.map = map;
	}

	@Override
	public String toString() {
		return "[LegacyMapSetup: syncing map]";
	}
}