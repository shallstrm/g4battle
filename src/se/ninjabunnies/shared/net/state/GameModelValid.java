package se.ninjabunnies.shared.net.state;

/**
 * The game model is now valid. The game has started.
 */
public class GameModelValid implements INinjaBunniesChange {
	/**
	 * Get a new instance of this change object.
	 */
	public static GameModelValid getInstance() {
		return new GameModelValid();
	}
	
	@Override
	public String toString() {
		return "[GameModelValid]";
	}
}