package se.ninjabunnies.shared.net.state;

import java.util.List;

import se.ninjabunnies.shared.model.IReadonlyTeam;

/**
 * A legacy way to transfer teams with players on game setup.
 */
public class LegacyTeamSetup implements INinjaBunniesChange {
	/** The teams with players in them. Note the legacy data types! */
	private List<IReadonlyTeam> teams;
	
	/**
	 * Get a new instance of this change object.
	 */
	public static LegacyTeamSetup getInstance(List<IReadonlyTeam> teams) {
		LegacyTeamSetup instance = new LegacyTeamSetup();
		
		instance.setTeams(teams);
		
		return instance;
	}
	
	/**
	 * @return the list of teams that is the new teams.
	 */
	public List<IReadonlyTeam> getTeams() {
		return teams;
	}

	/**
	 * Set the teams. The list of teams supplied should contain teams as well
	 * as players in them.
	 * 
	 * @param teams a list of teams.
	 */
	public void setTeams(List<IReadonlyTeam> teams) {
		this.teams = teams;
	}

	@Override
	public String toString() {
		String teamCount = (teams != null)
				? Integer.toString(teams.size())
				: "null";
		return "[LegacyTeamSetup: syncing " + teamCount + " teams]";
	}
}