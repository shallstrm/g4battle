package se.ninjabunnies.shared.net.state;

/**
 * The user requests that the turn should be ended.
 */
public class EndTurnRequest implements INinjaBunniesChange {
	/**
	 * Get a new instance of this change object.
	 */
	public static EndTurnRequest getInstance() {
		return new EndTurnRequest();
	}
	
	@Override
	public String toString() {
		return "[EndTurnRequest]";
	}
}