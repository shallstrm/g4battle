package se.ninjabunnies.shared.net.state;

/**
 * A figure health change consists of a player id valid in the context (change
 * bus) the change object should be posted to.
 */
public class FigureHealthChange implements INinjaBunniesChange {
	/** The figure's ID. */
	private int figureId = -1;
	
	/** The health level. */
	private int health = 0;
	
	/**
	 * Get a new instance of this change object.
	 */
	public static FigureHealthChange getInstance(int figureId, int health) {
		FigureHealthChange instance = new FigureHealthChange();
		
		instance.setFigureId(figureId);
		instance.setHealth(health);
		
		return instance;
	}

	/**
	 * @return the figureId
	 */
	public int getFigureId() {
		return figureId;
	}


	/**
	 * @param figureId the figureId to set
	 */
	public void setFigureId(int playerId) {
		this.figureId = playerId;
	}


	/**
	 * @return the health
	 */
	public int getHealth() {
		return health;
	}


	/**
	 * @param health the health to set
	 */
	public void setHealth(int health) {
		this.health = health;
	}
	
	@Override
	public String toString() {
		return "[FigureHealthChanged: {figureId: " + figureId + ", health: " +
				health + "}]";
	}
}