package se.ninjabunnies.shared.net.state;

import se.ninjabunnies.shared.util.Position;

/**
 * The user performed an action on the specified tile (position).
 */
public class TileActionPerformed implements INinjaBunniesChange {
	/** The position of the tile that got acted on. */
	private Position position;
	
	/**
	 * Get a new instance of this change object.
	 */
	public static TileActionPerformed getInstance(Position position) {
		TileActionPerformed instance = new TileActionPerformed();
		
		instance.setPosition(position);
		
		return instance;
	}

	/**
	 * @return the position
	 */
	public Position getPosition() {
		return position;
	}

	/**
	 * @param position the position to set
	 */
	public void setPosition(Position position) {
		this.position = position;
	}
	
	@Override
	public String toString() {
		String text = (position != null) ? position.toString() : "null";
		return "[TileActionPerformed: " + text + "]";
	}
}