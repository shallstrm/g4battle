package se.ninjabunnies.shared.net.state;

import se.ninjabunnies.shared.changebus.IChange;

/**
 * A change withing the Ninja Bunnies game occured, and was serialized as an
 * object of this type.
 * 
 * <p>INinjaBunniesChanges should be JavaBean-like:
 * <ul>
 *  <li>They should have a 0-ary constructor (no arguments).</li>
 *  <li>They should have getters and setters for properties.</li>
 *  <li>They do not, unlike JavaBeans, have to implement Serializable.</li>
 * </ul>
 * 
 * <!-- Motivation: We use Kryo, which requires 0-ary constructors, but handles
 *      serialization on its own without Serializable. -->
 * 
 * @author Joel Severin
 */
public interface INinjaBunniesChange extends IChange {
}