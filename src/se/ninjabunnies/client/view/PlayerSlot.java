package se.ninjabunnies.client.view;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JCheckBox;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.BorderLayout;
import java.awt.Font;
import javax.swing.SwingConstants;
import se.ninjabunnies.shared.util.ColorGenerator.ColorDescriptor;
import javax.swing.border.BevelBorder;
import java.awt.GridLayout;

/**
 * View representing one player
 * @author Serade
 *
 */
@SuppressWarnings("serial")
public class PlayerSlot extends JPanel {
	private boolean isOpen = true;
	private JTextField textFieldPlayerName;
	private final JCheckBox chckbxReady = new JCheckBox();
	private JButton buttonClosePlayerSlot;
	private JComboBox<String> comboBoxTeam;
	private JComboBox<String> comboBoxColor;
	private JComboBox<String> comboBoxStartPosition;
	private JPanel panel;
	private JPanel panel_1;
	private JPanel panel_2;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public PlayerSlot() {
		setBorder(null);
		this.setMaximumSize(new Dimension(3000, 40));
		this.setMinimumSize(new Dimension(100, 40));
		setLayout(new CardLayout(0, 0));
		
		JPanel panelCardOpen = new JPanel();
		panelCardOpen.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		add(panelCardOpen, "name_5026042939760");
		panelCardOpen.setLayout(new GridLayout(0, 2, 0, 0));
		
		panel = new JPanel();
		panelCardOpen.add(panel);
		panel.setLayout(new BorderLayout(0, 0));
		
		textFieldPlayerName = new JTextField();
		panel.add(textFieldPlayerName);
		textFieldPlayerName.setFont(new Font("Verdana", Font.PLAIN, 14));
		textFieldPlayerName.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				JTextField source = (JTextField) e.getSource();
				source.setSelectionStart(0);
				source.setSelectionEnd(source.getText().length());
			}
		});
		textFieldPlayerName.setAlignmentX(Component.LEFT_ALIGNMENT);
		textFieldPlayerName.setColumns(10);
		
		panel_1 = new JPanel();
		panelCardOpen.add(panel_1);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		panel_2 = new JPanel();
		panel_1.add(panel_2);
		panel_2.setLayout(new GridLayout(0, 4, 0, 0));
		
		comboBoxTeam = new JComboBox<String>();
		panel_2.add(comboBoxTeam);
		comboBoxTeam.setFont(new Font("Verdana", Font.PLAIN, 14));
		comboBoxTeam.setAlignmentX(Component.LEFT_ALIGNMENT);
		comboBoxTeam.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3", "4"}));
		
		comboBoxColor = new JComboBox<String>();
		panel_2.add(comboBoxColor);
		comboBoxColor.setFont(new Font("Verdana", Font.PLAIN, 14));
		comboBoxColor.setAlignmentX(Component.LEFT_ALIGNMENT);
		comboBoxColor.setModel(new DefaultComboBoxModel(ColorDescriptor.values()));
		
		comboBoxStartPosition = new JComboBox<String>();
		panel_2.add(comboBoxStartPosition);
		comboBoxStartPosition.setFont(new Font("Verdana", Font.PLAIN, 14));
		comboBoxStartPosition.setBorder(null);
		comboBoxStartPosition.setAlignmentX(Component.LEFT_ALIGNMENT);
		comboBoxStartPosition.setModel(new DefaultComboBoxModel<String>(new String[] {"1", "2","3","4","5"}));
		chckbxReady.setHorizontalAlignment(SwingConstants.CENTER);
		panel_2.add(chckbxReady);
		chckbxReady.setFont(new Font("Verdana", Font.PLAIN, 14));
		chckbxReady.setToolTipText("All players need to be set to Ready before starting the game");
		
		buttonClosePlayerSlot = new JButton("X");
		panel_1.add(buttonClosePlayerSlot, BorderLayout.EAST);
		buttonClosePlayerSlot.setFont(new Font("Verdana", Font.PLAIN, 14));
		buttonClosePlayerSlot.setToolTipText("Remove Player");
		buttonClosePlayerSlot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JButton button = (JButton) e.getSource();
				PlayerSlot player = (PlayerSlot) button.getParent().getParent().getParent();
				player.closePlayerSlot();
			}
		});
		
		JPanel panelCardClosed = new JPanel();
		panelCardClosed.setBackground(Color.WHITE);
		add(panelCardClosed, "name_5026056770819");
		panelCardClosed.setLayout(new BorderLayout(0, 0));
		
		JButton buttonOpenPlayerSlot = new JButton();
		buttonOpenPlayerSlot.setHorizontalAlignment(SwingConstants.LEFT);
		buttonOpenPlayerSlot.setFont(new Font("Tahoma", Font.PLAIN, 15));
		buttonOpenPlayerSlot.setText("Open this player slot");
		buttonOpenPlayerSlot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JButton button = (JButton) e.getSource();
				PlayerSlot player = (PlayerSlot) button.getParent().getParent();
				player.openPlayerSlot();
			}
		});
		buttonOpenPlayerSlot.setBackground(Color.WHITE);
		panelCardClosed.add(buttonOpenPlayerSlot, BorderLayout.CENTER);
	}
	
	/**
	 * Enables or disables the ready checkbox acording to gametype.
	 * @param hotseat whether the game is hotseated or not
	 */
	public void setHotseat(Boolean hotseat) {
		chckbxReady.setSelected(hotseat);
		chckbxReady.setEnabled(!hotseat);
	}
	
	/**
	 * 
	 * @return
	 */
	public JButton getClosePlayerSlotButton () {
		return buttonClosePlayerSlot;
	}
	
	public void setPlayerName(String name) {
		textFieldPlayerName.setText(name);
	}

	public void setTeam(int count) {
		comboBoxTeam.setSelectedIndex(count);
	}

	public void setColor(int count) {
		comboBoxColor.setSelectedIndex(count);
		
	}

	public void setStartPosition(int count) {
		comboBoxStartPosition.setSelectedIndex(count);
	}
	
	/**
	 * Get the starting position of the player. zero-based.
	 * 
	 * @return the start position.
	 */
	public int getStartPosition() {
		return comboBoxStartPosition.getSelectedIndex();
	}

	/**
	 * Closes given player slot
	 * @param player the player slot to close
	 */
	public void closePlayerSlot() {
		CardLayout manager = (CardLayout) getLayout();
		manager.last(this);
		isOpen = false;
	}

	/**
	 * Opens given player slot
	 * @param player the player slot to close
	 */
	public void openPlayerSlot() {
		CardLayout manager = (CardLayout) getLayout();
		manager.first(this);
		isOpen = true;
	}
	
	/**
	 * Sets a player to essential and thus makes it impossible to remove them form the list of players. 
	 * The first two players are essential and therefore should be set Essentail at all times.
	 * @param player OnePlayerPanel representing the player to set/unset essential
	 * @param esential whether to set or unset essential status.
	 */
	public void setEsential(Boolean esential) {
		openPlayerSlot();
		getClosePlayerSlotButton().setEnabled(!esential);
	}

	/**
	 * Returns the index for the selected team
	 * @return the index for the selected team
	 */
	public int getTeamIndex() {
		return comboBoxTeam.getSelectedIndex();
	}
	
	/**
	 * Returns the index for the selected color
	 * @return the index for the selected color
	 */
	public int getColorIndex() {
		return comboBoxColor.getSelectedIndex();
	}
	
	/**
	 * Returns the given name for the player
	 * @return the given name for the player
	 */
	public String getPlayerName() {
		return textFieldPlayerName.getText();
	}

	/**
	 * Returns if PlayerSlot is open
	 * @return <code>true</code> if slot is open, else <code>false</code>
	 */
	public boolean isOpen() {
		return isOpen;
	}

}

