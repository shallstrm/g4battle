package se.ninjabunnies.client.view.net;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.swing.Timer;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;

import se.ninjabunnies.shared.changebus.IChange;
import se.ninjabunnies.shared.changebus.IChangeSet;
import se.ninjabunnies.shared.changebus.IWritableChangeBus;
import se.ninjabunnies.shared.net.NinjaBunniesNetwork;

/**
 * The view used for client networking. A shim to the underlying networking
 * implementation.
 * 
 * <p><strong>Note:</strong> all interfacing of this class should be done from,
 * and updates will be delivered on, the AWT/Swing thread. This is a convenience
 * for users, which always use that thread anyway. This thread is called "local"
 * 
 * @author Joel Severin
 */
public class NetClientView {
	/** The KryoNet client instance. */
	private Client client = null;
	
	/**
	 * The bus client implementation can write replies on.
	 * Set to non-null by the KryoNet server thread when connected.
	 * */
	private volatile IWritableChangeBus replyBus = null;
	
	/** Set to true connection was disconnected gracefully. */
	private volatile boolean disconnected = false;
	
	/** The IOException that was thrown if something failed. */
	private volatile IOException failedWithException = null;
	
	/** The net event listener used for this client view. */
	private NetEventListener netEventListener = null;
	
	/** The client notifier (just for (thread) separation, in this class). */
	private ClientNotifier clientNotifier = new ClientNotifier();
	
	/**
	 * The incoming change set queue, filled by the networking thread and read
	 * by the local thread.
	 */
	Queue<IChangeSet> changeSetQueue = new ConcurrentLinkedQueue<IChangeSet>();
	
	/** Receive networking updates on this thread. */
	public interface NetEventListener {
		/**
		 * Called once when the client has connected to the server.
		 * 
		 * @param replyBus
		 *        the bus to reply to. Calls to send() will send the change over
		 *        the network straight away. Calling setChangeBusBarrier is
		 *        reserved in the current implementation (do not call it).
		 */
		public void onConnect(IWritableChangeBus replyBus);
		
		/**
		 * Called once when the client has disconnected from the server.
		 * 
		 * @param exception
		 *        a exception that was thrown if something failed, or null
		 *        if the conenction was gracefully closed.
		 * 
		 */
		public void onDisconnect(IOException exception);
		
		/**
		 * A change set was received over the network.
		 * 
		 * @param change the change.
		 */
		public void onChangeSet(IChangeSet changeSet);
	}
	
	/**
	 * Create a new connection.
	 * 
	 * @param host the server hostname (or ip address).
	 * @param port the TCP port to connect to the server on.
	 * 
	 * @throws IllegalStateException if the client has already connected.
	 */
	public void connect(final String host, final int port) {
		if(client != null) {
			throw new IllegalStateException("client already conencted");
		}
		
		client = new Client(8092*16, 2048*16);
		client.start();
		NinjaBunniesNetwork.setupNetworkFor(client);// Kryo registration.
		clientNotifier.start();
		client.addListener(new KryoNetListener());
		
		// connect() is blocking, so we launch a new thread doing it.
		(new Thread() {
			@Override
			public void run() {
				try {
					client.connect(NinjaBunniesNetwork.getClientTimeout(),
						host, port);
				} catch (IOException exception) {
					failedWithException = exception;
				}
			}
		}).start();
	}
	
	public void disconnect() {
		client.close();// Will disconnect() also, stopping our Timer below.
	}
	
	private class KryoNetListener extends Listener {
		@Override
		public void connected(Connection connection) {
			// This also signals that "connected = true" for the local thread.
			replyBus = new BusWrappedConnection(connection);
		}
		
		@Override
		public void received(Connection connection, Object object) {
			if(object instanceof IChangeSet) {
				// Put it so the local thread can read it.
				changeSetQueue.offer((IChangeSet) object);
			}
		}
		
		@Override
		public void disconnected(Connection connection) {
			disconnected = true;
		}
	}
	
	/**
	 * The client notifier, responsible of running things in the local thread.
	 */
	private class ClientNotifier implements ActionListener {
		/** Set to true when local thread has notified the NetEvent. */
		private boolean connectedNotified = false;
		
		/** Set to true when local thread has notified the NetEvent. */
		private boolean disconnectedNotified = false;
		
		/** Start the client notifier. */
		public void start() {
			Timer timer = new Timer(NinjaBunniesNetwork.getClientUpdateRate(),
					this);
			timer.setRepeats(false);
			timer.start();
		}

		@Override
		public void actionPerformed(ActionEvent actionEvent) {
			// Note: This is also run the local thread (=AWT/Swing-thread).
			
			boolean stopTimer = false;
			
			// Notify for connection. A replyBus that's not null means we are
			// connected (and can send replies).
			if(netEventListener != null && !connectedNotified
					&& replyBus != null) {
				connectedNotified = true;
				netEventListener.onConnect(replyBus);
			}
			
			// Notify for disconnection/fail. Issue multiple disconnects if
			// there is errors to report.
			if(disconnected || failedWithException != null) {
				stopTimer = true;
				if(!disconnectedNotified && netEventListener != null) {
					disconnectedNotified = true;
					netEventListener.onDisconnect(failedWithException);
				}
			// Check for new server data.
			} else if(netEventListener != null) {
				IChangeSet changeSet = null;
				do {
					changeSet = changeSetQueue.poll();
					if(changeSet != null) {
						netEventListener.onChangeSet(changeSet);
					}
				} while(changeSet != null);
			}
			
			if(!stopTimer) {
				// @todo This is not the correct way doing it... Delta time...
				((Timer) actionEvent.getSource()).restart();
			}
		}
	}
	
	/**
	 * Sets the net event listener used for this client view.
	 * 
	 * @param netEventListener a net event listener, or null to unset.
	 */
	public void setNetEventListener(NetEventListener netEventListener) {
		this.netEventListener = netEventListener;
	}
	
	/**
	 * A KryoNet connection object that masquerades as a writable change bus.
	 * Writes goes to the network. Note that this is not buffered - calls to
	 * send() will get sent immediately, and setChangeSetBarrier() is reserved
	 * in this implemenation for future use.
	 */
	private static class BusWrappedConnection implements IWritableChangeBus {
		private Connection connection;
		
		public BusWrappedConnection(Connection connection) {
			this.connection = connection;
		}
		
		@Override
		public void send(IChange change) {
			int bytes = connection.sendTCP(change);
			System.out.println("[$Client sent " + bytes + " bytes]: " +
					change.toString());
		}

		@Override
		public void setChangeSetBarrier() {
			throw new UnsupportedOperationException("bus barrier reserved OP");
		}
	}
	
	/**
	 * Gets the reply bus on which client implementations can send data to the
	 * server. This method returns null before the onConnect event has been
	 * issued, and a writable change bus with rules specified by
	 * {@link NetEventListener#onConnect(IWritableChangeBus)}.
	 * 
	 * @return the writable change bus.
	 */
	public IWritableChangeBus getReplyBus() {
		return replyBus;
	}
}