package se.ninjabunnies.client.view;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;

import javax.swing.JButton;
import java.awt.Component;
import javax.swing.Box;

import se.ninjabunnies.shared.util.ResourceManager;

import java.awt.CardLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Dimension;
import javax.swing.SwingConstants;
import java.awt.GridLayout;
import java.awt.FlowLayout;
import java.awt.Cursor;
import javax.swing.border.MatteBorder;

@SuppressWarnings("serial")
public class TutorialView extends JPanel {
	private ITutorialViewListener listener;
	private int currentCard = 1;
	private JPanel panelCards;
	private JButton prevButton;
	private JButton nextButton;
	
	private ImageIcon backToMenu = new ImageIcon(ResourceManager.getInstance().getImageResource("tutorial/BackToMenu.png"));
	private ImageIcon backToMenu2 = new ImageIcon(ResourceManager.getInstance().getImageResource("tutorial/BackToMenu_2.png"));
	private ImageIcon previous = new ImageIcon(ResourceManager.getInstance().getImageResource("tutorial/Previous.png"));
	private ImageIcon previous2 = new ImageIcon(ResourceManager.getInstance().getImageResource("tutorial/Previous_2.png"));
	private ImageIcon next = new ImageIcon(ResourceManager.getInstance().getImageResource("tutorial/Next.png"));
	private ImageIcon next2 = new ImageIcon(ResourceManager.getInstance().getImageResource("tutorial/Next_2.png"));
	private ImageIcon sepImage = new ImageIcon(ResourceManager.getInstance().getImageResource("tutorial/Sep.png"));
	private ImageIcon figureImage = new ImageIcon(ResourceManager.getInstance().getImageResource("tutorial/koala.png"));
	private ImageIcon allFiguresImage = new ImageIcon(ResourceManager.getInstance().getImageResource("tutorial/instructions.png"));
	
	public TutorialView() {
		setBackground(Color.WHITE);
		setLayout(new BorderLayout(0, 0));
		
		JPanel panelTitlePostioning = new JPanel();
		panelTitlePostioning.setBackground(Color.WHITE);
		add(panelTitlePostioning, BorderLayout.NORTH);
		panelTitlePostioning.setLayout(new BorderLayout(0, 0));
		
		JLabel lblHowToPlay = new JLabel();
		lblHowToPlay.setHorizontalAlignment(SwingConstants.CENTER);
		lblHowToPlay.setIcon(new ImageIcon(ResourceManager.getInstance().getImageResource("tutorial/HowToPlayNinjaBunnies.png")));
		panelTitlePostioning.add(lblHowToPlay);
		lblHowToPlay.setBackground(Color.WHITE);
		
		Component verticalStrut_2 = Box.createVerticalStrut(20);
		verticalStrut_2.setPreferredSize(new Dimension(0, 40));
		panelTitlePostioning.add(verticalStrut_2, BorderLayout.SOUTH);
		
		panelCards = new JPanel();
		add(panelCards, BorderLayout.CENTER);
		panelCards.setLayout(new CardLayout(0, 0));
		
		JPanel card1 = new JPanel();
		panelCards.add(card1, "name_105231781288422");
		card1.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		card1.add(panel, BorderLayout.NORTH);
		
		JLabel lblSummary = new JLabel("Summary");
		lblSummary.setFont(new Font("Verdana", Font.PLAIN, 40));
		panel.add(lblSummary);
		
		JPanel panel_5 = new JPanel();
		panel_5.setBackground(Color.WHITE);
		card1.add(panel_5, BorderLayout.CENTER);
		panel_5.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane1 = new JScrollPane();
		scrollPane1.setBorder(null);
		panel_5.add(scrollPane1);
		
		JTextArea txt1 = new JTextArea();
		txt1.setBorder(null);
		txt1.setBackground(Color.WHITE);
		txt1.setDisabledTextColor(Color.BLACK);
		txt1.setFont(new Font("Verdana", Font.PLAIN, 16));
		txt1.setWrapStyleWord(true);
		txt1.setLineWrap(true);
		txt1.setText("Ninja Bunnies is a highly simplified version of the very famous tabletop battle games.  The game borrows some of the classic tabletop game rules, eg. various figures can only walk on different types of terrain. Even if you are an experienced tabletop player or a newbie we suggest you familiarize yourselft with some of the features. Browse through this guide and learn how to experience Ninja Bunniy at its best.\r\n\r\n In Ninja Bunnies, players fight in teams against each other. The goal is to defeat the opposing teams and being the only one left on the board. You fight by moving and attacking with diferent kinds of figures with different attributes. A team may consist of one or more players. The game ends when only one team alone is left standing.\r\n\r\n-G4");
		txt1.setEnabled(false);
		txt1.setEditable(false);
		
		scrollPane1.setViewportView(txt1);
		
		Component horizontalStrut_2 = Box.createHorizontalStrut(20);
		horizontalStrut_2.setPreferredSize(new Dimension(150, 0));
		panel_5.add(horizontalStrut_2, BorderLayout.WEST);
		
		Component horizontalStrut_3 = Box.createHorizontalStrut(20);
		horizontalStrut_3.setPreferredSize(new Dimension(150, 0));
		panel_5.add(horizontalStrut_3, BorderLayout.EAST);
		
		JPanel card2 = new JPanel();
		panelCards.add(card2, "name_105231793081170");
		card2.setLayout(new BoxLayout(card2, BoxLayout.Y_AXIS));
		
		JPanel panel_6 = new JPanel();
		panel_6.setBackground(Color.WHITE);
		card2.add(panel_6);
		
		JLabel lblLobby = new JLabel("Lobby");
		lblLobby.setFont(new Font("Verdana", Font.PLAIN, 40));
		panel_6.add(lblLobby);
		
		JPanel panel_7 = new JPanel();
		panel_7.setBackground(Color.WHITE);
		card2.add(panel_7);
		panel_7.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane2 = new JScrollPane();
		scrollPane2.setBorder(null);
		scrollPane2.setBackground(Color.WHITE);
		panel_7.add(scrollPane2);
		
		JTextArea txt2 = new JTextArea();
		txt2.setBorder(null);
		txt2.setFont(new Font("Verdana", Font.PLAIN, 16));
		txt2.setDisabledTextColor(Color.BLACK);
		txt2.setLineWrap(true);
		txt2.setWrapStyleWord(true);
		txt2.setText("The lobby is where players get to decide under what conditions they want to play. Here you select map, number of players, players' names, teams, the figures' color as well as the start position on the map. The map description includes an image to display the maps enviromental properties, a name, a summary of the number of tiles that compiles the map and the maximum amount of players.\r\n\r\nThe number of players is determined by how many slots that are open. To open a new slot, simply click \"Open this player slot\". The slot can be closed by hitting \"X\"  at the far right.  There must be at least two slots open since the game requires at least two players. Next to the far right is a check box that confirmes if every player is ready. If  \"hotseat\" is selected in the menu, these are automatically checked.");
		txt2.setEnabled(false);
		txt2.setEditable(false);
		
		scrollPane2.setViewportView(txt2);
		
		Component horizontalStrut_4 = Box.createHorizontalStrut(20);
		horizontalStrut_4.setPreferredSize(new Dimension(150, 0));
		panel_7.add(horizontalStrut_4, BorderLayout.WEST);
		
		Component horizontalStrut_5 = Box.createHorizontalStrut(20);
		horizontalStrut_5.setPreferredSize(new Dimension(150, 0));
		panel_7.add(horizontalStrut_5, BorderLayout.EAST);
		
		JPanel card10 = new JPanel();
		panelCards.add(card10, "name_90210051312299");
		card10.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_26 = new JPanel();
		panel_26.setBackground(Color.WHITE);
		card10.add(panel_26, BorderLayout.NORTH);
		
		JLabel lblNewLabel_3 = new JLabel("Network");
		lblNewLabel_3.setFont(new Font("Verdana", Font.PLAIN, 40));
		panel_26.add(lblNewLabel_3);
		
		JPanel panel_27 = new JPanel();
		panel_27.setBackground(Color.WHITE);
		card10.add(panel_27, BorderLayout.CENTER);
		panel_27.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane10 = new JScrollPane();
		scrollPane10.setBorder(null);
		panel_27.add(scrollPane10);
		
		JTextArea txt10 = new JTextArea();
		txt10.setBorder(null);
		txt10.setText("Ninja Bunnies support networking.The game is created as normal in a lobby, but instead of \"Hotseat\", select \"New Game\". Other players will be able to join with your server address.");
		txt10.setWrapStyleWord(true);
		txt10.setLineWrap(true);
		txt10.setEnabled(false);
		txt10.setDisabledTextColor(Color.BLACK);
		txt10.setEditable(false);
		txt10.setFont(new Font("Verdana", Font.PLAIN, 16));
		
				scrollPane10.setViewportView(txt10);
				
				Component horizontalStrut_20 = Box.createHorizontalStrut(20);
				horizontalStrut_20.setPreferredSize(new Dimension(150, 0));
				panel_27.add(horizontalStrut_20, BorderLayout.WEST);
				
				Component horizontalStrut_21 = Box.createHorizontalStrut(20);
				horizontalStrut_21.setPreferredSize(new Dimension(150, 0));
				panel_27.add(horizontalStrut_21, BorderLayout.EAST);
		
		JPanel card3 = new JPanel();
		card3.setBackground(Color.WHITE);
		panelCards.add(card3, "name_740569621149956");
		card3.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_8 = new JPanel();
		panel_8.setBackground(Color.WHITE);
		card3.add(panel_8, BorderLayout.NORTH);
		
		JLabel lblBoard = new JLabel("Board");
		lblBoard.setFont(new Font("Verdana", Font.PLAIN, 40));
		panel_8.add(lblBoard);
		
		JPanel panel_9 = new JPanel();
		panel_9.setBackground(Color.WHITE);
		card3.add(panel_9);
		panel_9.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane3 = new JScrollPane();
		scrollPane3.setBackground(Color.WHITE);
		scrollPane3.setBorder(null);
		panel_9.add(scrollPane3, BorderLayout.CENTER);
		
		JTextArea textArea3 = new JTextArea();
		textArea3.setDisabledTextColor(Color.BLACK);
		textArea3.setEnabled(false);
		textArea3.setEditable(false);
		textArea3.setFont(new Font("Verdana", Font.PLAIN, 16));
		textArea3.setLineWrap(true);
		textArea3.setWrapStyleWord(true);
		textArea3.setText("Game's board, consists of a map and figures. The map consists of a number of tiles traversable by figures. Each tile represents an environmental, together forming the map. Different figures can walk on different set of tiles. (See \"Figures\" for information about traversable tiles).");
		textArea3.setBackground(Color.WHITE);
		textArea3.setBorder(null);
		
		scrollPane3.setViewportView(textArea3);
		
		Component horizontalStrut_6 = Box.createHorizontalStrut(20);
		horizontalStrut_6.setPreferredSize(new Dimension(150, 0));
		panel_9.add(horizontalStrut_6, BorderLayout.EAST);
		
		Component horizontalStrut_7 = Box.createHorizontalStrut(20);
		horizontalStrut_7.setPreferredSize(new Dimension(150, 0));
		panel_9.add(horizontalStrut_7, BorderLayout.WEST);
		
		JPanel card4 = new JPanel();
		card4.setBackground(Color.WHITE);
		panelCards.add(card4, "name_741017154305978");
		card4.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_10 = new JPanel();
		panel_10.setBackground(Color.WHITE);
		card4.add(panel_10, BorderLayout.NORTH);
		
		JLabel lblTurn = new JLabel("Turn");
		lblTurn.setFont(new Font("Verdana", Font.PLAIN, 40));
		panel_10.add(lblTurn);
		
		JPanel panel_11 = new JPanel();
		panel_11.setBackground(Color.WHITE);
		card4.add(panel_11, BorderLayout.CENTER);
		panel_11.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane4 = new JScrollPane();
		scrollPane4.setBorder(null);
		scrollPane4.setBackground(Color.WHITE);
		panel_11.add(scrollPane4, BorderLayout.CENTER);
		
		JTextArea txt4 = new JTextArea();
		txt4.setEditable(false);
		txt4.setEnabled(false);
		txt4.setBorder(null);
		txt4.setBackground(new Color(255, 255, 255));
		txt4.setDisabledTextColor(Color.BLACK);
		txt4.setFont(new Font("Verdana", Font.PLAIN, 16));
		txt4.setLineWrap(true);
		txt4.setWrapStyleWord(true);
		txt4.setText("The game is played in turns. During each turn one player is allowed, though limited, to move his figures and attack. Each kind of figure have a pre-set number of steps to walk. As soon as a figure attacks, it's status is set to used, making it unmovable. When all the figure's made an attack it automatically becomes the next player's turn. The player does not need to act with all or any of his figure and may at any time during his turn click \"End Turn\" button to exit the turn. \"End Turn\" button is located at the bottom of the side panel. The turn queue mixes between each team, which means that two players on the same team never have their turns right after each other.");
		
		scrollPane4.setViewportView(txt4);
		
		Component horizontalStrut_9 = Box.createHorizontalStrut(20);
		horizontalStrut_9.setPreferredSize(new Dimension(150, 0));
		panel_11.add(horizontalStrut_9, BorderLayout.WEST);
		
		Component horizontalStrut_10 = Box.createHorizontalStrut(20);
		horizontalStrut_10.setPreferredSize(new Dimension(150, 0));
		panel_11.add(horizontalStrut_10, BorderLayout.EAST);
		
		JPanel card5 = new JPanel();
		card5.setBackground(Color.WHITE);
		panelCards.add(card5, "name_741019345260775");
		card5.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_12 = new JPanel();
		panel_12.setBackground(Color.WHITE);
		card5.add(panel_12, BorderLayout.NORTH);
		
		JLabel lblNewLabel = new JLabel("Figures");
		lblNewLabel.setFont(new Font("Verdana", Font.PLAIN, 40));
		panel_12.add(lblNewLabel);
		
		JPanel panel_13 = new JPanel();
		panel_13.setBackground(Color.WHITE);
		card5.add(panel_13, BorderLayout.CENTER);
		panel_13.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_16 = new JPanel();
		panel_13.add(panel_16, BorderLayout.CENTER);
		panel_16.setLayout(new GridLayout(0, 2, 0, 0));
		
		JPanel panel_14 = new JPanel();
		panel_14.setBorder(new MatteBorder(0, 0, 0, 10, (Color) Color.WHITE));
		panel_16.add(panel_14);
		panel_14.setBackground(Color.WHITE);
		panel_14.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane5 = new JScrollPane();
		panel_14.add(scrollPane5);
		
		JTextArea txt5 = new JTextArea();
		txt5.setEnabled(false);
		txt5.setEditable(false);
		txt5.setFont(new Font("Verdana", Font.PLAIN, 16));
		txt5.setLineWrap(true);
		txt5.setWrapStyleWord(true);
		txt5.setText("An importan element of the game, which provides the strategic aspect,  is that different figues can only walk on different tiles. Every kind of figure has each an unique set of traversable tiles, forcing the player to use his head.\r\n\r\nA figures has a number of features that each players should know about in order to fully utilize them.\r\n\r\nFeatures:\r\n* A movement value which determines amount of steps determines how far the figure can travel during a turn.\r\n* An armor value which determines how much the damage from an attacking figure should be reduced.\r\n* Healt points (HP) which keep track of how much damage the figure can recieve before considered defeated. When the health point of a character reaches zero it's considered defeated and is removed from the board. Although healt point does not affected its performance or other properties.\r\n* A weapon that determines range, speed and  power of an attack.\r\n\r\nWeapon features:\r\n* Range determines how many tiles away the weapon reaches.\r\n* Attack speed determines the amount of blows.\r\n* Attack strength determines how strong each blow is.\r\n(Attack speed * attack power = how much HP the opposing figure loses).");
		txt5.setDisabledTextColor(Color.BLACK);
		
		scrollPane5.setViewportView(txt5);
		
		JPanel panel_15 = new JPanel();
		panel_16.add(panel_15);
		panel_15.setBackground(Color.WHITE);
		panel_15.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane51 = new JScrollPane();
		panel_15.add(scrollPane51);
		
		
		
		JLabel allFiguresLabel = new JLabel("");
		allFiguresLabel.setIcon(allFiguresImage);
		scrollPane51.setViewportView(allFiguresLabel);
		
		Component horizontalStrut_8 = Box.createHorizontalStrut(20);
		horizontalStrut_8.setPreferredSize(new Dimension(123, 0));
		panel_13.add(horizontalStrut_8, BorderLayout.WEST);
		
		Component horizontalStrut_11 = Box.createHorizontalStrut(20);
		horizontalStrut_11.setPreferredSize(new Dimension(123, 0));
		panel_13.add(horizontalStrut_11, BorderLayout.EAST);
		revalidate();
		
		JPanel card6 = new JPanel();
		card6.setBackground(Color.WHITE);
		panelCards.add(card6, "name_741021577512277");
		card6.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_17 = new JPanel();
		panel_17.setBackground(Color.WHITE);
		card6.add(panel_17, BorderLayout.NORTH);
		
		JLabel lblNewLabel_1 = new JLabel("Active Figure");
		lblNewLabel_1.setFont(new Font("Verdana", Font.PLAIN, 40));
		panel_17.add(lblNewLabel_1);
		
		JPanel panel_18 = new JPanel();
		panel_18.setBackground(Color.WHITE);
		card6.add(panel_18, BorderLayout.CENTER);
		panel_18.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane6 = new JScrollPane();
		scrollPane6.setBorder(null);
		panel_18.add(scrollPane6);
		
		JTextArea txt6 = new JTextArea();
		txt6.setEditable(false);
		txt6.setEnabled(false);
		txt6.setFont(new Font("Verdana", Font.PLAIN, 16));
		txt6.setBorder(null);
		txt6.setLineWrap(true);
		txt6.setWrapStyleWord(true);
		txt6.setText("Only one figure is capable of performing actions at a time, either move or attack. That awlays changing figure is the active figure and belongs to the active player. The active figure is recognized by it's yellow color. When there is a new turn a new active player is automatically chosen. During the turn the player may choose freely which figure to be active . A new active figure is selected by simply clicking on a different figure belonging to the active player. If a figure performes an attack, it becomes \"used\" (no longer possible to be active again) and player will have to wait until his next turn to use it again.");
		txt6.setDisabledTextColor(Color.BLACK);
		
		scrollPane6.setViewportView(txt6);
		
		Component horizontalStrut_12 = Box.createHorizontalStrut(20);
		horizontalStrut_12.setPreferredSize(new Dimension(150, 0));
		panel_18.add(horizontalStrut_12, BorderLayout.WEST);
		
		Component horizontalStrut_13 = Box.createHorizontalStrut(20);
		horizontalStrut_13.setPreferredSize(new Dimension(150, 0));
		panel_18.add(horizontalStrut_13, BorderLayout.EAST);
		
		JPanel card7 = new JPanel();
		card7.setBackground(Color.WHITE);
		panelCards.add(card7, "name_741026337390492");
		card7.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_19 = new JPanel();
		panel_19.setBackground(Color.WHITE);
		card7.add(panel_19, BorderLayout.NORTH);
		
		JLabel lblNewLabel_2 = new JLabel("Controls");
		lblNewLabel_2.setFont(new Font("Verdana", Font.PLAIN, 40));
		panel_19.add(lblNewLabel_2);
		
		JPanel panel_20 = new JPanel();
		panel_20.setBackground(Color.WHITE);
		card7.add(panel_20, BorderLayout.CENTER);
		panel_20.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane7 = new JScrollPane();
		scrollPane7.setBorder(null);
		panel_20.add(scrollPane7);
		
		JTextArea txt7 = new JTextArea();
		txt7.setEnabled(false);
		txt7.setEditable(false);
		txt7.setText("The game is controlled entirely with the mouse. The user navigate on the map by holding down the right mousebutton and dragging. Find out how to move and attack below.\r\n\r\nAttacking:\r\nAn attack can only be performed by the active figure. For an attack to be possible an opposing figure has to be in reach, which appears in red. If this requirement is met, the attack is performed by simply clicking the opponent's figure. After an attack is done, the figure's status is set to used, making it unmovable.\r\n\r\nMovement:\r\nA figure's movement capability, measured in steps, is determined by the figure's movement value. One step-length equals one tile. Movement can only be performed by the active figure with enought steps to reach the chosen location.\r\n");
		txt7.setLineWrap(true);
		txt7.setWrapStyleWord(true);
		txt7.setBorder(null);
		txt7.setFont(new Font("Verdana", Font.PLAIN, 16));
		txt7.setDisabledTextColor(Color.BLACK);
		
		scrollPane7.setViewportView(txt7);
		
		Component horizontalStrut_14 = Box.createHorizontalStrut(20);
		horizontalStrut_14.setPreferredSize(new Dimension(150, 0));
		panel_20.add(horizontalStrut_14, BorderLayout.EAST);
		
		Component horizontalStrut_15 = Box.createHorizontalStrut(20);
		horizontalStrut_15.setPreferredSize(new Dimension(150, 0));
		panel_20.add(horizontalStrut_15, BorderLayout.WEST);
		
		JPanel card8 = new JPanel();
		card8.setBackground(Color.WHITE);
		panelCards.add(card8, "name_741028299624613");
		card8.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_21 = new JPanel();
		panel_21.setBackground(Color.WHITE);
		card8.add(panel_21, BorderLayout.NORTH);
		
		JLabel lblSidePanel = new JLabel("Side Panel");
		lblSidePanel.setFont(new Font("Verdana", Font.PLAIN, 40));
		panel_21.add(lblSidePanel);
		
		JPanel panel_22 = new JPanel();
		panel_22.setBackground(Color.WHITE);
		card8.add(panel_22, BorderLayout.CENTER);
		panel_22.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane8 = new JScrollPane();
		scrollPane8.setBorder(null);
		panel_22.add(scrollPane8, BorderLayout.CENTER);
		
		JTextArea txt8 = new JTextArea();
		txt8.setEnabled(false);
		txt8.setEditable(false);
		txt8.setFont(new Font("Verdana", Font.PLAIN, 16));
		txt8.setText("To the right of the board is the side panel. The purpose of the side panel is to show desirable information, such as the properties of the active figure and whose turn it is. Another useful feature of the side panel is it's exploration display at the bottom, which provides information on the tile at the cursor's position, giving the player a chance to investigate the board. ");
		txt8.setBorder(null);
		txt8.setLineWrap(true);
		txt8.setWrapStyleWord(true);
		txt8.setDisabledTextColor(Color.BLACK);
		
		scrollPane8.setViewportView(txt8);
		
		Component horizontalStrut_16 = Box.createHorizontalStrut(20);
		horizontalStrut_16.setPreferredSize(new Dimension(150, 0));
		panel_22.add(horizontalStrut_16, BorderLayout.WEST);
		
		Component horizontalStrut_17 = Box.createHorizontalStrut(20);
		horizontalStrut_17.setPreferredSize(new Dimension(150, 0));
		panel_22.add(horizontalStrut_17, BorderLayout.EAST);
		
		JPanel card9 = new JPanel();
		card9.setBackground(Color.WHITE);
		panelCards.add(card9, "name_741029904398016");
		card9.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_23 = new JPanel();
		panel_23.setBackground(Color.WHITE);
		card9.add(panel_23, BorderLayout.NORTH);
		
		JLabel lblWinning = new JLabel("Winning");
		lblWinning.setFont(new Font("Verdana", Font.PLAIN, 40));
		panel_23.add(lblWinning);
		
		JPanel panel_24 = new JPanel();
		panel_24.setBackground(Color.WHITE);
		card9.add(panel_24, BorderLayout.CENTER);
		panel_24.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane9 = new JScrollPane();
		scrollPane9.setBorder(null);
		panel_24.add(scrollPane9);
		
		JTextArea txt9 = new JTextArea();
		txt9.setEnabled(false);
		txt9.setEditable(false);
		txt9.setText("The winner(s) is appointed when only one team remains. A winner-view will then appear with the results.");
		txt9.setLineWrap(true);
		txt9.setWrapStyleWord(true);
		txt9.setDisabledTextColor(Color.BLACK);
		txt9.setFont(new Font("Verdana", Font.PLAIN, 16));
		txt9.setBorder(null);
		
		scrollPane9.setViewportView(txt9);
		
		Component horizontalStrut_18 = Box.createHorizontalStrut(20);
		horizontalStrut_18.setPreferredSize(new Dimension(150, 0));
		panel_24.add(horizontalStrut_18, BorderLayout.WEST);
		
		Component horizontalStrut_19 = Box.createHorizontalStrut(20);
		horizontalStrut_19.setPreferredSize(new Dimension(150, 0));
		panel_24.add(horizontalStrut_19, BorderLayout.EAST);
		
		JPanel panelButtons = new JPanel();
		panelButtons.setBackground(Color.WHITE);
		add(panelButtons, BorderLayout.SOUTH);
		panelButtons.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.WHITE);
		panelButtons.add(panel_1);
		panel_1.setLayout(new GridLayout(0, 3, 0, 0));
		
		final JButton backButton = new JButton();
		backButton.setContentAreaFilled(false);
		backButton.setFocusPainted(false);
		backButton.setFocusable(false);
		backButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		backButton.setHorizontalAlignment(SwingConstants.RIGHT);
		panel_1.add(backButton);
		backButton.setBorder(null);
		backButton.setIcon(backToMenu);
		backButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				backButton.setIcon(backToMenu2);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				backButton.setIcon(backToMenu);
			}
		});
		backButton.setBackground(Color.WHITE);
			
			JPanel panel_3 = new JPanel();
			panel_3.setBackground(Color.WHITE);
			panel_1.add(panel_3);
			
			JPanel panel_2 = new JPanel();
			panel_2.setBackground(Color.WHITE);
			panel_1.add(panel_2);
				panel_2.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
				
				JPanel panel_4 = new JPanel();
				panel_4.setBackground(Color.WHITE);
				FlowLayout flowLayout = (FlowLayout) panel_4.getLayout();
				flowLayout.setAlignment(FlowLayout.LEFT);
				panel_2.add(panel_4);
				
				prevButton = new JButton();
				prevButton.setContentAreaFilled(false);
				prevButton.setFocusPainted(false);
				prevButton.setFocusable(false);
				prevButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				panel_4.add(prevButton);
				prevButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						JButton button = (JButton)e.getSource();
						JPanel buttonsPanel = (JPanel)button.getParent();
						TutorialView view = (TutorialView) buttonsPanel.getParent().getParent().getParent().getParent();
						JPanel cardsPanel = (JPanel)view.getComponent(1);
						CardLayout layout = (CardLayout) cardsPanel.getLayout();
						layout.previous(cardsPanel);
						view.registerCardChange(-1);
					}
				});
				prevButton.setBorder(null);
				prevButton.setBackground(Color.WHITE);
				prevButton.setIcon(previous);
				prevButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseEntered(MouseEvent e) {
						JButton button = (JButton)e.getSource();
						if(button.isEnabled()) {
							button.setIcon(previous2);
						}
					}
					@Override
					public void mouseExited(MouseEvent e) {
						JButton button = (JButton)e.getSource();
						if(button.isEnabled()) {
							button.setIcon(previous);
						}
					}
				});
				prevButton.setEnabled(false);
				
				JLabel lblSep = new JLabel("");
				lblSep.setIcon(sepImage);
				panel_4.add(lblSep);
				
				nextButton = new JButton();
				nextButton.setContentAreaFilled(false);
				nextButton.setFocusPainted(false);
				nextButton.setFocusable(false);
				nextButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				panel_4.add(nextButton);
				nextButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						JButton button = (JButton)e.getSource();
						JPanel buttonsPanel = (JPanel)button.getParent();
						TutorialView view = (TutorialView) buttonsPanel.getParent().getParent().getParent().getParent();
						JPanel cardsPanel = (JPanel)view.getComponent(1);
						CardLayout layout = (CardLayout) cardsPanel.getLayout();
						layout.next(cardsPanel);
						view.registerCardChange(1);
					}
				});
				
					nextButton.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseEntered(MouseEvent e) {
							JButton button = (JButton)e.getSource();
							if(button.isEnabled()) {
								button.setIcon(next2);
							}
						}
						@Override
						public void mouseExited(MouseEvent e) {
							JButton button = (JButton)e.getSource();
							if(button.isEnabled()) {
								button.setIcon(next);
							}
						}
					});
					nextButton.setBorder(null);
					nextButton.setIcon(next);
					nextButton.setBackground(Color.WHITE);
					
					JLabel figure = new JLabel("");
					figure.setHorizontalAlignment(SwingConstants.LEFT);
					panel_2.add(figure);
					figure.setIcon(figureImage);
			
			Component verticalStrut_1 = Box.createVerticalStrut(20);
			verticalStrut_1.setPreferredSize(new Dimension(0, 10));
			panelButtons.add(verticalStrut_1, BorderLayout.NORTH);
			
			Component verticalStrut = Box.createVerticalStrut(20);
			verticalStrut.setPreferredSize(new Dimension(0, 10));
			panelButtons.add(verticalStrut, BorderLayout.SOUTH);
			
			Component horizontalStrut = Box.createHorizontalStrut(20);
			horizontalStrut.setPreferredSize(new Dimension(150, 0));
			add(horizontalStrut, BorderLayout.WEST);
			
			Component horizontalStrut_1 = Box.createHorizontalStrut(20);
			horizontalStrut_1.setPreferredSize(new Dimension(150, 0));
			add(horizontalStrut_1, BorderLayout.EAST);
		backButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				listener.onBackToMenuClicked();
			}
		});
	}
	
	
	protected void registerCardChange(int i) {
		currentCard = currentCard + i;
		if(currentCard == 1) {
			prevButton.setEnabled(false);
			nextButton.setEnabled(true);
			nextButton.setIcon(next);
		} else if (currentCard == panelCards.getComponentCount()) {
			prevButton.setEnabled(true);
			prevButton.setIcon(previous);
			nextButton.setEnabled(false);
		} else {
			prevButton.setEnabled(true);
			nextButton.setEnabled(true);
		}
	}


	public interface ITutorialViewListener {
		public void onBackToMenuClicked();
	}


	public void setListener(ITutorialViewListener listener) {
		this.listener = listener;
	}
}
