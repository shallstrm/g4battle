package se.ninjabunnies.client.view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.List;

import javax.swing.JPanel;

import se.ninjabunnies.client.view.GameplayView.IReceiver;
import se.ninjabunnies.shared.model.IReadonlyBoard;
import se.ninjabunnies.shared.model.IReadonlyFigure;
import se.ninjabunnies.shared.model.IReadonlyTile;
import se.ninjabunnies.shared.util.Position;

/**
 * A simple renderer to draw the board
 * 
 * @author Anton
 * 
 */
@SuppressWarnings("serial")
public class Renderer extends JPanel {

	// The board to draw
	private IReadonlyBoard board;

	// Board's dimensions
	private int tilesPerXAxis;
	private int tilesPerYAxis;

	// Temporary list to store the visual tiles
	private VisualTile[][] listOfVisualTiles;

	// Tiles' general measurements (used when rendering)
	private int w, r, s, t;
	/*
	 * s = side, t = cathetus along the side, w= length between to sides, r =
	 * w/2.
	 */

	// Receiver class to process mouse-clicks
	private IReceiver receiver;

	// Cached traversable tiles
	private List<List<? extends IReadonlyTile>> rings;

	// Cached active tile
	private IReadonlyTile activeTile;

	private int diffX;
	private int diffY;

	private int firstX;
	private int firstY;

	boolean dragging = false;
	boolean caching = false;

	private int lastX = 0;
	private int lastY = 0;

	private int boardPixelsWidth;
	private int pixelsWidth = 0;
	private int pixelsHeight;

	private int boardPixelsHeight;

	public Renderer(IReadonlyBoard board) {

		// Measurements
		w = GameplayView.TILE_WIDTH;
		r = w / 2;
		s = (int) (r / Math.cos(Math.PI / 6));
		t = (int) (s * Math.sin(Math.PI / 6));

		// Set board and initializing variables
		this.board = board;

		tilesPerXAxis = board.getWidth();
		tilesPerYAxis = board.getHeight();

		boardPixelsWidth = board.getWidth() * w + r;

		if (board.getHeight() % 2 == 1) {
			boardPixelsHeight = GameplayView.getHeight(w)
					* (tilesPerYAxis / 2 + 1) + (tilesPerYAxis / 2)
					* (int) ((w / 2) / Math.cos(Math.PI / 6));
		} else {
			boardPixelsHeight = GameplayView.getHeight(w) * (tilesPerYAxis / 2)
					+ ((tilesPerYAxis / 2) * s) + t;
		}

		listOfVisualTiles = new VisualTile[tilesPerXAxis][tilesPerYAxis];

	}

	/**
	 * 
	 * @param receiver
	 *            The receiver which process the mouse-clicks.
	 */
	public void setReceiver(IReceiver receiver) {
		this.receiver = receiver;
		// Initializing listener
		MouseListener listener = new MouseListener();
		addMouseListener(listener);
		MoitionListener motionListener = new MoitionListener();
		addMouseMotionListener(motionListener);
		RightMotion rightMotion = new RightMotion();
		addMouseMotionListener(rightMotion);
		RightClicker rightClicker = new RightClicker();
		addMouseListener(rightClicker);
	}

	/**
	 * Draws the board.
	 */
	public void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		super.paintComponent(g2);

		this.setBackground(Color.WHITE);

		// draw board
		for (int i = 0; i < tilesPerYAxis; i++) {
			for (int j = 0; j < tilesPerXAxis; j++) {

				int x = j * w + Math.abs(i % 2) * w / 2;
				int y = i * (s + t);

				IReadonlyTile tile = board.getTile(new Position(j, i));

				/*
				 * System.out.println(""+getBounds().width);
				 * System.out.println((boardPixelsWidth));
				 * System.out.println((getWidth()-boardPixelsWidth));
				 */

				if (dragging) {

					// Store the visual tiles
					listOfVisualTiles[j][i] = new VisualTile(w, new Point(x
							+ lastX + diffX, y + lastY + diffY), tile,
							getHintingLevel(tile), activeTile, j, i);

				} else {

					// Store the visual tiles
					listOfVisualTiles[j][i] = new VisualTile(w, new Point(x
							+ lastX, y + lastY), tile, getHintingLevel(tile),
							activeTile, j, i);

				}

				// Draw a specific tile
				listOfVisualTiles[j][i].drawTile(g2);

			}
		}
	}

	public void updateActiveFigureProperties(IReadonlyFigure figure) {
		activeTile = board.getTile(figure);
		rings = activeTile.getNeighborRings(figure.getMoveCondition(),
				figure.distanceAbleToMove());
	}

	private double getHintingLevel(IReadonlyTile tile) {
		for (int i = 0; i < rings.size(); i++) {

			if (rings.get(i).contains(tile)) {
				return 0.5;
			}

		}

		return 0;
	}

	private class RightMotion extends MouseMotionAdapter {

		public void mouseDragged(MouseEvent e) {

			moveMap(e);
		}
	}

	private class RightClicker extends MouseAdapter {

		public void mousePressed(MouseEvent e) {
			if (e.isMetaDown()) {
				firstX = (int) e.getPoint().getX();
				firstY = (int) e.getPoint().getY();
			}
		}

		public void mouseReleased(MouseEvent e) {
			if (e.isMetaDown() && caching) {
				caching = false;

				lastX = lastX + diffX;

				lastY = lastY + diffY;
				repaint();
				dragging = false;
			}
		}
	}

	private class MouseListener extends MouseAdapter {

		public void mousePressed(MouseEvent e) {

			if (!e.isMetaDown()) {
				// Find which tile was clicked.
				// Loop through the list of visual tiles
				for (int i = 0; i < tilesPerYAxis; i++) {
					for (int j = 0; j < tilesPerXAxis; j++) {

						// If the point intersect with the tile
						if (listOfVisualTiles[j][i].intersects(e.getPoint())) {

							// The receiver processes the mouse-click.
							receiver.mouseClicked(new Position(j, i));
							repaint();
							return;
						}

					}
				}
			}

		}
	}

	private class MoitionListener extends MouseMotionAdapter {

		public void mouseMoved(MouseEvent e) {

			boolean noTileFound = true;

			// Find which tile was clicked.
			// Loop through the list of visual tiles
			for (int i = 0; i < tilesPerYAxis; i++) {
				for (int j = 0; j < tilesPerXAxis; j++) {

					// If the point intersect with the tile
					try {
						if (listOfVisualTiles[j][i].intersects(e.getPoint())) {

							noTileFound = false;

							// The receiver processes the mouse-click.
							receiver.mouseMoved(new Position(j, i));
							repaint();
							return;
						}
					} catch (NullPointerException ex) {
						// Do nothing
					}

				}
			}
			if (noTileFound) {
				receiver.mouseMoved(null);
			}
		}

		public void mouseDragged(MouseEvent e) {

			moveMap(e);

		}

	}

	public void sendWidth(int i) {

		if (pixelsWidth != i) {
			lastX = 0;
			diffX = 0;
			repaint();
			pixelsWidth = i;
		}

	}

	public void sendHight(int height) {
		

		if (pixelsHeight != height) {
			lastY = 0;
			diffY = 0;
			repaint();
			pixelsHeight = height;
		}
	}

	private void moveMap(MouseEvent e) {

		caching = true;

		if (e.isMetaDown()) {

			int mini;
			if (pixelsWidth - boardPixelsWidth < 0) {
				mini = pixelsWidth - boardPixelsWidth;

				diffX = (int) e.getPoint().getX() - firstX;

				if (diffX + lastX < mini) {
					diffX = 0;
					lastX = mini;

				}

				if (diffX + lastX > 0) {
					diffX = 0;
					lastX = 0;

				}

			} else {

				diffX = (int) e.getPoint().getX() - firstX;

				if (diffX + lastX < 0) {
					diffX = 0;
					lastX = 0;

				}

				if (diffX + lastX > pixelsWidth - boardPixelsWidth) {
					diffX = 0;
					lastX = pixelsWidth - boardPixelsWidth;

				}

			}

			if (pixelsHeight - boardPixelsHeight < 0) {

				mini = pixelsHeight - boardPixelsHeight;

				diffY = (int) e.getPoint().getY() - firstY;

				if (diffY + lastY < mini) {
					diffY = 0;
					lastY = mini;

				}

				if (diffY + lastY > 0) {
					diffY = 0;
					lastY = 0;

				}

			} else {

				diffY = (int) e.getPoint().getY() - firstY;

				if (diffY + lastY < 0) {
					diffY = 0;
					lastY = 0;

				}

				if (diffY + lastY > pixelsHeight - boardPixelsHeight) {
					diffY = 0;
					lastY = pixelsHeight - boardPixelsHeight;

				}

			}

			dragging = true;
			repaint();
		}
		repaint();
	}

}
