package se.ninjabunnies.client.view;

import java.awt.Component;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import se.ninjabunnies.shared.util.ResourceManager;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JSplitPane;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Cursor;

/**
 * View for setting up a new game. Can set up hotseat and network games.
 * @author Serade
 *
 */
@SuppressWarnings("serial")
public class LobbyView extends JPanel {
	private ILobbyViewListener listener;
	private MapPanel map;
	private PlayersPanel players;
	private ImageIcon backToMenu = new ImageIcon(ResourceManager.getInstance().getImageResource("lobby/BackToMenu.png"));
	private ImageIcon backToMenu2 = new ImageIcon(ResourceManager.getInstance().getImageResource("lobby/BackToMenu_2.png"));
	private ImageIcon startGame = new ImageIcon(ResourceManager.getInstance().getImageResource("lobby/StartGame.png"));
	private ImageIcon startGame2 = new ImageIcon(ResourceManager.getInstance().getImageResource("lobby/StartGame_2.png"));
	private ImageIcon starImage = new ImageIcon(ResourceManager.getInstance().getImageResource("lobby/Star.png"));
	private ImageIcon lobbyImage = new ImageIcon(ResourceManager.getInstance().getImageResource("lobby/Lobby.png"));
	
	public LobbyView(boolean isHotseat) {
		setBorder(null);
		setBackground(Color.WHITE);
		setLayout(new BorderLayout(0, 0));
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(Color.WHITE);
		add(panel_2, BorderLayout.NORTH);
		panel_2.setLayout(new BorderLayout(0, 0));
		
		JLabel lblLobby = new JLabel("");
		lblLobby.setHorizontalAlignment(SwingConstants.CENTER);
		panel_2.add(lblLobby);
		lblLobby.setIcon(lobbyImage);
		
		Component verticalStrut_2 = Box.createVerticalStrut(20);
		verticalStrut_2.setPreferredSize(new Dimension(0, 40));
		panel_2.add(verticalStrut_2, BorderLayout.SOUTH);
		
		JPanel panelButtons = new JPanel();
		panelButtons.setBorder(null);
		panelButtons.setBackground(Color.WHITE);
		this.add(panelButtons, BorderLayout.SOUTH);
		panelButtons.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(null);
		panel_1.setBackground(Color.WHITE);
		panelButtons.add(panel_1);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_3 = new JPanel();
		panel_3.setBorder(null);
		panel_3.setBackground(Color.WHITE);
		panel_1.add(panel_3, BorderLayout.CENTER);
		panel_3.setLayout(new GridLayout(0, 3, 0, 0));
		
		final JButton btnBack = new JButton();
		btnBack.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnBack.setHorizontalTextPosition(SwingConstants.CENTER);
		panel_3.add(btnBack);
		btnBack.setContentAreaFilled(false);
		btnBack.setBorderPainted(false);
		btnBack.setFocusPainted(false);
		btnBack.setFocusable(false);
		btnBack.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				btnBack.setIcon(backToMenu2);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnBack.setIcon(backToMenu);
			}
		});
		btnBack.setIcon(backToMenu);
		btnBack.setBorder(null);
		btnBack.setBackground(Color.WHITE);
		
		JLabel star = new JLabel("");
		star.setVisible(false);
		star.setBorder(null);
		panel_3.add(star);
		star.setHorizontalAlignment(SwingConstants.CENTER);
		star.setIcon(starImage);
		
		final JButton btnStartGame = new JButton();
		btnStartGame.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnStartGame.setHorizontalTextPosition(SwingConstants.CENTER);
		panel_3.add(btnStartGame);
		btnStartGame.setFocusPainted(false);
		btnStartGame.setFocusable(false);
		btnStartGame.setContentAreaFilled(false);
		btnStartGame.setBorderPainted(false);
		btnStartGame.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnStartGame.setIcon(startGame2);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnStartGame.setIcon(startGame);
			}
		});
		btnStartGame.setIcon(startGame);
		btnStartGame.setBorder(null);
		btnStartGame.setBackground(Color.WHITE);
		btnStartGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				listener.onStartGameClicked();
			}
		});
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				listener.onBackToMenuClicked();
			}
		});
		
		Component verticalStrut = Box.createVerticalStrut(20);
		verticalStrut.setPreferredSize(new Dimension(0, 10));
		panelButtons.add(verticalStrut, BorderLayout.SOUTH);
		
		Component verticalStrut_1 = Box.createVerticalStrut(20);
		verticalStrut_1.setPreferredSize(new Dimension(0, 10));
		panelButtons.add(verticalStrut_1, BorderLayout.NORTH);
		
		JPanel panel = new JPanel();
		panel.setBorder(null);
		add(panel, BorderLayout.CENTER);
		panel.setLayout(new BorderLayout(0, 0));
		
		JSplitPane splitPane = new JSplitPane();
		splitPane.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		splitPane.setBorder(null);
		splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		panel.add(splitPane);
		
		map = new MapPanel();
		map.setBorder(null);
		map.setBackground(Color.WHITE);
		splitPane.setLeftComponent(map);
		
		players = new PlayersPanel(isHotseat);
		players.setBorder(null);
		splitPane.setRightComponent(players);
		players.setBackground(Color.WHITE);
	}
	
	public interface ILobbyViewListener {
		public void onStartGameClicked();
		public void onBackToMenuClicked();
	}

	/**
	 * @return the listener
	 */
	public ILobbyViewListener getViewListener() {
		return listener;
	}

	/**
	 * @param listener the listener to set
	 */
	public void setViewListener(ILobbyViewListener listener) {
		this.listener = listener;
	}

	/**
	 * Returns the MapPanel from the lobby.
	 * @return the MapPanel
	 */
	public MapPanel getMapPanel() {
		return map;
	}

	/**
	 * Returns the PlayersPanel form the lobby.
	 * @return the PlayersPanel
	 */
	public PlayersPanel getPlayersPanel() {
		return players;
	}
	
}
