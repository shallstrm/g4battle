package se.ninjabunnies.client.view;

import javax.swing.JPanel;
import javax.swing.BoxLayout;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import se.ninjabunnies.shared.util.GameConstants;
import se.ninjabunnies.shared.util.MapDescriptorTemplate;
import se.ninjabunnies.shared.util.ResourceManager;

import java.awt.Color;
import java.io.IOException;
import java.util.ArrayList;
import java.awt.Dimension;
import javax.swing.JLabel;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.SystemColor;

/**
 * View for edeting players to be in a game during settup.
 * @author Serade
 *
 */
@SuppressWarnings("serial")
public class PlayersPanel extends JPanel {
	JPanel playersPanel;
	Boolean isHotseat = true;
	
	public PlayersPanel(boolean isHotseat) {
		setBackground(Color.WHITE);
		setBorder(null);
		setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBorder(null);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		add(scrollPane, BorderLayout.CENTER);
		
		playersPanel = new JPanel();
		playersPanel.setBorder(null);
		scrollPane.setViewportView(playersPanel);
		playersPanel.setLayout(new BoxLayout(playersPanel, BoxLayout.Y_AXIS));
		
		JPanel panel = new JPanel();
		panel.setBorder(null);
		panel.setBackground(SystemColor.scrollbar);
		panel.setMinimumSize(new Dimension(100, 30));
		panel.setMaximumSize(new Dimension(3000, 30));
		add(panel, BorderLayout.NORTH);
		panel.setLayout(new GridLayout(0, 2, 0, 0));
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(null);
		panel_1.setBackground(SystemColor.scrollbar);
		panel.add(panel_1);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		JLabel lblNewLabel = new JLabel(" Player Name");
		lblNewLabel.setFont(new Font("Verdana", Font.BOLD, 16));
		lblNewLabel.setHorizontalAlignment(SwingConstants.LEFT);
		panel_1.add(lblNewLabel);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(SystemColor.scrollbar);
		panel_2.setBorder(null);
		panel.add(panel_2);
		panel_2.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_3 = new JPanel();
		panel_3.setBorder(null);
		panel_3.setBackground(SystemColor.scrollbar);
		panel_2.add(panel_3);
		panel_3.setLayout(new GridLayout(0, 4, 0, 0));
		
		JLabel lblNewLabel_1 = new JLabel("Team");
		lblNewLabel_1.setFont(new Font("Verdana", Font.BOLD, 16));
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.LEFT);
		panel_3.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Color");
		lblNewLabel_2.setFont(new Font("Verdana", Font.BOLD, 16));
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.LEFT);
		panel_3.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Start Postion");
		lblNewLabel_3.setFont(new Font("Verdana", Font.BOLD, 16));
		lblNewLabel_3.setHorizontalAlignment(SwingConstants.LEFT);
		panel_3.add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Ready");
		lblNewLabel_4.setFont(new Font("Verdana", Font.BOLD, 16));
		lblNewLabel_4.setHorizontalAlignment(SwingConstants.CENTER);
		panel_3.add(lblNewLabel_4);
		
		JButton btnNewButton = new JButton("X");
		btnNewButton.setForeground(SystemColor.scrollbar);
		btnNewButton.setBorderPainted(false);
		btnNewButton.setContentAreaFilled(false);
		btnNewButton.setFocusPainted(false);
		btnNewButton.setFocusable(false);
		btnNewButton.setFont(new Font("Verdana", Font.BOLD, 16));
		panel_2.add(btnNewButton, BorderLayout.EAST);
		
		//add players
		ResourceManager manager = ResourceManager.getInstance();
		MapDescriptorTemplate template = null;
		try {
			template = MapDescriptorTemplate.stringToMapParser(manager.getMapString(manager.getMapNames()[GameConstants.DEFAULT_MAP]));
		} catch (IOException e) {
			e.printStackTrace();
		}
		for(int i = 0; i < template.getPossiblePlayers(); i++) {
			addPlayer();
		}
		
		//make sure hotseat settings are correct
		setHotseat(isHotseat);
	}
	
	/**
	 * The Panel holding the OnePlayerPanels representing the players in the game.
	 * @return the Panel holding the OnePlayerPanels representing the players in the game
	 */
	public JPanel getPlayersPanel () {
		return playersPanel;
	}
	
	/**
	 * Sets the number of player slots in a PlayersPanel
	 * @param panel the PlayersPanel to be modified
	 * @param possiblePlayers the number of player slots to be in PlayersPanel
	 */
	public void setPossiblePlayers(int possiblePlayers) {
		JPanel addingPanel = getPlayersPanel();
		if(addingPanel.getComponentCount() > possiblePlayers) { //we want to remove some
			for(int i = addingPanel.getComponentCount(); i > possiblePlayers; i--) {
				removePlayer();
			}
			revalidate();
		} else if(addingPanel.getComponentCount() < possiblePlayers) { //we want to add some
			for(int i = addingPanel.getComponentCount(); i < possiblePlayers; i++) {
				addPlayer();
			}
			revalidate();
		} //else they are the same and everything is fine
	}

	/**
	 * Adds a OnePlayerPanel to given PlayersPanel. Use this to ensure the panels are added correctly.
	 * @param panel the PlayersPanel to add a OnePlayerPanel to
	 */
	public void addPlayer() {
		PlayerSlot player = new PlayerSlot();
		JPanel addingPanel = getPlayersPanel();
		int count = addingPanel.getComponentCount(); //we use this a couple of times
		if(count < 2) { //the first two are essential
			player.setEsential(true);
		} else {
			player.closePlayerSlot();
		}
		player.setPlayerName("Player " + (count+1));
		player.setHotseat(isHotseat);
		player.setTeam(count);
		player.setStartPosition(count);
		player.setColor(count);
		player.setStartPosition(count);
		addingPanel.add(player);
	}
	
	/**
	 * Removes a OnePlayerPanel to given PlayersPanel. Use this to ensure the panels are removed correctly.
	 * @param panel the PlayersPanel to remove a OnePlayerPanel from
	 */
	public void removePlayer() {
		if(getPlayersPanel().getComponentCount() > 2) { //there must always be 2 players
			getPlayersPanel().remove(getPlayersPanel().getComponentCount()-1);
			repaint();
		}
	}
	
	/**
	 * HotSeat setting makes sure all players are ready as default instead of the other way around.
	 * This reduced the clicks for starting a hotseat game a lot.
	 * @param panel the PlayersPanel for this game
	 * @param hotSeat whether or not it is a hotseat game
	 */
	public void setHotseat(Boolean hotSeat) {
		isHotseat = hotSeat;
		for (Component comp : getPlayersPanel().getComponents()) {
			if (comp.getClass().equals(PlayerSlot.class)) {
				PlayerSlot player = (PlayerSlot) comp;
				player.setHotseat(hotSeat);
			}
		}
	}

	/**
	 * Returns all open <code>PlayerSlot</code>s
	 * @return the open <code>PlayerSlot</code>s
	 */
	public ArrayList<PlayerSlot> getPlayers() {
		ArrayList<PlayerSlot> list = new ArrayList<PlayerSlot>();
		for(Component comp: getPlayersPanel().getComponents()) {
			if(comp.getClass() == PlayerSlot.class) {
				PlayerSlot slot = (PlayerSlot)comp;
				if(slot.isOpen()) {
					list.add(slot);
				}
			}
		}
		return list;
	}

}
