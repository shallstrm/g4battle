package se.ninjabunnies.client.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.RenderingHints;

import se.ninjabunnies.shared.model.IReadonlyFigure;
import se.ninjabunnies.shared.model.IReadonlyPlayer;
import se.ninjabunnies.shared.model.IReadonlyTile;
import se.ninjabunnies.shared.util.ResourceManager;

/**
 * Visualization class of a concrete tile
 * 
 * @author Anton
 * 
 */
public class VisualTile {

	// The concrete tile to visualize
	private IReadonlyTile tile;

	// The polygon (part of the visualization)
	private Polygon poly;

	// Measurements of the polygon
	private int s, t, w, r;
	/*
	 * s = side, t = cathetus along the side, w= length between to sides, r =
	 * w/2.
	 */

	// Location of the polygon
	private Point origin;

	// How strong the hinting is
	private double hintLevel = 0;

	// Cached active tile
	private IReadonlyTile activeTile;

	/**
	 * Constructor
	 * 
	 * @param width
	 *            Width of the visual tile (polygon) in pixels
	 * @param point
	 *            The location of the visual tile
	 * @param tile
	 *            The concrete tile to visualize
	 * @param x
	 *            Position x of the board
	 * @param y
	 *            Position y of the board
	 * @param level
	 *            hinting level
	 * @param activeTile
	 */
	public VisualTile(int width, Point origin, IReadonlyTile tile,
			double level, IReadonlyTile activeTile, int x, int y) {
		setSize(width);
		this.origin = origin;
		this.tile = tile;
		hintLevel = level;
		this.activeTile = activeTile;
		poly = createPolygon(origin);

	}

	/**
	 * Sets the width of the polygon and calculates the remaining variables.
	 * 
	 * @param width
	 *            The width of the polygon
	 */
	private void setSize(int width) {

		w = width;
		r = w / 2;
		s = (int) (r / Math.cos(Math.PI / 6));
		t = (int) (s * Math.sin(Math.PI / 6));
	}

	/**
	 * Creates a polygon using the point of origin and the measurements (s, t,
	 * w, r)
	 * 
	 * @param origin
	 *            Location of the polygon
	 * @return The polygon representing the visuals of a tile
	 */
	public Polygon createPolygon(Point origin) {

		int x = (int) origin.getX();
		int y = (int) origin.getY();

		int[] px;
		int[] py;

		px = new int[] { x, x, x + r, x + r + r, x + r + r, x + r };

		py = new int[] { y + t, y + s + t, y + s + t + t, y + s + t, y + t, y };

		Polygon poly = new Polygon(px, py, 6);

		return poly;

	}

	/**
	 * Controls if a giving point intersects with the polygon
	 * 
	 * @param point
	 *            The giving point
	 * @return
	 */
	public boolean intersects(Point point) {

		return poly.contains(point);
	}

	/**
	 * Draws the tile and in some cases the figure standing on it
	 * 
	 * @param g2
	 *            The pencil
	 */
	public void drawTile(Graphics2D g2) {

		// Set appropriate font
		g2.setFont(new Font("Font", Font.BOLD, 15));

		// The tile's type (part of the path below)
		String tileType = tile.getTileDescriptor().name().toLowerCase();

		// The tile's image
		Image tileImage = ResourceManager.getInstance().getImageResource(
				"tiles/" + tileType + ".png");

		// Draw the image representing the tile
		g2.drawImage(tileImage, (int) origin.getX(), (int) origin.getY(), null);

		if (tile.hasFigure()) {

			IReadonlyFigure figure = tile.getFigure();
			IReadonlyPlayer player = figure.getPlayer();
			// The unit's type (part of the path below)
			String unitType = figure.getFigureDescriptor().name().toLowerCase();
			// Generate a color depending on the player
			String teamColor = player.getColor().toString();
			// The unit's image
			Image unitImage = ResourceManager.getInstance().getImageResource(
					unitType + "/" + teamColor + ".png");
			// Draw the image representing the unit
			g2.drawImage(unitImage, (int) origin.getX() + 17,
					(int) origin.getY() + 23, null);

			// Draw the unit's health
			g2.setColor(Color.GREEN);
			g2.drawString(
					"+" + tile.getFigure().getHealth().getCurrentHealth(),
					(int) origin.getX() + r - 18, (int) origin.getY() + t + 4);

			if (!figure.isUsed()) {
				// Checks if the figure standing on the tile is the active one
				// If it is, fills the tile with a yellowish color
				IReadonlyFigure activeFigure = player.getActiveFigure();
				if (figure == activeFigure) {
					g2.setColor(new Color(255, 255, 0, 100));
					g2.fillPolygon(poly);
				} else if (figure.distanceAbleToMove() == 0) {
					// If the figure is used, fill the tile with a gray color
					g2.setColor(new Color(190, 190, 190, 100));
					g2.fillPolygon(poly);

				}

			} else if (figure.isUsed()) {
				// If the figure is used, fill the tile with a gray color
				g2.setColor(new Color(0, 0, 0, 80));
				g2.fillPolygon(poly);

				if (activeTile.getFigure().canAttackBetween(activeTile, tile)
						&& tile.getFigure().getPlayer().getTeam() != activeTile
								.getFigure().getPlayer().getTeam()) {
					g2.setColor(new Color(255, 0, 0, 80));
					g2.fillPolygon(poly);
				}

			}

		}

		// Draw the polygon which together form a grid
		g2.setColor(Color.BLACK);
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		g2.drawPolygon(poly);

		if (hintLevel > 0) {
			g2.setColor(new Color(255, 255, 0, (int) (100 * hintLevel)));
			g2.fillPolygon(poly);

		}
	}

	public IReadonlyTile getTile() {
		return tile;
	}
}
