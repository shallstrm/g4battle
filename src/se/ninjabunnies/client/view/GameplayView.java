package se.ninjabunnies.client.view;

import java.awt.*;
import javax.swing.*;

import se.ninjabunnies.shared.model.IReadonlyBoard;
import se.ninjabunnies.shared.model.IReadonlyFigure;
import se.ninjabunnies.shared.util.Position;

/**
 * A class to unite the GameModel and the Renderer.
 * 
 * @author Anton
 * 
 */
@SuppressWarnings("serial")
public class GameplayView extends JPanel {
	/** Tile's width in pixels */
	public static final int TILE_WIDTH = 70;

	/** The drawing renderer */
	private Renderer renderer;

	/**
	 * 
	 * 
	 * @param tilesPerSide
	 *            Tiles per side, determines the size of the board.
	 * @param hexWidth
	 *            The size of each polygon, measured from side to side (width)
	 * @param borders
	 *            The size of borders enclosing the board
	 */
	public GameplayView(IReadonlyBoard board) {
		setLayout(new BorderLayout());
		renderer = new Renderer(board);
		add(renderer, BorderLayout.CENTER);

		// Measurements to set renderer's size
		// s = side, t = cathetus along the side, w= length between to sides,
		// r = w/2.
		int w = TILE_WIDTH;
		int r = w / 2;
		int s = (int) (r / Math.cos(Math.PI / 6));
		int t = (int) (s * Math.sin(Math.PI / 6));
		int tilesPerXAxis = board.getWidth();
		int tilesPerYAxis = board.getHeight();

		// Calculate a proper size (in pixels) for the renderer
		int renderSizeX = w * tilesPerXAxis + w / 2;
		int renderSizeY;
		if (tilesPerYAxis % 2 == 1) {
			renderSizeY = getHeight(w) * (tilesPerYAxis / 2 + 1)
					+ (tilesPerYAxis / 2)
					* (int) ((w / 2) / Math.cos(Math.PI / 6));
		} else {
			renderSizeY = getHeight(w) * (tilesPerYAxis / 2)
					+ ((tilesPerYAxis / 2) * s) + t;
		}
		renderer.setPreferredSize(new Dimension(renderSizeX, renderSizeY));
	}

	/**
	 * Calculating the hexagonal height (length between two pointy edge) using
	 * the width (length between two flat surfaces)
	 * 
	 * @param width
	 *            screen width
	 * @return the proper screen height
	 */
	public static int getHeight(int width) {
		int r = width / 2;
		int s = (int) (r / Math.cos(Math.PI / 6));
		int t = (int) (s * Math.sin(Math.PI / 6));
		
		return s + t + t;
	}

	/**
	 * Adds a receiver object to process mouse-clicks.
	 * 
	 * @param receiver
	 */
	public void setReceiver (IReceiver receiver) {
		renderer.setReceiver(receiver);
	}

	public void updateActiveFigureProperties(IReadonlyFigure figure) {
		renderer.updateActiveFigureProperties(figure);
	}

	/**
	 * Draws the board.
	 */
	public void paintComponent(Graphics g) {
		renderer.sendWidth(getWidth() - 200); // width of the side panel	
		renderer.sendHight(getHeight());
	}

	/**
	 * Inner interface...
	 * 
	 * @author Anton
	 * 
	 */
	public interface IReceiver {
		void mouseClicked(Position pos);
		void mouseMoved(Position pos);
	}
}