package se.ninjabunnies.client.view;

import javax.swing.JPanel;
import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import se.ninjabunnies.shared.util.ResourceManager;
import java.awt.BorderLayout;
import javax.swing.SwingConstants;

@SuppressWarnings("serial")
public class LoadingGameView extends JPanel {
	public LoadingGameView() {
		setBackground(Color.WHITE);
		setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		add(panel, BorderLayout.CENTER);
		panel.setLayout(new BorderLayout(0, 0));
		
		JLabel lblLoading = new JLabel("");
		panel.add(lblLoading);
		lblLoading.setHorizontalAlignment(SwingConstants.CENTER);
		lblLoading.setIcon(new ImageIcon(ResourceManager.getInstance().getImageResource("loading/warp.png")));
		lblLoading.setBackground(Color.WHITE);
	}

}
