package se.ninjabunnies.client.view;

import javax.swing.JPanel;
import java.awt.BorderLayout;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;

import se.ninjabunnies.shared.model.IReadonlyFigure;
import se.ninjabunnies.shared.model.IReadonlyPlayer;
import se.ninjabunnies.shared.model.IReadonlyTile;
import se.ninjabunnies.shared.util.ResourceManager;

import java.awt.GridLayout;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.UIManager;
import javax.swing.border.MatteBorder;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.font.TextAttribute;
import java.util.HashMap;
import java.util.Map;
import java.awt.event.ActionListener;
import java.awt.Cursor;

@SuppressWarnings("serial")
public class SidePanel extends JPanel {
	private ImageIcon endTurn = new ImageIcon(ResourceManager.getInstance()
			.getImageResource("side_panel/newEndTurnStart.png"));
	private ImageIcon endTurn2 = new ImageIcon(ResourceManager.getInstance()
			.getImageResource("side_panel/newEndTurn.png"));
	private ImageIcon endTurn3 = new ImageIcon(ResourceManager.getInstance()
			.getImageResource("side_panel/newEndTurn_2.png"));

	private JLabel aName;
	private JLabel aHealth;
	private JLabel aArmor;
	private JLabel aMovement;
	private JLabel aWName;
	private JLabel aWDamage;
	private JLabel aWSpeed;
	private JLabel aWRange;
	private JLabel image;
	private JLabel player;
	private JLabel tileLabel;
	private JLabel cursorFigureImage;
	private JLabel cursorFigureName;
	private JLabel cursorFigureHealth;
	private JLabel cursorFigureMovement;
	private JLabel cursorFigureWeapon;
	private JLabel cursorFigureDamage;
	private JLabel cursorFigureSpeed;
	private JLabel cursorFigureRange;
	private JLabel cursorFigureArmor;
	private JPanel activeFigurePanel;
	private JPanel cursorFigurePanel;
	private JButton endTurnButon;
	private JLabel secondPlayer;
	private JButton endGameButton;
	private JLabel aSecondName;
	private JLabel cursorFigureSecondName;

	public SidePanel() {
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				cursorOutOfBounds();
			}
		});
		setLayout(new BorderLayout(0, 0));

		JPanel panel = new JPanel();
		panel.setBackground(UIManager.getColor("Button.background"));
		add(panel, BorderLayout.NORTH);
		panel.setLayout(new BorderLayout(0, 0));

		endGameButton = new JButton("END GAME");
		endGameButton.addMouseListener(new MouseAdapter() {

			Font original;

			@Override
			public void mouseEntered(MouseEvent arg0) {
				original = endGameButton.getFont();
				Map<TextAttribute, ?> attributes = original.getAttributes();
				Map<TextAttribute, Object> newAttributes =
						new HashMap<TextAttribute, Object>(attributes);
				newAttributes.put(TextAttribute.UNDERLINE,
						(Object) TextAttribute.UNDERLINE_ON);
				endGameButton.setFont(original.deriveFont(newAttributes));
				endGameButton.setForeground(Color.RED);
			}

			@Override
			public void mouseExited(MouseEvent e) {
				endGameButton.setFont(original);
				endGameButton.setForeground(Color.BLACK);
			}
		});
		endGameButton.setContentAreaFilled(false);
		endGameButton.setBorderPainted(false);
		endGameButton.setBackground(UIManager.getColor("Button.background"));
		endGameButton.setFont(new Font("Tahoma", Font.BOLD, 13));
		panel.add(endGameButton, BorderLayout.EAST);

		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		add(horizontalStrut_1, BorderLayout.EAST);

		Component horizontalStrut = Box.createHorizontalStrut(20);
		add(horizontalStrut, BorderLayout.WEST);

		JPanel panel_1 = new JPanel();
		add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new GridLayout(3, 0, 0, 0));

		JPanel active = new JPanel();
		active.setBorder(new MatteBorder(0, 0, 10, 0, (Color) UIManager
				.getColor("Button.background")));
		active.setBackground(UIManager.getColor("Button.background"));
		panel_1.add(active);
		active.setLayout(new BorderLayout(0, 0));

		activeFigurePanel = new JPanel();
		active.add(activeFigurePanel);
		activeFigurePanel.setLayout(new GridLayout(2, 2, 0, 0));

		JPanel panel_3 = new JPanel();
		activeFigurePanel.add(panel_3);
		panel_3.setBackground(Color.WHITE);
		panel_3.setLayout(new GridLayout(0, 2, 0, 0));

		JPanel panel_4 = new JPanel();
		panel_4.setBackground(Color.WHITE);
		panel_3.add(panel_4);
		panel_4.setLayout(new BorderLayout(0, 0));

		image = new JLabel("");
		image.setHorizontalAlignment(SwingConstants.CENTER);
		panel_4.add(image);

		JPanel panel_5 = new JPanel();
		panel_5.setBackground(Color.WHITE);
		panel_3.add(panel_5);
		panel_5.setLayout(new BorderLayout(0, 0));

		JPanel panel_18 = new JPanel();
		panel_18.setBackground(Color.WHITE);
		panel_5.add(panel_18, BorderLayout.NORTH);
		panel_18.setLayout(new BorderLayout(0, 0));

		aSecondName = new JLabel("");
		panel_18.add(aSecondName, BorderLayout.SOUTH);

		aName = new JLabel("unit");
		panel_18.add(aName, BorderLayout.CENTER);

		aHealth = new JLabel("health");
		panel_5.add(aHealth, BorderLayout.SOUTH);

		aMovement = new JLabel("movement");
		panel_5.add(aMovement, BorderLayout.CENTER);

		JPanel panel_2 = new JPanel();
		activeFigurePanel.add(panel_2);
		panel_2.setBackground(Color.WHITE);
		panel_2.setLayout(new GridLayout(5, 2, 0, 0));

		JLabel lblNewLabel_1 = new JLabel("Armor: ");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.RIGHT);
		panel_2.add(lblNewLabel_1);

		aArmor = new JLabel("armor");
		panel_2.add(aArmor);

		JLabel lblNewLabel_4 = new JLabel("Weapon: ");
		lblNewLabel_4.setHorizontalAlignment(SwingConstants.RIGHT);
		panel_2.add(lblNewLabel_4);

		aWName = new JLabel("weapon");
		panel_2.add(aWName);

		JLabel lblNewLabel_11 = new JLabel("Damage: ");
		lblNewLabel_11.setHorizontalAlignment(SwingConstants.RIGHT);
		panel_2.add(lblNewLabel_11);

		aWDamage = new JLabel("damage");
		panel_2.add(aWDamage);

		JLabel lblNewLabel_7 = new JLabel("Speed: ");
		lblNewLabel_7.setHorizontalAlignment(SwingConstants.RIGHT);
		panel_2.add(lblNewLabel_7);

		aWSpeed = new JLabel("speed");
		panel_2.add(aWSpeed);

		JLabel lblNewLabel_9 = new JLabel("Range: ");
		lblNewLabel_9.setHorizontalAlignment(SwingConstants.RIGHT);
		panel_2.add(lblNewLabel_9);

		aWRange = new JLabel("range");
		panel_2.add(aWRange);

		JPanel panel_6 = new JPanel();
		active.add(panel_6, BorderLayout.NORTH);
		panel_6.setLayout(new BorderLayout(0, 0));

		JLabel lblActive = new JLabel("Active");
		lblActive.setFont(new Font("Tahoma", Font.BOLD, 15));
		panel_6.add(lblActive, BorderLayout.NORTH);

		JPanel panel_17 = new JPanel();
		panel_17.setBackground(Color.WHITE);
		panel_6.add(panel_17, BorderLayout.SOUTH);
		panel_17.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		player = new JLabel("player");
		panel_17.add(player);
		player.setFont(new Font("Tahoma", Font.BOLD, 13));

		JPanel panel_15 = new JPanel();
		panel_1.add(panel_15);
		panel_15.setBackground(UIManager.getColor("Button.background"));
		panel_15.setLayout(new BorderLayout(0, 0));

		JPanel panel_16 = new JPanel();
		panel_15.add(panel_16, BorderLayout.CENTER);
		panel_16.setLayout(new BorderLayout(0, 0));

		JPanel tilePanel = new JPanel();
		tilePanel.setBackground(Color.WHITE);
		panel_16.add(tilePanel, BorderLayout.CENTER);
		tilePanel.setLayout(new BorderLayout(0, 0));

		tileLabel = new JLabel("tileLabel");
		tileLabel.setFont(new Font("Tahoma", Font.BOLD, 11));
		tileLabel.setBackground(Color.WHITE);
		tileLabel.setForeground(Color.WHITE);
		tileLabel.setIconTextGap(20);
		tileLabel.setHorizontalTextPosition(SwingConstants.CENTER);
		tilePanel.add(tileLabel);
		tileLabel.setHorizontalAlignment(SwingConstants.CENTER);

		JPanel panel_9 = new JPanel();
		panel_15.add(panel_9, BorderLayout.NORTH);
		panel_9.setLayout(new BorderLayout(0, 0));

		JLabel lblCursor = new JLabel("Tile");
		panel_9.add(lblCursor, BorderLayout.NORTH);
		lblCursor.setForeground(Color.BLACK);
		lblCursor.setBackground(UIManager.getColor("Button.background"));
		lblCursor.setFont(new Font("Tahoma", Font.BOLD, 15));

		JPanel cursor = new JPanel();
		cursor.setBackground(UIManager.getColor("Button.background"));
		panel_1.add(cursor);
		cursor.setLayout(new BorderLayout(0, 0));

		JPanel panel_7 = new JPanel();
		cursor.add(panel_7, BorderLayout.CENTER);
		panel_7.setLayout(new BorderLayout(0, 0));

		cursorFigurePanel = new JPanel();
		cursorFigurePanel.setBorder(new MatteBorder(0, 0, 10, 0,
				(Color) UIManager.getColor("Button.background")));
		cursorFigurePanel.setBackground(Color.WHITE);
		panel_7.add(cursorFigurePanel);
		cursorFigurePanel.setLayout(new BorderLayout(0, 0));

		JPanel panel_13 = new JPanel();
		cursorFigurePanel.add(panel_13);
		panel_13.setLayout(new GridLayout(2, 2, 0, 0));

		JPanel panel_8 = new JPanel();
		panel_13.add(panel_8);
		panel_8.setBackground(Color.WHITE);
		panel_8.setLayout(new GridLayout(1, 2, 0, 0));

		JPanel panel_10 = new JPanel();
		panel_10.setBackground(Color.WHITE);
		panel_8.add(panel_10);
		panel_10.setLayout(new BorderLayout(0, 0));

		cursorFigureImage = new JLabel("");
		panel_10.add(cursorFigureImage, BorderLayout.CENTER);
		cursorFigureImage.setHorizontalTextPosition(SwingConstants.CENTER);
		cursorFigureImage.setHorizontalAlignment(SwingConstants.CENTER);

		JPanel panel_11 = new JPanel();
		panel_11.setBackground(Color.WHITE);
		panel_8.add(panel_11);
		panel_11.setLayout(new BorderLayout(0, 0));

		cursorFigureHealth = new JLabel("");
		panel_11.add(cursorFigureHealth, BorderLayout.SOUTH);

		cursorFigureMovement = new JLabel("");
		panel_11.add(cursorFigureMovement, BorderLayout.CENTER);

		JPanel panel_19 = new JPanel();
		panel_19.setBackground(Color.WHITE);
		panel_11.add(panel_19, BorderLayout.NORTH);
		panel_19.setLayout(new BorderLayout(0, 0));

		cursorFigureName = new JLabel("");
		panel_19.add(cursorFigureName, BorderLayout.CENTER);

		cursorFigureSecondName = new JLabel("");
		panel_19.add(cursorFigureSecondName, BorderLayout.SOUTH);

		JPanel panel_14 = new JPanel();
		panel_13.add(panel_14);
		panel_14.setBackground(Color.WHITE);
		panel_14.setLayout(new GridLayout(5, 2, 0, 0));

		JLabel lblNewLabel_2 = new JLabel("Armor: ");
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.RIGHT);
		panel_14.add(lblNewLabel_2);

		cursorFigureArmor = new JLabel("");
		panel_14.add(cursorFigureArmor);

		JLabel lblNewLabel_8 = new JLabel("Weapon: ");
		lblNewLabel_8.setHorizontalAlignment(SwingConstants.RIGHT);
		panel_14.add(lblNewLabel_8);

		cursorFigureWeapon = new JLabel("");
		panel_14.add(cursorFigureWeapon);

		JLabel lblNewLabel_12 = new JLabel("Damage: ");
		lblNewLabel_12.setHorizontalAlignment(SwingConstants.RIGHT);
		panel_14.add(lblNewLabel_12);

		cursorFigureDamage = new JLabel("");
		panel_14.add(cursorFigureDamage);

		JLabel lblNewLabel_13 = new JLabel("Speed: ");
		lblNewLabel_13.setHorizontalAlignment(SwingConstants.RIGHT);
		panel_14.add(lblNewLabel_13);

		cursorFigureSpeed = new JLabel("");
		panel_14.add(cursorFigureSpeed);

		JLabel lblNewLabel_6 = new JLabel("Range: ");
		lblNewLabel_6.setHorizontalAlignment(SwingConstants.RIGHT);
		panel_14.add(lblNewLabel_6);

		cursorFigureRange = new JLabel("");
		panel_14.add(cursorFigureRange);

		JPanel panel_12 = new JPanel();
		cursorFigurePanel.add(panel_12, BorderLayout.NORTH);
		panel_12.setBackground(Color.WHITE);
		panel_12.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		secondPlayer = new JLabel("");
		panel_12.add(secondPlayer);
		secondPlayer.setFont(new Font("Tahoma", Font.BOLD, 13));

		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setBackground(UIManager.getColor("Button.background"));
		lblNewLabel.setForeground(UIManager.getColor("Button.background"));
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 15));
		panel_7.add(lblNewLabel, BorderLayout.SOUTH);

		endTurnButon = new JButton("");
		endTurnButon.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		endTurnButon.setFocusable(false);
		endTurnButon.setContentAreaFilled(false);
		endTurnButon.setBorderPainted(false);
		endTurnButon.setBackground(Color.WHITE);
		endTurnButon.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				endTurnButon.setIcon(endTurn);
			}

			@Override
			public void mousePressed(MouseEvent e) {
				endTurnButon.setIcon(endTurn3);
			}
			
			@Override
			public void mouseReleased(MouseEvent e) {
				endTurnButon.setIcon(endTurn);
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				endTurnButon.setIcon(endTurn2);
			}
		});
		endTurnButon.setBorder(null);
		endTurnButon.setIcon(endTurn2);
		add(endTurnButon, BorderLayout.SOUTH);

		cursorOutOfBounds();

	}

	public void updateActiveFigure(IReadonlyFigure activeFigure) {

		activeFigurePanel.setVisible(false);
		cursorFigurePanel.setVisible(false);

		IReadonlyPlayer p = activeFigure.getPlayer();

		player.setForeground(new Color(p.getColor().getRgb()));

		player.setText(p.getName() + " of " + p.getTeam().getName());

		String unitType = activeFigure.getFigureDescriptor().name()
				.toLowerCase();

		String teamColor = p.getColor().toString();

		image.setIcon(new ImageIcon(ResourceManager.getInstance()
				.getImageResource(unitType + "/" + teamColor + ".png")));

		String name = activeFigure.getFigureDescriptor().getName();

		if (name.length() >= 12) {
			String[] twoNames = name.split(" ");
			String firstName = twoNames[0] + "-";
			String lastName = twoNames[1];
			aName.setText(firstName);
			aSecondName.setText(lastName);
		} else {
			aName.setText(name);
			aSecondName.setText(null);
		}

		aHealth.setText(activeFigure.getHealth().toString() + " HP");
		aArmor.setText("" + activeFigure.getArmour());
		aMovement.setText("" + activeFigure.distanceAbleToMove() + "/"
				+ activeFigure.getMovement() + " steps");

		aWName.setText(activeFigure.getWeapon().getWeaponName());
		aWDamage.setText("" + activeFigure.getWeapon().getWeaponDamage());
		aWSpeed.setText("" + activeFigure.getWeapon().getWeaponSpeed());
		aWRange.setText("" + activeFigure.getWeapon().getWeaponRange());

		activeFigurePanel.setVisible(true);

	}

	public void updateTempBoth(IReadonlyTile tile) {

		updateTempTileOnly(tile);

		if (tile.hasFigure()) {
			updateTempFigureOnly(tile.getFigure());

		} else {
			cursorFigurePanel.setVisible(false);
		}

	}

	public void updateTempTileOnly(IReadonlyTile tile) {

		cursorFigurePanel.setVisible(false);

		String tileType = tile.getTileDescriptor().name().toLowerCase();

		tileLabel.setText(tile.getTileDescriptor().getName());

		tileLabel.setIcon(new ImageIcon(ResourceManager.getInstance()
				.getImageResource("tiles/" + tileType + ".png")));

	}

	public void updateTempFigureOnly(IReadonlyFigure figure) {

		IReadonlyPlayer p = figure.getPlayer();

		secondPlayer.setForeground(new Color(p.getColor().getRgb()));

		secondPlayer.setText(p.getName() + " of "
				+ p.getTeam().getName());

		String unitType = figure.getFigureDescriptor().name().toLowerCase();

		String teamColor = p.getColor().toString();

		String name = figure.getFigureDescriptor().getName();

		if (name.length() >= 12) {
			String[] twoNames = name.split(" ");
			String firstName = twoNames[0] + "-";
			String lastName = twoNames[1];
			cursorFigureName.setText(firstName);
			cursorFigureSecondName.setText(lastName);
		} else {
			cursorFigureName.setText(name);
			cursorFigureSecondName.setText(null);
		}

		cursorFigureImage.setIcon(new ImageIcon(ResourceManager.getInstance()
				.getImageResource(unitType + "/" + teamColor + ".png")));

		cursorFigureHealth.setText(figure.getHealth().toString() + " HP");
		cursorFigureMovement.setText("" + figure.distanceAbleToMove() + "/"
				+ figure.getMovement() + " steps");
		cursorFigureArmor.setText("" + figure.getArmour());
		cursorFigureDamage.setText("" + figure.getWeapon().getWeaponDamage());
		cursorFigureWeapon.setText(figure.getWeapon().getWeaponName());
		cursorFigureSpeed.setText("" + figure.getWeapon().getWeaponSpeed());
		cursorFigureRange.setText("" + figure.getWeapon().getWeaponRange());

		cursorFigurePanel.setVisible(true);

	}

	public void cursorOutOfBounds() {

		tileLabel.setIcon(new ImageIcon(ResourceManager.getInstance()
				.getImageResource("tiles/notile.png")));
		tileLabel.setText("NO TILE");
		cursorFigurePanel.setVisible(false);

	}

	public void addEndTurnButtonListener(MouseAdapter listener) {

		endTurnButon.addMouseListener(listener);

	}
	
	public void addEndGameButtonActionListener(ActionListener actionListener) {
		
		endGameButton.addActionListener(actionListener);
		
	}

}
