package se.ninjabunnies.client.view;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.JButton;

import java.awt.Cursor;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.JLabel;
import se.ninjabunnies.shared.util.ResourceManager;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.BorderLayout;
import javax.swing.SwingConstants;
import java.awt.FlowLayout;
import javax.swing.border.MatteBorder;
import java.awt.Dimension;

@SuppressWarnings("serial")
public class MenuView extends JPanel {
	private IMenuViewListener listener;
	
	//because we do not want to create the ImageIcons over and over again
	private ImageIcon hotseat = new ImageIcon(ResourceManager.getInstance().getImageResource("menu/Hotseat.png"));
	private ImageIcon hotseat2 = new ImageIcon(ResourceManager.getInstance().getImageResource("menu/Hotseat_2.png"));
	private ImageIcon newGame = new ImageIcon(ResourceManager.getInstance().getImageResource("menu/NewGame.png"));
	private ImageIcon newGame2 = new ImageIcon(ResourceManager.getInstance().getImageResource("menu/NewGame_2.png"));
	private ImageIcon joinGame = new ImageIcon(ResourceManager.getInstance().getImageResource("menu/JoinGame.png"));
	private ImageIcon joinGame2 = new ImageIcon(ResourceManager.getInstance().getImageResource("menu/JoinGame_2.png"));
	private ImageIcon howToPlay = new ImageIcon(ResourceManager.getInstance().getImageResource("menu/HowToPlay.png"));
	private ImageIcon howToPlay2 = new ImageIcon(ResourceManager.getInstance().getImageResource("menu/HowToPlay_2.png"));
	private ImageIcon exitGame = new ImageIcon(ResourceManager.getInstance().getImageResource("menu/ExitGame.png"));
	private ImageIcon exitGame2 = new ImageIcon(ResourceManager.getInstance().getImageResource("menu/ExitGame_2.png"));
	private ImageIcon networkImage = new ImageIcon(ResourceManager.getInstance().getImageResource("menu/network.png"));
	private ImageIcon changeTheme1 = new ImageIcon(ResourceManager.getInstance().getImageResource("menu/ChangeTheme.png"));
	private ImageIcon changeTheme2 = new ImageIcon(ResourceManager.getInstance().getImageResource("menu/ChangeTheme_2.png"));
	
	public MenuView() {
		setBackground(Color.WHITE);
		setLayout(new BorderLayout(0, 0));
		
		JPanel panel_10 = new JPanel();
		panel_10.setBackground(Color.WHITE);
		add(panel_10, BorderLayout.CENTER);
		panel_10.setLayout(new BorderLayout(0, 0));
		
		Component verticalStrut = Box.createVerticalStrut(20);
		panel_10.add(verticalStrut, BorderLayout.NORTH);
		
		JPanel panel_7 = new JPanel();
		panel_10.add(panel_7, BorderLayout.WEST);
		panel_7.setBackground(Color.WHITE);
		panel_7.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_11 = new JPanel();
		panel_7.add(panel_11, BorderLayout.CENTER);
		panel_11.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_4 = new JPanel();
		panel_11.add(panel_4, BorderLayout.CENTER);
		panel_4.setBackground(Color.WHITE);
		panel_4.setLayout(new BoxLayout(panel_4, BoxLayout.Y_AXIS));
		
		JPanel panel_1 = new JPanel();
		panel_4.add(panel_1);
		panel_1.setBackground(Color.WHITE);
		
		//Hotseat
		final JButton btnHotseat = new JButton();
		btnHotseat.setFocusable(false);
		btnHotseat.setHorizontalTextPosition(SwingConstants.CENTER);
		btnHotseat.setContentAreaFilled(false);
		btnHotseat.setBorderPainted(false);
		btnHotseat.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnHotseat.setIcon(hotseat);
		btnHotseat.setBackground(Color.WHITE);
		btnHotseat.setBorder(null);
		btnHotseat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				listener.onHotseatClicked();
			}
		});
		btnHotseat.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnHotseat.setIcon(hotseat2);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnHotseat.setIcon(hotseat);
			}
		});
		panel_1.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		panel_1.add(btnHotseat);
		
		JPanel panel_5 = new JPanel();
		panel_5.setBackground(Color.WHITE);
		panel_4.add(panel_5);
		panel_5.setLayout(new BoxLayout(panel_5, BoxLayout.X_AXIS));
		
		JLabel network = new JLabel("");
		panel_5.add(network);
		network.setHorizontalAlignment(SwingConstants.CENTER);
		network.setVerticalTextPosition(SwingConstants.BOTTOM);
		network.setVerticalAlignment(SwingConstants.BOTTOM);
		network.setHorizontalTextPosition(SwingConstants.CENTER);
		network.setIcon(networkImage);
		
		JPanel panel_2 = new JPanel();
		panel_4.add(panel_2);
		panel_2.setBackground(Color.WHITE);
		
		//Network
		final JButton btnNewGame = new JButton();
		btnNewGame.setContentAreaFilled(false);
		btnNewGame.setBorderPainted(false);
		btnNewGame.setFocusable(false);
		btnNewGame.setHorizontalTextPosition(SwingConstants.CENTER);
		btnNewGame.setCursor(new Cursor(Cursor.HAND_CURSOR));
		btnNewGame.setIcon(newGame);
		btnNewGame.setBackground(Color.WHITE);
		btnNewGame.setBorder(null);
		btnNewGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				listener.onNewGameClicked();
			}
		});
		btnNewGame.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnNewGame.setIcon(newGame2);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnNewGame.setIcon(newGame);
			}
		});
		panel_2.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		panel_2.add(btnNewGame);
		
		JPanel panel_9 = new JPanel();
		panel_4.add(panel_9);
		panel_9.setBackground(Color.WHITE);
		
		final JButton btnJoinGame = new JButton("");
		btnJoinGame.setBorderPainted(false);
		btnJoinGame.setContentAreaFilled(false);
		btnJoinGame.setFocusable(false);
		btnJoinGame.setHorizontalTextPosition(SwingConstants.CENTER);
		btnJoinGame.setCursor(new Cursor(Cursor.HAND_CURSOR));
		btnJoinGame.setBackground(Color.WHITE);
		btnJoinGame.setIcon(joinGame);
		btnJoinGame.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnJoinGame.setIcon(joinGame2);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnJoinGame.setIcon(joinGame);
			}
		});
		btnJoinGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				listener.onJoinGameClicked();
			}
		});
		panel_9.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		btnJoinGame.setBorder(null);
		panel_9.add(btnJoinGame);
		
		JPanel panel_3 = new JPanel();
		panel_4.add(panel_3);
		panel_3.setBackground(Color.WHITE);
		
		//HowToPlay
		final JButton btnTutorial = new JButton();
		btnTutorial.setContentAreaFilled(false);
		btnTutorial.setBorderPainted(false);
		btnTutorial.setFocusPainted(false);
		btnTutorial.setFocusable(false);
		btnTutorial.setHorizontalTextPosition(SwingConstants.CENTER);
		btnTutorial.setCursor(new Cursor(Cursor.HAND_CURSOR));
		btnTutorial.setIcon(howToPlay);
		btnTutorial.setBackground(Color.WHITE);
		btnTutorial.setBorder(null);
		btnTutorial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				listener.onTutorialClicked();
			}
		});
		btnTutorial.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnTutorial.setIcon(howToPlay2);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnTutorial.setIcon(howToPlay);
			}
		});
		panel_3.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		panel_3.add(btnTutorial);
		
		Component horizontalStrut = Box.createHorizontalStrut(20);
		horizontalStrut.setPreferredSize(new Dimension(80, 0));
		panel_7.add(horizontalStrut, BorderLayout.WEST);
		
		JPanel panel = new JPanel();
		panel_7.add(panel, BorderLayout.SOUTH);
		panel.setBackground(Color.WHITE);
		panel.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_13 = new JPanel();
		panel_13.setBackground(Color.WHITE);
		panel.add(panel_13, BorderLayout.WEST);
		
		//Exit Game
		final JButton btnExitGame = new JButton();
		panel_13.add(btnExitGame);
		btnExitGame.setContentAreaFilled(false);
		btnExitGame.setFocusable(false);
		btnExitGame.setCursor(new Cursor(Cursor.HAND_CURSOR));
		btnExitGame.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnExitGame.setIcon(exitGame2);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnExitGame.setIcon(exitGame);
			}
		});
		btnExitGame.setIcon(exitGame);
		btnExitGame.setBackground(Color.WHITE);
		btnExitGame.setBorder(new MatteBorder(0, 5, 5, 1, (Color) Color.WHITE));
		btnExitGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				listener.onExitGameClicked();
			}
		});
		
		JPanel panel_6 = new JPanel();
		panel_10.add(panel_6);
		panel_6.setBackground(Color.WHITE);
		panel_6.setLayout(new BorderLayout(0, 0));
		
		//Picture
		JLabel labelPicture = new JLabel();
		labelPicture.setHorizontalAlignment(SwingConstants.CENTER);
		labelPicture.setBackground(Color.WHITE);
		panel_6.add(labelPicture);
		labelPicture.setIcon(new ImageIcon(ResourceManager.getInstance().getImageResource("menu/rightPicture.png")));
		
		JPanel panel_12 = new JPanel();
		panel_12.setBackground(Color.WHITE);
		panel_6.add(panel_12, BorderLayout.SOUTH);
		panel_12.setLayout(new BorderLayout(0, 0));
		
		final JButton changeTheme = new JButton("");
		changeTheme.setContentAreaFilled(false);
		changeTheme.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		changeTheme.setFocusable(false);
		changeTheme.setBorderPainted(false);
		panel_12.add(changeTheme, BorderLayout.EAST);
		changeTheme.setIcon(changeTheme1);
		changeTheme.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				listener.changeThemeClicked();
				
			}
			@Override
			public void mouseEntered(MouseEvent arg0) {
				changeTheme.setIcon(changeTheme2);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				changeTheme.setIcon(changeTheme1);
			}
		});
		
		JPanel panel_8 = new JPanel();
		add(panel_8, BorderLayout.NORTH);
		panel_8.setBackground(Color.WHITE);
		panel_8.setLayout(new BorderLayout(0, 0));
		
		//Ninja Bunnies Header
		JLabel labelNinjaBunnies = new JLabel();
		labelNinjaBunnies.setIcon(new ImageIcon(ResourceManager.getInstance().getImageResource("menu/NinjaBunnies.png")));
		labelNinjaBunnies.setBackground(Color.WHITE);
		panel_8.add(labelNinjaBunnies, BorderLayout.CENTER);
		
	}
	
	/**
	 * @return the listener
	 */
	public IMenuViewListener getViewListener() {
		return listener;
	}

	/**
	 * @param listener the listener to set
	 */
	public void setViewListener(IMenuViewListener listener) {
		this.listener = listener;
	}

	public interface IMenuViewListener {
		public void onExitGameClicked();
		public void onHotseatClicked();
		public void onNewGameClicked();
		public void onJoinGameClicked();
		public void onTutorialClicked();
		public void changeThemeClicked();
	}



}
