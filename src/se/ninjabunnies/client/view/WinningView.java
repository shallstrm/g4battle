package se.ninjabunnies.client.view;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.BoxLayout;
import javax.swing.JButton;

import se.ninjabunnies.shared.util.ResourceManager;

import java.awt.event.ActionListener;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.SwingConstants;

/**
 * A view showing who won.
 * @author Serade
 *
 */
@SuppressWarnings("serial")
public class WinningView extends JPanel {
	private ImageIcon backToMenu = new ImageIcon(ResourceManager.getInstance().getImageResource("winning/BackToMenu.png"));
	private ImageIcon backToMenu2 = new ImageIcon(ResourceManager.getInstance().getImageResource("winning/BackToMenu_2.png"));
	
	private JLabel lblTeamName;
	private JLabel lblGoodWork;
	private JButton btnBackToMenu;
	
	public WinningView() {
		setBackground(Color.WHITE);
		setLayout(new BorderLayout(0, 0));
		
		JPanel panelText = new JPanel();
		panelText.setBackground(Color.WHITE);
		add(panelText, BorderLayout.CENTER);
		panelText.setLayout(new GridLayout(0, 1, 0, 0));
		
		JPanel panelWinning = new JPanel();
		panelText.add(panelWinning);
		panelWinning.setBackground(Color.WHITE);
		panelWinning.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblCongratulations = new JLabel("Congratulations! ");
		lblCongratulations.setHorizontalTextPosition(SwingConstants.CENTER);
		lblCongratulations.setHorizontalAlignment(SwingConstants.CENTER);
		panelWinning.add(lblCongratulations);
		lblCongratulations.setBackground(Color.WHITE);
		lblCongratulations.setFont(new Font("Verdana", Font.PLAIN, 30));
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.WHITE);
		panelWinning.add(panel_1);
		
		lblTeamName = new JLabel("");
		panel_1.add(lblTeamName);
		lblTeamName.setBackground(Color.WHITE);
		lblTeamName.setFont(new Font("Verdana", Font.PLAIN, 30));
		
		JLabel lblWon = new JLabel(" Won!");
		panel_1.add(lblWon);
		lblWon.setBackground(Color.WHITE);
		lblWon.setFont(new Font("Verdana", Font.PLAIN, 30));
		
		JPanel panelLoosing = new JPanel();
		panelText.add(panelLoosing);
		panelLoosing.setBackground(Color.WHITE);
		panelLoosing.setLayout(new BorderLayout(0, 0));
		
		lblGoodWork = new JLabel("Good work:");
		lblGoodWork.setHorizontalTextPosition(SwingConstants.CENTER);
		lblGoodWork.setHorizontalAlignment(SwingConstants.CENTER);
		panelLoosing.add(lblGoodWork, BorderLayout.CENTER);
		lblGoodWork.setBackground(Color.WHITE);
		lblGoodWork.setFont(new Font("Verdana", Font.PLAIN, 30));
		
		JPanel panelMenuButton = new JPanel();
		add(panelMenuButton, BorderLayout.SOUTH);
		panelMenuButton.setBackground(Color.WHITE);
		panelMenuButton.setLayout(new BorderLayout(0, 0));
		
		btnBackToMenu = new JButton("");
		btnBackToMenu.setFocusPainted(false);
		btnBackToMenu.setFocusable(false);
		btnBackToMenu.setContentAreaFilled(false);
		btnBackToMenu.setBorderPainted(false);
		panelMenuButton.add(btnBackToMenu, BorderLayout.NORTH);
		btnBackToMenu.setBorder(null);
		btnBackToMenu.setBackground(Color.WHITE);
		btnBackToMenu.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnBackToMenu.setIcon(backToMenu2);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnBackToMenu.setIcon(backToMenu);
			}
		});
		btnBackToMenu.setIcon(backToMenu);
		
		JPanel panelPicture = new JPanel();
		panelPicture.setBackground(Color.WHITE);
		add(panelPicture, BorderLayout.EAST);
		panelPicture.setLayout(new BoxLayout(panelPicture, BoxLayout.X_AXIS));
		
		JLabel labelPicture = new JLabel("");
		labelPicture.setBackground(Color.WHITE);
		panelPicture.add(labelPicture);
		labelPicture.setIcon(new ImageIcon(ResourceManager.getInstance().getImageResource("winning/Picture.png")));
		
	}
	
	public void addBackToMenuListener(ActionListener listener) {
		btnBackToMenu.addActionListener(listener);
	}
	
	/**
	 * Sets the text for the winning team.
	 * @param name the name of the winning team
	 */
	public void setWinnerTeam(String name) {
		lblTeamName.setText(name);
	}

	/**
	 * Adds to the text for the loosing teams.
	 * @param name the name of the loosing team to add
	 */
	public void addLooserTeam(String name) {
		lblGoodWork.setText(lblGoodWork.getText()+ " " + name);
	}

}
