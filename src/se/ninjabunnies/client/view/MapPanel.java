package se.ninjabunnies.client.view;

import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.JSplitPane;
import javax.swing.JLabel;
import java.awt.BorderLayout;

import javax.swing.JTextPane;

import se.ninjabunnies.shared.util.GameConstants;
import se.ninjabunnies.shared.util.MapDescriptorTemplate;
import se.ninjabunnies.shared.util.ResourceManager;

import java.awt.Font;
import java.io.IOException;
import java.awt.Color;
import java.awt.GridLayout;
import javax.swing.JList;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.Component;
import javax.swing.Box;
import java.awt.FlowLayout;
import javax.swing.ListSelectionModel;
import javax.swing.border.MatteBorder;
import java.awt.Dimension;
import javax.swing.SwingConstants;
import java.awt.SystemColor;

/**
 * View showing information about a map
 * @author Serade
 *
 */
@SuppressWarnings("serial")
public class MapPanel extends JPanel {
	private MapDescriptorTemplate template = null;
	private JLabel lblMapPicture = new JLabel();
	private JLabel lblSelectedMapName = new JLabel();
	private JTextPane txtPnSelectedMapDescription = new JTextPane();;
	private JLabel lblMapSizeNumber = new JLabel();
	private JLabel lblMaxNumberOfPlayersNumber = new JLabel();
	
	public MapPanel() {
		ResourceManager resourceManager = ResourceManager.getInstance();
		setBackground(Color.WHITE);
		
		//better looking names for map choosing
		String[] mapNames = resourceManager.getMapNames();
		String[] names = new String[mapNames.length];
		for(int i = mapNames.length-1; i >= 0 ;i--) {
			String input = mapNames[i].replace(".map", "");
			names[i] = input.substring(0, 1).toUpperCase() + input.substring(1);
		}
		
		MapDescriptorTemplate[] maps = new MapDescriptorTemplate[mapNames.length];
		for(int i = mapNames.length-1; i >= 0 ;i--) {

			try {
				maps[i]=MapDescriptorTemplate
						.stringToMapParser(resourceManager
								.getMapString(resourceManager.getMapNames()[i]));
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}
		setLayout(new BorderLayout(0, 0));
		
		
		
		JPanel panel_2 = new JPanel();
		add(panel_2);
		panel_2.setLayout(new GridLayout(0, 3, 0, 0));
		
		JPanel panelSelectMap = new JPanel();
		panelSelectMap.setBorder(null);
		panelSelectMap.setBackground(Color.WHITE);
		panel_2.add(panelSelectMap);
		panelSelectMap.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_3 = new JPanel();
		panel_3.setBorder(new MatteBorder(2, 2, 2, 2, (Color) Color.LIGHT_GRAY));
		panel_3.setBackground(Color.WHITE);
		panelSelectMap.add(panel_3, BorderLayout.CENTER);
		panel_3.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(null);
		panel_3.add(panel_1, BorderLayout.CENTER);
		panel_1.setFocusable(false);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		final JList<String> list = new JList<String>();
		list.setBackground(SystemColor.menu);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setCellRenderer(new DefaultListCellRenderer() {
			public int getHorizontalAlignment() {
				return CENTER;
			}
		});
		panel_1.add(list);
		list.setListData(names);
		list.setSelectedIndex(1);
		list.setFont(new Font("Verdana", Font.PLAIN, 16));
		
		JPanel panel_9 = new JPanel();
		panel_9.setBackground(new Color(128, 0, 0));
		panel_9.setBorder(null);
		panel_3.add(panel_9, BorderLayout.NORTH);
		
		JLabel lblMap = new JLabel("Map Name");
		lblMap.setForeground(Color.WHITE);
		panel_9.add(lblMap);
		lblMap.setFont(new Font("Verdana", Font.BOLD, 24));
		list.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				list.repaint();
				setMap((String)list.getSelectedValue() + ".map");
				
			}
		});
		
		Component horizontalStrut = Box.createHorizontalStrut(20);
		panelSelectMap.add(horizontalStrut, BorderLayout.WEST);
		
		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		panelSelectMap.add(horizontalStrut_1, BorderLayout.EAST);
		
		JPanel panelMapPictureBackground = new JPanel();
		panelMapPictureBackground.setBorder(new MatteBorder(2, 2, 2, 2, (Color) Color.LIGHT_GRAY));
		panelMapPictureBackground.setBackground(Color.BLACK);
		panel_2.add(panelMapPictureBackground);
		panelMapPictureBackground.setLayout(new BorderLayout(0, 0));
		panelMapPictureBackground.add(lblMapPicture, BorderLayout.CENTER);
		lblMapPicture.setHorizontalTextPosition(SwingConstants.CENTER);
		lblMapPicture.setHorizontalAlignment(SwingConstants.CENTER);
		lblMapPicture.setRequestFocusEnabled(false);
		lblMapPicture.setFocusable(false);
		lblMapPicture.setBackground(Color.WHITE);
		
		JPanel panelMapInfo = new JPanel();
		panelMapInfo.setBorder(null);
		panel_2.add(panelMapInfo);
		panelMapInfo.setBackground(Color.WHITE);
		panelMapInfo.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setFont(new Font("Verdana", Font.PLAIN, 16));
		panel.setBorder(new MatteBorder(2, 2, 2, 2, (Color) Color.LIGHT_GRAY));
		panelMapInfo.add(panel);
		panel.setLayout(new BorderLayout(0, 0));
		
		JPanel panelMapStatsSurround = new JPanel();
		panelMapStatsSurround.setBorder(null);
		panel.add(panelMapStatsSurround, BorderLayout.SOUTH);
		
		JPanel panelMapStats = new JPanel();
		panelMapStats.setBorder(null);
		panelMapStatsSurround.add(panelMapStats);
		panelMapStats.setLayout(new GridLayout(2, 2, 0, 0));
		
		JPanel panelMapSize = new JPanel();
		panelMapStats.add(panelMapSize);
		panelMapSize.setLayout(new BorderLayout(0, 0));
		
		JLabel lblMapSizeSize = new JLabel("Size: ");
		lblMapSizeSize.setFont(new Font("Verdana", Font.PLAIN, 16));
		lblMapSizeSize.setBackground(Color.WHITE);
		panelMapSize.add(lblMapSizeSize, BorderLayout.EAST);
		
		JPanel lblMaxNumberOfPlayers = new JPanel();
		panelMapStats.add(lblMaxNumberOfPlayers);
		lblMaxNumberOfPlayers.setLayout(new BoxLayout(lblMaxNumberOfPlayers, BoxLayout.X_AXIS));
		lblMaxNumberOfPlayers.add(lblMapSizeNumber);
		
		lblMapSizeNumber.setFont(new Font("Verdana", Font.PLAIN, 16));
		lblMapSizeNumber.setBackground(Color.WHITE);
		
		JLabel lblMapSizeTiles = new JLabel(" Tiles");
		lblMaxNumberOfPlayers.add(lblMapSizeTiles);
		lblMapSizeTiles.setFont(new Font("Verdana", Font.PLAIN, 16));
		lblMapSizeTiles.setBackground(Color.WHITE);
		
		JPanel panel_4 = new JPanel();
		panelMapStats.add(panel_4);
		panel_4.setLayout(new BorderLayout(0, 0));
		
		JLabel lblMaxNumberOfPlayersMaxPlayers = new JLabel("  Max Players: ");
		panel_4.add(lblMaxNumberOfPlayersMaxPlayers, BorderLayout.EAST);
		lblMaxNumberOfPlayersMaxPlayers.setFont(new Font("Verdana", Font.PLAIN, 16));
		lblMaxNumberOfPlayersMaxPlayers.setBackground(Color.WHITE);
		
		JPanel panel_5 = new JPanel();
		panelMapStats.add(panel_5);
		panel_5.setLayout(new BoxLayout(panel_5, BoxLayout.X_AXIS));
		panel_5.add(lblMaxNumberOfPlayersNumber);
		
		lblMaxNumberOfPlayersNumber.setFont(new Font("Verdana", Font.PLAIN, 16));
		lblMaxNumberOfPlayersNumber.setBackground(Color.WHITE);
		
		JLabel lblMaxNumberOfPlayersPlayers = new JLabel(" Players");
		panel_5.add(lblMaxNumberOfPlayersPlayers);
		lblMaxNumberOfPlayersPlayers.setFont(new Font("Verdana", Font.PLAIN, 16));
		lblMaxNumberOfPlayersPlayers.setBackground(Color.WHITE);
		
		JPanel panel_6 = new JPanel();
		panel_6.setBorder(null);
		panel_6.setBackground(new Color(128, 0, 0));
		panel.add(panel_6, BorderLayout.NORTH);
		panel_6.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		lblSelectedMapName.setForeground(Color.WHITE);
		panel_6.add(lblSelectedMapName);
		lblSelectedMapName.setBorder(null);
		lblSelectedMapName.setHorizontalAlignment(SwingConstants.CENTER);
		lblSelectedMapName.setFont(new Font("Verdana", Font.BOLD, 24));
		
		JPanel panel_7 = new JPanel();
		panel_7.setBorder(null);
		panel.add(panel_7, BorderLayout.CENTER);
		panel_7.setLayout(new BorderLayout(0, 0));
		panel_7.add(txtPnSelectedMapDescription);
		txtPnSelectedMapDescription.setBackground(SystemColor.menu);
		txtPnSelectedMapDescription.setBorder(null);
		
		txtPnSelectedMapDescription.setFont(new Font("Verdana", Font.PLAIN, 16));
		txtPnSelectedMapDescription.setEditable(false);
		
		Component horizontalStrut_4 = Box.createHorizontalStrut(20);
		panel_7.add(horizontalStrut_4, BorderLayout.WEST);
		
		Component horizontalStrut_5 = Box.createHorizontalStrut(20);
		panel_7.add(horizontalStrut_5, BorderLayout.EAST);
		
		Component horizontalStrut_2 = Box.createHorizontalStrut(20);
		horizontalStrut_2.setPreferredSize(new Dimension(40, 0));
		panelMapInfo.add(horizontalStrut_2, BorderLayout.EAST);
		
		Component horizontalStrut_3 = Box.createHorizontalStrut(20);
		panelMapInfo.add(horizontalStrut_3, BorderLayout.WEST);
		
		Component verticalStrut = Box.createVerticalStrut(20);
		add(verticalStrut, BorderLayout.SOUTH);
		
		//We want the default map to fill in the blanks
		try {
			template = MapDescriptorTemplate.stringToMapParser(resourceManager.getMapString(resourceManager.getMapNames()[GameConstants.DEFAULT_MAP]));
		} catch (IOException e) {
			e.printStackTrace();
		}
		setMapRepresentation(template);
	}

	/**
	 * Sets which map the MapPanel should represent
	 * @param template the MapDescriptorTemplate describing the map to be represented
	 */
	public void setMapRepresentation(MapDescriptorTemplate template) {
		this.template = template;
		lblMapPicture.setIcon(new ImageIcon(ResourceManager.getInstance().getImageResource("map_pictures/" + template.getMapName().toLowerCase() + ".png")));
		lblSelectedMapName.setText(template.getMapName().replace(".map", ""));
		txtPnSelectedMapDescription.setText(template.getMapDescription());
		lblMapSizeNumber.setText(template.getAmountOfTiles()+"");
		lblMaxNumberOfPlayersNumber.setText(template.getPossiblePlayers()+"");
	}
	
	/**
	 * Sets which map the MapPanel should represent
	 * @param name the name of the map to be represented
	 */
	public void setMap(String name) {
		MapDescriptorTemplate template = null;
		try { //We want the template for the map
			template = MapDescriptorTemplate.stringToMapParser(ResourceManager.getInstance().getMapString(name.toLowerCase()));
		} catch (IOException e) {
			e.printStackTrace();
		}
		setMapRepresentation(template);
		
		JSplitPane splitPane = (JSplitPane) getParent();
		if(splitPane != null){		
			PlayersPanel players = (PlayersPanel) splitPane.getRightComponent();
			players.setPossiblePlayers(template.getPossiblePlayers());
			
		}
	}

	public MapDescriptorTemplate getSelectedMap() {
		return template;
	}

}
