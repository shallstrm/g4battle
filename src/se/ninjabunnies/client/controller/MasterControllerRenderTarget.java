package se.ninjabunnies.client.controller;

import java.awt.Component;

import javax.swing.JFrame;

/**
 * Class to be used by MasterController when supplying real renderers.
 * A render target can be disposed, which means anything rendered to it will be
 * ignored. This can be checked by isDisposed().
 * 
 * @author Joel Severin
 *
 */
class MasterControllerRenderTarget implements IRenderTarget {
	/**
	 * Holds status of disposed or not. Draw calls to disposed render targets
	 * will be ignored. If set to true, it cannot be set back to false again.
	 */
	private boolean disposed = false;
	
	/**
	 * Holds a JFrame to render in. Note that this is a shared instance, such
	 * that it must not be manipulated after disposed is set to true!
	 */
	private JFrame frame;
	
	/**
	 * Construct a master controller render target (default render target).
	 */
	public MasterControllerRenderTarget(JFrame frame) {
		this.frame = frame;
	}
	
	/**
	 * {{@inheritDoc}
	 */
	@Override
	public void renderAWTComponent(Component component) {
		if(!disposed) {
			frame.add(component);
			frame.validate();
		}
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public void setRenderContextTitle(String title) {
		if(!disposed) {
			frame.setTitle(MasterController.GAME_TITLE + " - " + title);
		}
	}

	/**
	 * Dispose the view, effectively making it ignore further draw calls.
	 */
	public void dispose() {
		// Reset frame.
		frame.setTitle(MasterController.GAME_TITLE);
		// Note: Graphics will probably be cached. Run frame.validate() if you
		// have to, but for performance reasons, we don't do that right now.
		// Validate is run after components are added instead.
		frame.getContentPane().removeAll();
		disposed = true;
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public boolean isDisposed() {
		return disposed;
	}
}
