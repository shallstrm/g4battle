package se.ninjabunnies.client.controller;

import se.ninjabunnies.client.view.TutorialView;
import se.ninjabunnies.client.view.TutorialView.ITutorialViewListener;

/**
 * The tutorial controller shows guidance for users about how to play the game.
 * 
 * @author Simone Damaschke
 */
public class TutorialController implements IController {
	@Override
	public void start(IRenderTarget renderTarget) {
		TutorialView view = new TutorialView();
		view.setListener(new TutorialViewListener());
		renderTarget.renderAWTComponent(view);
	}

	@Override
	public void dispose() {
		// NOP
	}
	
	/**
	 * Open this controller.
	 */
	public static void open() {
		MasterController.open(new TutorialController());
	}
	
	public class TutorialViewListener implements ITutorialViewListener {
		@Override
		public void onBackToMenuClicked() {
			MenuController.open();
		}	
	}
}