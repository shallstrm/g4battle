package se.ninjabunnies.client.controller;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.List;

import se.ninjabunnies.client.view.GameplayView;
import se.ninjabunnies.client.view.GameplayView.IReceiver;
import se.ninjabunnies.client.view.SidePanel;
import se.ninjabunnies.client.view.net.NetClientView;
import se.ninjabunnies.client.view.net.NetClientView.NetEventListener;
import se.ninjabunnies.shared.changebus.IChange;
import se.ninjabunnies.shared.changebus.IChangeSet;
import se.ninjabunnies.shared.changebus.IWritableChangeBus;
import se.ninjabunnies.shared.model.IReadonlyBoard;
import se.ninjabunnies.shared.model.IReadonlyFigure;
import se.ninjabunnies.shared.model.IReadonlyGameModel;
import se.ninjabunnies.shared.model.IReadonlyPlayer;
import se.ninjabunnies.shared.model.IReadonlyTeam;
import se.ninjabunnies.shared.model.IReadonlyTile;
import se.ninjabunnies.shared.net.state.EndTurnRequest;
import se.ninjabunnies.shared.net.state.GameModelValid;
import se.ninjabunnies.shared.net.state.LegacyGameModelSync;
import se.ninjabunnies.shared.net.state.TileActionPerformed;
import se.ninjabunnies.shared.util.Position;

/**
 * Controller for actually playing a game that has been started and connected
 * to. Always works with a server (even "hotseated" games are played against a
 * locally hosted game server with some special gameplay characteristics).
 * 
 * @author Joel Severin, Anton Hallin
 */
public class GameController implements IController {
	/** The game model describes the running game. */
	private SwappableGameModel gameModel = new SwappableGameModel();

	/** The client view held to the game server. */
	private NetClientView netClientView;

	/** The render target for GUI rendering. */
	private IRenderTarget renderTarget;

	/** The main game play view controlled by this controller. */
	private GameplayView view;

	/** The side panel (part of the view) to display information */
	private SidePanel sidePanel;

	/** Set to true if the user interface has been constructed yet. */
	private boolean uiConstructed = false;

	/**
	 * Cached position of where the user clicked the last time.
	 */
	private Position lastClicked;

	/**
	 * Cached figure for hinting movements
	 */
	private IReadonlyFigure activeFigure;

	/**
	 * Starts a new game.
	 */
	public void start(IRenderTarget renderTarget) {
		this.renderTarget = renderTarget;

		// Register us for network updates, and tell the server we are ready!
		GameNetEventListener listener = new GameNetEventListener(netClientView);
		netClientView.setNetEventListener(listener);
		listener.setReady();
	}

	/**
	 * Run when the game model has been set.
	 */
	private void newGameModelSet() {
		if (!uiConstructed) {
			uiConstructed = true;
			
			activeFigure = gameModel.getActiveFigure();
			
			// Create main view.
			view = new GameplayView(gameModel.getBoard());
			view.updateActiveFigureProperties(activeFigure);
			
			// Create side panel.
			sidePanel = new SidePanel();
			sidePanel.addEndTurnButtonListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent arg0) {
					netClientView.getReplyBus().send(
							EndTurnRequest.getInstance());

				}
			});
			
			sidePanel.addEndGameButtonActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					MenuController.open();
				}

			});
			
			// Insert side panel into main view and draw it.
			sidePanel.setPreferredSize(new Dimension(200, view.getHeight()));
			view.add(sidePanel, BorderLayout.EAST);
			sidePanel.updateActiveFigure(gameModel.getActiveFigure());

			// Display main view (with side panel).
			renderTarget.renderAWTComponent(view);
			view.setReceiver(new Receiver());

			// Update side panel's active figure when the game starts
			sidePanel.updateActiveFigure(activeFigure);
		} else {
			// Always update side panel's active figure when it's a new turn
			if (activeFigure != gameModel.getActiveFigure()) {
				activeFigure = gameModel.getActiveFigure();
				sidePanel.updateActiveFigure(activeFigure);
			}
			
			// Always update the view with new information about the active
			// figure
			view.updateActiveFigureProperties(activeFigure);
			view.repaint();

			if (gameModel.hasWinner()) {
				WinningController.open(gameModel.getWinner(),
						gameModel.getLosers());
			} else {
				repaintSidePanel(lastClicked);
			}
		}
	}

	/**
	 * A class to process the mouse-clicks
	 * 
	 * @author Anton Hallin
	 */
	private class Receiver implements IReceiver {
		/**
		 * {@inheritDoc}
		 */
		@Override
		public void mouseClicked(Position mousePos) {
			lastClicked = mousePos;
			netClientView.getReplyBus().send(
					TileActionPerformed.getInstance(mousePos));
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void mouseMoved(Position mousePos) {
			repaintSidePanel(mousePos);
		}
	}

	/**
	 * Repaint the side panel with appropriate methods.
	 * 
	 * @param mousePos the tile position, which will be shown in the side panel.
	 */
	private void repaintSidePanel(Position mousePos) {
		if (mousePos != null) {
			IReadonlyTile mouseOverTile = gameModel.getBoard().getTile(mousePos);

			if (mouseOverTile.hasFigure()) {
				if (mouseOverTile.getFigure().equals(
						gameModel.getActiveFigure())) {
					sidePanel.updateActiveFigure(mouseOverTile.getFigure());
					sidePanel.updateTempTileOnly(mouseOverTile);
				} else {
					sidePanel.updateTempBoth(mouseOverTile);
				}
			} else {
				sidePanel.updateTempTileOnly(mouseOverTile);
			}
		} else {
			sidePanel.cursorOutOfBounds();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void dispose() {
		view = null;
		gameModel = null;
		netClientView.disconnect();
	}

	/**
	 * Open this controller with specified data.
	 * 
	 * @param netClientView
	 *            the NetClientView, prepared with map and teams.
	 */
	public static void open(NetClientView netClientView) {
		GameController instance = new GameController();
		instance.setNetClientView(netClientView);
		MasterController.open(instance);
	}

	/**
	 * Set the NetClientView used to communicate with the game server.
	 * 
	 * @param netClientView
	 *            the server communication view.
	 */
	private void setNetClientView(NetClientView netClientView) {
		this.netClientView = netClientView;
	}

	/**
	 * The listener class for the ninja bunnies client. Everything here is run
	 * in the "local" thread, which is the AWT/Swing thread, for convenience.
	 * GUI updating etc. can be done directly from here. (Don't block.)
	 */
	private class GameNetEventListener implements NetEventListener {
		/** The client view for sending an recieving changes to/from server. */
		private NetClientView client;

		/** Local cache of client.getReplyBus()... */
		private IWritableChangeBus replyBus;

		public GameNetEventListener(NetClientView netClientView) {
			client = netClientView;
			replyBus = client.getReplyBus();
		}

		/**
		 * Tell the server we are ready to game!
		 */
		public void setReady() {
			replyBus.send(GameModelValid.getInstance());
		}

		@Override
		public void onConnect(IWritableChangeBus replyBus) {
			// Should not happen... Maybe useful for re-connects, later on?
			throw new IllegalStateException("client double connected");
			// this.replyBus = replyBus;
		}

		@Override
		public void onDisconnect(IOException exception) {
			System.out.println("Disconnected from local game server.");
			if (exception != null) {
				System.out.println("Error was:");
				exception.printStackTrace();
			}
		}

		@Override
		public void onChangeSet(IChangeSet changeSet) {
			for (IChange change : changeSet) {
				if (change instanceof LegacyGameModelSync) {
					// We got a new game model. Let's show it...
					gameModel.setGameModel(((LegacyGameModelSync) change)
							.getGameModel());
					newGameModelSet();
				}
			}
		}
	}

	/**
	 * Class allowing us to easily swap game models in old code which only can
	 * take a game model on construction. Delegation for getters, no setters.
	 */
	private static class SwappableGameModel implements IReadonlyGameModel {
		private IReadonlyGameModel gameModel = null;
		private SwappableBoard board = new SwappableBoard();

		public void setGameModel(IReadonlyGameModel gameModel) {
			this.gameModel = gameModel;
			board.setBoard(gameModel.getBoard());
		}

		@Override
		public IReadonlyTeam getActiveTeam() {
			return gameModel.getActiveTeam();
		}

		@Override
		public IReadonlyBoard getBoard() {
			return board;
		}

		@Override
		public boolean hasWinner() {
			return gameModel.hasWinner();
		}

		@Override
		public IReadonlyTeam getWinner() {
			return gameModel.getWinner();
		}

		@Override
		public List<? extends IReadonlyTeam> getLosers() {
			return gameModel.getLosers();
		}

		@Override
		public IReadonlyPlayer getActivePlayer() {
			return gameModel.getActivePlayer();
		}

		@Override
		public IReadonlyFigure getActiveFigure() {
			return gameModel.getActiveFigure();
		}

		@Override
		public List<? extends IReadonlyTeam> getTeams() {
			return gameModel.getTeams();
		}
	}

	/**
	 * Class allowing us to easily swap board in old code which only can take a
	 * board on construction. Delegation for getters, no setters.
	 */
	private static class SwappableBoard implements IReadonlyBoard {
		private IReadonlyBoard board = null;

		@Override
		public int getWidth() {
			return board.getWidth();
		}

		@Override
		public int getHeight() {
			return board.getHeight();
		}

		@Override
		public IReadonlyTile getTile(Position position) {
			return board.getTile(position);
		}

		@Override
		public IReadonlyTile getTile(IReadonlyFigure figure) {
			return board.getTile(figure);
		}

		/**
		 * @param board
		 *            the board to set
		 */
		public void setBoard(IReadonlyBoard board) {
			this.board = board;
		}
	}
}