package se.ninjabunnies.client.controller;

import java.io.IOException;

import se.ninjabunnies.client.view.net.NetClientView;
import se.ninjabunnies.client.view.net.NetClientView.NetEventListener;
import se.ninjabunnies.shared.changebus.IChangeSet;
import se.ninjabunnies.shared.changebus.IWritableChangeBus;

/**
 * Quick hack to allowing users connect to a server that has already been setup.
 * 
 * @author Joel Severin
 */
public class DirectGameController implements IController {
	/** The hostname to connect to. */
	private String hostname;
	
	/** The port to connect on. */
	private int port;
	
	/** The client view. (Handled over to game controller when conencted.) */
	private NetClientView client;
	
	/**
	 * Open this controller with specified data.
	 * 
	 * @param hostname
	 *            the hostname to conenct to.
	 * @param port
	 *            the port to connect on.
	 */
	public static void open(String hostname, int port) {
		DirectGameController instance = new DirectGameController();
		instance.setHostname(hostname);
		instance.setPort(port);
		MasterController.open(instance);
	}

	/**
	 * @return the hostname
	 */
	public String getHostname() {
		return hostname;
	}

	/**
	 * @param hostname the hostname to set
	 */
	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	/**
	 * @return the port
	 */
	public int getPort() {
		return port;
	}

	/**
	 * @param port the port to set
	 */
	public void setPort(int port) {
		this.port = port;
	}

	@Override
	public void start(IRenderTarget renderTarget) {
		renderTarget.setRenderContextTitle("Connecting...");
		
		client = new NetClientView();
		client.setNetEventListener(new StartupNetEventListener());
		client.connect(hostname, port);
	}
	
	/**
	 * The listener class for the ninja bunnies client. Everything here is run
	 * in the "local" thread, which is the AWT/Swing thread, for convenience.
	 * GUI updating etc. can be done directly from here. (Don't block.)
	 */
	private class StartupNetEventListener implements NetEventListener {
		@Override
		public void onConnect(IWritableChangeBus replyBus) {
			System.out.println("Connected to " + hostname + ":" + port + ".");
			
			// Ok, let GameController continue!
			client.setNetEventListener(null);
			GameController.open(client);
		}

		@Override
		public void onDisconnect(IOException exception) {
			System.out.println("Disconnected from server.");
			if(exception != null) {
				System.out.println("Error was:");
				exception.printStackTrace();
			}
		}

		@Override
		public void onChangeSet(IChangeSet changeSet) {
		}
	}

	@Override
	public void dispose() {
	}
}