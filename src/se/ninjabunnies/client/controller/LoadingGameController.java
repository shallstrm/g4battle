package se.ninjabunnies.client.controller;

import java.io.IOException;
import java.util.List;

import javax.swing.JOptionPane;

import se.ninjabunnies.client.view.LoadingGameView;
import se.ninjabunnies.client.view.net.NetClientView;
import se.ninjabunnies.client.view.net.NetClientView.NetEventListener;
import se.ninjabunnies.shared.changebus.IChangeSet;
import se.ninjabunnies.shared.changebus.IWritableChangeBus;
import se.ninjabunnies.shared.descriptor.IMapDescriptor;
import se.ninjabunnies.shared.model.IReadonlyTeam;
import se.ninjabunnies.shared.net.state.LegacyMapSetup;
import se.ninjabunnies.shared.net.state.LegacyTeamSetup;

/**
 * Used between start game-click and game rendering, the LoadingGameController
 * synchronizes client and server with a running game.
 * 
 * @author Joel Severin
 */
public class LoadingGameController implements IController {
	/** The map to build the game on. */
	private IMapDescriptor map;

	/** The teams that play. */
	private List<IReadonlyTeam> teams;
	
	/** The client view. (Handled over to game controller when conencted.) */
	private NetClientView client;
	
	/** The hostname to connect to. */
	private String hostname;
	
	/** The port to connect on. */
	private int port;
	
	/**
	 * Open this controller with specified data.
	 * 
	 * @param hostname The game server hostname to connect to.
	 * @param port The game server port to connect on.
	 * @param map the map to play.
	 * @param teams the teams which will play.
	 */
	public static void open(String hostname, int port,
			IMapDescriptor map, List<IReadonlyTeam> teams) {
		LoadingGameController instance = new LoadingGameController();
		instance.setHostname(hostname);
		instance.setPort(port);
		instance.setMap(map);
		instance.setTeams(teams);
		MasterController.open(instance);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void start(IRenderTarget renderTarget) {
		renderTarget.renderAWTComponent(new LoadingGameView());
		renderTarget.setRenderContextTitle("Loading game...");
		
		// Conenct to it here (client part).
		client = new NetClientView();
		client.setNetEventListener(new StartupNetEventListener());
		client.connect(hostname, port);
	}
	
	/**
	 * The listener class for the ninja bunnies client. Everything here is run
	 * in the "local" thread, which is the AWT/Swing thread, for convenience.
	 * GUI updating etc. can be done directly from here. (Don't block.)
	 */
	private class StartupNetEventListener implements NetEventListener {
		@Override
		public void onConnect(IWritableChangeBus replyBus) {
			System.out.println("Connected to local game server.");
			
			// In this version, we just push teams and map to the server.
			System.out.println("Sending team list (legacy impl.).");
			replyBus.send(LegacyTeamSetup.getInstance(teams));
			
			System.out.println("Sending map (legacy impl.).");
			replyBus.send(LegacyMapSetup.getInstance(map));
			
			// Ok, let GameController continue!
			client.setNetEventListener(null);
			GameController.open(client);
		}

		@Override
		public void onDisconnect(IOException exception) {
			System.out.println("Disconnected from local game server.");
			if(exception != null) {
				JOptionPane.showMessageDialog(null,
						exception.getLocalizedMessage(), "An error has ocurred",
						JOptionPane.ERROR_MESSAGE);
				exception.printStackTrace();
				MenuController.open();
			}
		}

		@Override
		public void onChangeSet(IChangeSet changeSet) {
			// We just ignore this for now...
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void dispose() {
	}

	/**
	 * @param hostname the hostname of the remote game server.
	 */
	private void setHostname(String hostname) {
		this.hostname = hostname;
	}

	/**
	 * @param port the port to connect on of the remote game server.
	 */
	private void setPort(int port) {
		this.port = port;
	}
	
	/**
	 * @param map the map to play on the server. Legacy map-synced.
	 */
	private void setMap(IMapDescriptor map) {
		this.map = map;
	}

	/**
	 * @param teams the teams that will play on the server. Legacy team-synced.
	 */
	private void setTeams(List<IReadonlyTeam> teams) {
		this.teams = teams;
	}
}