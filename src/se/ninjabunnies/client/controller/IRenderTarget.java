package se.ninjabunnies.client.controller;

import java.awt.Component;

/**
 * The <code>IRenderTarget</code> is responsible for being rendered on, where
 * rendering can mean graphics, sound etc. Commonly, user clicks and keyboard
 * input is also handled by the graphics component - but that's leaved up to
 * the implementation to decide.
 * 
 * <p>The purpose of this class is to provide easy substitution of render
 * technology while still maintaining static typing etc. Another purpose is to
 * detach render targets from the underlying technology, isolating ownership of
 * the resources. A render target which is not active will not render anything
 * visible.
 * 
 * @author Joel Severin
 */
public interface IRenderTarget {
	/**
	 * Render to a AWT Component showed on-screen to the user. AWT Components
	 * are usually rendered in a context (often an OS window), which properties
	 * can be set by the setRenderContext*-methods.
	 * 
	 * @param Component the component to render.
	 */
	public void renderAWTComponent(Component component);
	
	/**
	 * Sets the title in the context graphics are rendered (usually a window),
	 * when applicable. In some platforms/contexts, this may be obscured or not
	 * shown at all.
	 * 
	 * @param title the context (i.e. window) title.
	 */
	public void setRenderContextTitle(String title);
	
	/**
	 * Checks whether this render target is disposed of. A disposed render
	 * target will not be shown to the user.
	 * 
	 * @return true only if disposed (ignore mode).
	 */
	public boolean isDisposed();
}
