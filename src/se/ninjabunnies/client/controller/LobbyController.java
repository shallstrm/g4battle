package se.ninjabunnies.client.controller;

import java.util.ArrayList;
import java.util.List;

import se.ninjabunnies.client.view.LobbyView;
import se.ninjabunnies.client.view.MapPanel;
import se.ninjabunnies.client.view.PlayerSlot;
import se.ninjabunnies.client.view.PlayersPanel;
import se.ninjabunnies.server.controller.GameServerController;
import se.ninjabunnies.server.model.IPlayer;
import se.ninjabunnies.server.model.ITeam;
import se.ninjabunnies.server.model.Player;
import se.ninjabunnies.server.model.Team;
import se.ninjabunnies.shared.descriptor.IMapDescriptor;
import se.ninjabunnies.shared.descriptor.MapDescriptor;
import se.ninjabunnies.shared.model.IReadonlyTeam;
import se.ninjabunnies.shared.net.NinjaBunniesNetwork;
import se.ninjabunnies.shared.util.ColorGenerator;

/**
 * The controller that shows the game lobby, where users can choose which teams
 * and maps to play.
 * 
 * @author Joel Severin
 */
public class LobbyController implements IController {
	/** The view with the lobby (map and team choosing). */
	private LobbyView view;
	
	/** Controls whether the game should be a hotseat game or not. */
	private boolean isHotseat;
	
	/**
	 * {{@inheritDoc}
	 */
	@Override
	public void start(IRenderTarget renderTarget) {
		view = new LobbyView(isHotseat);
		view.setViewListener(new LobbyViewListener());
		renderTarget.renderAWTComponent(view);
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public void dispose() {
		view = null;
	}
	
	/**
	 * Open this controller.
	 * 
	 * @param isHotseat true if starting a hotseat game, false for networked.
	 */
	public static void open(boolean isHotseat) {
		LobbyController instance = new LobbyController();
		instance.setHotseat(isHotseat);
		MasterController.open(instance);
	}

	/**
	 * Start the game by creating a local game server and connecting, by leaving
	 * control to an appropriate controller that can continue.
	 */
	private void startGame() {
		//Gat a couple of variables we will need
		PlayersPanel playersPanel = view.getPlayersPanel();
		ArrayList<PlayerSlot> playersSlots = playersPanel.getPlayers();
		MapPanel mapPanel = view.getMapPanel();
		IMapDescriptor map = new MapDescriptor(
				mapPanel.getSelectedMap().getMapDescriptorCompatibleFormat());

		//This helps us split the players into teams
		ArrayList<Integer> teamlist = new ArrayList<Integer>();
		for(PlayerSlot playerSlot: playersSlots) {
			if(!teamlist.contains(playerSlot.getTeamIndex())){
				teamlist.add(playerSlot.getTeamIndex());
			}
		}
		
		//create players and teams
		List<ITeam> teams = new ArrayList<ITeam>();
		int teamIDs = 1; //gives us an easy way to keep track of the teamIDs
		for(PlayerSlot playerSlot: playersSlots) {
			IPlayer player = new Player(playerSlot.getPlayerName(),
					ColorGenerator.generateColor(playerSlot.getColorIndex()));
			player.setStartingPosition(playerSlot.getStartPosition());
			if(teams.size() <= teamlist.indexOf(playerSlot.getTeamIndex())) {
				ArrayList<IPlayer> players = new ArrayList<IPlayer>();
				players.add(player);
				teams.add(new Team("Team " + (teamIDs), players));
				teamIDs++;
			} else {
				teams.get(teamlist.indexOf(playerSlot.getTeamIndex())).addPlayer(player);
			}
		}
		
		// Spawn a new server running on localhost.
		GameServerController gameServer = new GameServerController();
		gameServer.setPort(NinjaBunniesNetwork.DEFAULT_PORT);
		gameServer.setHotseat(isHotseat);
		gameServer.start();

		// Connect to the server.
		LoadingGameController.open("localhost", NinjaBunniesNetwork.DEFAULT_PORT,
				map, new ArrayList<IReadonlyTeam>(teams));
	}


	/**
	 * @param isHotseat the new hotseat status. false means networked.
	 */
	private void setHotseat(boolean isHotseat) {
		this.isHotseat = isHotseat;
	}

	/**
	 * Listener receiveing all events from the lobby view.
	 */
	private class LobbyViewListener implements LobbyView.ILobbyViewListener {
		@Override
		public void onStartGameClicked() {
			startGame();
		}

		@Override
		public void onBackToMenuClicked() {
			MenuController.open();
		}		
	}
}