package se.ninjabunnies.client.controller;

import javax.swing.JOptionPane;

import se.ninjabunnies.client.view.MenuView;
import se.ninjabunnies.shared.net.NinjaBunniesNetwork;
import se.ninjabunnies.shared.util.ResourceManager;

/**
 * The controller that shows the game menu plus surrounding graphics.
 * 
 * @author Joel Severin
 */
public class MenuController implements IController {
	/** The menu with buttons <em>and</em> surrounding graphics. */
	private MenuView view;

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public void start(IRenderTarget renderTarget) {
		view = new MenuView();
		view.setViewListener(new MenuViewListener());
		renderTarget.renderAWTComponent(view);
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public void dispose() {
		view = null;
	}

	/**
	 * Open this controller.
	 */
	public static void open() {
		MasterController.open(new MenuController());
	}

	private class MenuViewListener implements MenuView.IMenuViewListener {
		@Override
		public void onHotseatClicked() {
			LobbyController.open(true);
		}

		@Override
		public void onNewGameClicked() {
			LobbyController.open(false);
		}

		@Override
		public void onJoinGameClicked() {
			String server = JOptionPane.showInputDialog("Server address?\n"
					+ "Format: host/IP:port or just host/IP (default port "
					+ NinjaBunniesNetwork.DEFAULT_PORT + " will be used).",
					"localhost:" + NinjaBunniesNetwork.DEFAULT_PORT);
			// Null means abort or closed window.
			if (server != null) {
				String[] parts = server.split(":", 2);

				String hostname = parts[0].trim();
				int port = NinjaBunniesNetwork.DEFAULT_PORT;
				if (parts.length > 1) {
					try {
						port = Integer.parseInt(parts[1].trim(), 10);
					} catch (NumberFormatException e) {
						// Just ignore it, keeping the default port...
					}
				}

				if (port > 0 && hostname.length() > 0) {
					LoadingGameController.open(hostname, port, null, null);
				} else {
					JOptionPane.showMessageDialog(view,
							"Invalid host/IP or port.");
				}
			}
		}

		@Override
		public void onTutorialClicked() {
			TutorialController.open();
		}

		@Override
		public void onExitGameClicked() {
			System.exit(0);
		}

		@Override
		public void changeThemeClicked() {

			if (ResourceManager.getInstance().getTheme() == null) {

				ResourceManager.getInstance().setTheme("retro");
			} else {

				ResourceManager.getInstance().setTheme(null);
			}

			MenuController.open();

		}
	}
}