package se.ninjabunnies.client.controller;

/**
 * The controller interface every controller has to implement in order to work
 * with the unified controller handling.
 * 
 * @author Joel Severin
 */
public interface IController {
	/**
	 * Starts the controller for the first time. Should be used to do heavy
	 * work instead of in the constructor. Could be called with a delay after
	 * construction, or immediately.
	 */
	public void start(IRenderTarget renderTarget);
	
	/**
	 * Shuts down the controller. Afterwards, it will no longer will be active.
	 */
	public void dispose();
}
