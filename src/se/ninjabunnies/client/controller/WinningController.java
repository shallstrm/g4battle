package se.ninjabunnies.client.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import se.ninjabunnies.client.view.WinningView;
import se.ninjabunnies.shared.model.IReadonlyTeam;


/**
 * The controller that shows the winners (and loosers) from a game.
 * 
 * @author Joel Severin
 */
public class WinningController implements IController {
	private IReadonlyTeam winningTeam;
	private List<? extends IReadonlyTeam> losingTeams;
	
	/**
	 * {{@inheritDoc}
	 */
	@Override
	public void start(IRenderTarget renderTarget) {
		renderTarget.setRenderContextTitle(winningTeam.getName() + " won!");
		WinningView win = new WinningView();
		renderTarget.renderAWTComponent(win);
		
		win.setWinnerTeam(winningTeam.getName());
		for(IReadonlyTeam looser : losingTeams) {
			win.addLooserTeam(looser.getName());
		}
		
		win.addBackToMenuListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MenuController.open();
			}
		});
	}
	
	/**
	 * {{@inheritDoc}
	 */
	@Override
	public void dispose() {
	}

	/** Sets the winning team. */
	private void setWinningTeam(IReadonlyTeam winningTeam) {
		this.winningTeam = winningTeam;
	}

	/** Sets the losing teams. */
	private void setLosingTeams(List<? extends IReadonlyTeam> losingTeams) {
		this.losingTeams = losingTeams;
	}
	
	/**
	 * Open this controller.
	 * 
	 * @param winner the winning team.
	 * @param list list of losers.
	 */
	public static void open(IReadonlyTeam winner,
			List<? extends IReadonlyTeam> list) {
		WinningController controller = new WinningController();
		controller.setWinningTeam(winner);
		controller.setLosingTeams(list);
		MasterController.open(controller);
	}
}
