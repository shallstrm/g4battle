package se.ninjabunnies.client.controller;

import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import se.ninjabunnies.shared.util.ResourceManager;

/**
 * Controller changing functionality is provided by this class.
 * 
 * @author Joel Severin
 */
public final class MasterController {
	public static final String GAME_TITLE = "Ninja Bunnies";

	/** Singleton instance. */
	private static MasterController instance = null;
	
	/** The active controller instance. */
	private IController activeController = null;
	
	/** The active render target (swapped with controller). */
	private MasterControllerRenderTarget activeRenderTarget = null;
	
	/** The window which the game is drawn in. */
	private JFrame frame = null;
	
	/**
	 * This class is an internal singleton, but with a static interface (we
	 * simply don't expose that we are a singleton). Make calls to getInstance()
	 * from your static wrappers.
	 * 
	 * @return singleton instance of this class.
	 */
	private static MasterController getInstance() {
		if(instance == null) {
			instance = new MasterController();
		}
		return instance;
	}
	
	/** Private constructor. (Singleton) */
	private MasterController() {
		// First of all, try setting the system look and feel of UI widgets.
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	    } 
		// (Silently fail on purpose, we don't care if things are not pretty.)
	    catch (UnsupportedLookAndFeelException e) {
	    }
	    catch (ClassNotFoundException e) {
	    }
	    catch (InstantiationException e) {
	    }
	    catch (IllegalAccessException e) {
	    }
		
		frame = new JFrame(GAME_TITLE);
		frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		Toolkit toolkit =  Toolkit.getDefaultToolkit ();
		frame.setMinimumSize(new Dimension(toolkit.getScreenSize().width / 2,
				toolkit.getScreenSize().height/2));
		frame.setIconImage(ResourceManager.getInstance()
				.getImageResource("FrameImageIcon.png"));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(true);
		frame.pack();
		frame.setVisible(true);
		frame.setExtendedState(
				frame.getExtendedState() | JFrame.MAXIMIZED_BOTH);
		frame.getContentPane().removeAll();
	}
	
	/**
	 * Open the controller specified and set it as active, de-activating the
	 * current (if any).
	 * 
	 * @param controller the controller to activate.
	 */
	public static void open(IController controller) {
		getInstance().openController(controller);
	}
	
	/**
	 * Internal controller opener.
	 * 
	 * @see MasterController#open(IController)
	 * @param controller the controller to open.
	 */
	private void openController(IController controller) {
		// Get rid of old controller and render target.
		if(activeController != null) {
			activeController.dispose();
		}
		if(activeRenderTarget != null) {
			activeRenderTarget.dispose();
		}
		
		// Swap controller, swapping render target too.
		activeRenderTarget = new MasterControllerRenderTarget(frame);
		activeController = controller;
		activeController.start(activeRenderTarget);
	}
}
