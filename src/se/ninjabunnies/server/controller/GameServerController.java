package se.ninjabunnies.server.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import se.ninjabunnies.server.model.GameModel;
import se.ninjabunnies.server.model.IGameModel;
import se.ninjabunnies.server.model.IPlayer;
import se.ninjabunnies.server.model.ITeam;
import se.ninjabunnies.server.model.Team;
import se.ninjabunnies.server.net.SimpleServerChangeBus;
import se.ninjabunnies.server.view.NetServerView;
import se.ninjabunnies.server.view.NetServerView.ISessionActions;
import se.ninjabunnies.shared.changebus.ChangeSetChangeBus;
import se.ninjabunnies.shared.changebus.DeMultiplexingChangeBus;
import se.ninjabunnies.shared.changebus.IChange;
import se.ninjabunnies.shared.changebus.IChangeSet;
import se.ninjabunnies.shared.changebus.IWritableChangeBus;
import se.ninjabunnies.shared.changebus.LoggingChangeBus;
import se.ninjabunnies.shared.descriptor.IMapDescriptor;
import se.ninjabunnies.shared.model.IReadonlyTeam;
import se.ninjabunnies.shared.net.NinjaBunniesNetwork;
import se.ninjabunnies.shared.net.state.EndTurnRequest;
import se.ninjabunnies.shared.net.state.GameModelValid;
import se.ninjabunnies.shared.net.state.LegacyMapSetup;
import se.ninjabunnies.shared.net.state.LegacyTeamSetup;
import se.ninjabunnies.shared.net.state.TileActionPerformed;
import se.ninjabunnies.shared.util.Position;

public class GameServerController {
	/** The server view with input from the network. */
	private NetServerView<Session> netServerView;
	
	/** The port to connect to the server view on. Currently binds on 0.0.0.0 */
	private int port = NinjaBunniesNetwork.DEFAULT_PORT;
	
	/** Holds the games running, with the key being the game ID. */
	private Map<Integer, Game> games = new HashMap<Integer, Game>();
	
	/** True only if the server is acting as a hotseat game. */
	private boolean isHotseat = false;
	
	public void start() {
		try {
			netServerView = new NetServerView<Session>(
					new GameServerEventListener());
			netServerView.bind(port);
			netServerView.start();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	private class GameServerEventListener
			implements NetServerView.ServerEventListener<Session> {
		/**
		 * Called on connection to server.
		 * 
		 * @return the session object created.
		 */
		@Override
		public Session onConnect(ISessionActions sessionActions) {
			Session session = new Session(sessionActions);
			
			// With hotseating, we only have one game. If another client tries
			// to conenct, they are refused.
			if(isHotseat) {
				Game game = games.get(0);
				if(game == null) {
					game = new Game();
					game.setAdmin(session);
					games.put(0, game);
					session.setGame(game);
				} else {
					// Cannot sessionActions.disconnect(); since KryoNet throws
					// a null pointer exception then. We have to wait...
				}
			} else {
				// @todo fix this so we can have multiple games etc...
				Game game = games.get(0);
				if(game == null) {
					game = new Game();
					game.setAdmin(session);
					games.put(0, game);
					session.setGame(game);
					// Player is set when the game is created.
				} else {
					// Try obtaining a new player slot.
					IPlayer player = game.getAvailablePlayer();
					if(player != null) {
						session.setPlayer(player);
						session.setGame(game);
					}
				}
			}
			
			return session;
		}
		
		@Override
		public void onConnected(Session session) {
			if(session.getGame() == null) {
				session.disconnect();
			}
		}

		@Override
		public void onChange(IChange change, Session session) {
			Game game = session.getGame();
			if(game == null) {
				// Stupid clients having nothing to do here...
				return;
			}
			
			// Might be null.
			IGameModel gameModel = game.getGameModel();
			
			// Used below to check if running a game and the game is either
			// hotseated or the player is the current player in non-hoteset mode
			boolean isSessionsTurn = (gameModel != null && (isHotseat
					|| gameModel.getActivePlayer() == session.getPlayer()));
			
			if(change instanceof LegacyTeamSetup) {
				if(session.isAdmin()) {
					game.teams = ((LegacyTeamSetup) change).getTeams();
				}
			} else if(change instanceof LegacyMapSetup) {
				if(session.isAdmin()) {
					game.map = ((LegacyMapSetup) change).getMap();
				}
			} else if(change instanceof GameModelValid) {
				if(session.isAdmin() && game.map != null && game.teams != null
						&& gameModel == null) {
					game.createGameModel();
					if(!isHotseat) {
						// Set the player this session controls.
						// (Only for non-hotseat games, hotseat controls all)
						// Other players get this set on connect instead.
						session.setPlayer(game.getAvailablePlayer());
					}
				}
			} else if(change instanceof TileActionPerformed) {
				// Validate that the player is playing right now.
				if(isSessionsTurn) {
					Position position = 
							((TileActionPerformed) change).getPosition();
					game.tileActionPerformed(session, position);
				}
			} else if(change instanceof EndTurnRequest) {
				// Validate that the player is playing right now, or is admin.
				if(isSessionsTurn || game.isAdmin(session)) {
					game.endTurn(session);
				}
			}

			game.commitChangeSets();
		}

		@Override
		public void onDisconnect(Session session) {
			// Hotseated games means that when the local client (only client)
			// disconencts, the server shuts down.
			if(isHotseat) {
				// Make sure this is a real game session and not one refused.
				Game game = session.getGame();
				if(game != null && game.isAdmin(session)) {
					netServerView.stop();
				}
			} else {
				// @todo We should not do this when running a daemon server.
				// Kill the server when the admin quits.
				Game game = session.getGame();
				if(game != null && game.isAdmin(session)) {
					netServerView.stop();
				}
			}
		}
	}
	
	/**
	 * The session represents a player/connection (persistent). In the future,
	 * sessions could be detached from connections (allowing re-connects etc.)
	 */
	private static class Session {
		private Game game = null;
		
		/** The player the session is controlling in the Game(Model). */
		private IPlayer player = null;
		
		private ISessionActions sessionActions;
		
		public Session(ISessionActions sessionActions) {
			this.sessionActions = sessionActions;
		}

		/**
		 * Set the player the session is controlling in the Game.
		 * 
		 * @param player the player controlled.
		 */
		public void setPlayer(IPlayer player) {
			this.player = player;
		}
		
		/**
		 * Get the player the session is controlling in the Game.
		 * 
		 * @return the player controlled, or null if none.
		 */
		public IPlayer getPlayer() {
			return player;
		}

		public void disconnect() {
			sessionActions.disconnect();
		}

		public boolean isAdmin() {
			return game != null && game.isAdmin(this);
		}

		public Game getGame() {
			return game;
		}

		public void setGame(Game game) {
			this.game = game;
			game.addSession(this);
		}
		
		public void sendChangeSet(IChangeSet changeSet) {
			sessionActions.sendChangeSet(changeSet);
		}
	}
	
	/**
	 * A game is representing a GameModel, with some extra state. Usually, many
	 * sessions participate in a game.
	 * 
	 * <p>This class should not be created outside the GameServerController
	 * implementation. It is only public to allow some KryoNet hacks.
	 */
	public static class Game {
		/** The sessions participating in this game. */
		private List<Session> sessions = new ArrayList<Session>();
		
		/** The admin of the game. Can load map, teams, create game etc. */
		private Session admin = null;
		
		/** The server bus is where GameModel (component) updates come from. */
		private ServerChangeBus serverBus;
		
		/**
		 * This is where change sets are optimized (of there are any
		 * optimizations...) and packaged for sending over the network.
		 */
		ChangeSetChangeBus changeSetChangeBus = new ChangeSetChangeBus();
		
		/** The game model isntance. */
		private IGameModel gameModel = null;
		
		// Legacy team and map transfer.
		private List<IReadonlyTeam> teams = null;
		private IMapDescriptor map = null;
		
		// Legacy player setup.
		private int lastTeamAvailable = 0;
		private int lastPlayerAvailable = 0;
		
		/**
		 * Checks whether a session is a trusted admin of the game or not.
		 * 
		 * @param session the session to check.
		 * @return true if session is game admin, false otherwise.
		 */
		public boolean isAdmin(Session session) {
			return admin != null && admin.equals(session);
		}

		/**
		 * An ending of the current turn was requested.
		 * 
		 * @param session the session of which the request originated.
		 */
		public void endTurn(Session session) {
			// @todo add security
			if(gameModel != null) { // Never trust the client!
				gameModel.endTurn();
				serverBus.setChangeSetBarrier();
			}
		}

		/**
		 * A tile action was performed on the current game model.
		 * 
		 * @param session the session that performed the tile action.
		 * @param position the position of the tile that received the action.
		 */
		public void tileActionPerformed(Session session, Position position) {
			// @todo add security
			if(gameModel != null) { // Never trust the client!
				gameModel.tileActionPerformed(position);
				serverBus.setChangeSetBarrier();
			}
		}

		/**
		 * Makes the session admin of this game. Only one session can be admin.
		 * 
		 * @param admin the new admin session.
		 */
		public void setAdmin(Session admin) {
			this.admin = admin;
		}

		/**
		 * Create the game model. Make sure there is a map to play and teams
		 * with players in, fitting the map, before calling this method.
		 */
		public void createGameModel() {
			// Create a bus with many receivers.
			DeMultiplexingChangeBus busses = new DeMultiplexingChangeBus();
			
			// Logging on stdout is a receiver... (hashcode (ab)use helps debug)
			busses.addReceiver(new LoggingChangeBus(
					"Game " + this.hashCode(), System.out));
			
			// The network is a receiver...
			busses.addReceiver(changeSetChangeBus);
			
			serverBus = new ServerChangeBus(busses);
			
			// Create teams. This is the legacy way, we should create teams from
			// different clients somewhere in this controller in the future.
			List<ITeam> mutableTeams = new ArrayList<ITeam>(teams.size());
			for(IReadonlyTeam readonlyTeam : teams) {
				mutableTeams.add(new Team(readonlyTeam));
			}
			this.gameModel = new GameModel(map, mutableTeams, serverBus);
			
			serverBus.setChangeSetBarrier();
		}

		/**
		 * Returns the game model of this game.
		 * @return the GameModel.
		 */
		public IGameModel getGameModel() {
			return this.gameModel;
		}
		
		/**
		 * Couple this game with the session supplied.
		 * 
		 * @param session the session to take part in this game.
		 */
		public void addSession(Session session) {
			sessions.add(session);
		}
		
		/**
		 * Commits all change sets to the network, for all clients...
		 */
		public void commitChangeSets() {
			IChangeSet changes = changeSetChangeBus.receiveAll();
			if(changes != null) {
				for(Session session : sessions) {
					session.sendChangeSet(changes);
				}
			}
		}
		
		/**
		 * Get an available player, if there is one, according to an internal
		 * player obtaining rotation. Requires a started GameModel with teams.
		 * 
		 * @return the available player, or null if none found.
		 */
		public IPlayer getAvailablePlayer() {
			// Must do this on GameModel, since teams might change by client...
			if(gameModel != null) {
				List<? extends ITeam> teams = gameModel.getTeams();
				
				// Check in all teams from here to end.
				while(lastTeamAvailable < teams.size()) {
					List<? extends IPlayer> players =
							teams.get(lastTeamAvailable).getPlayers();
					
					// If player found, goto next player for next method
					// invocation, and return the player found.
					if(lastPlayerAvailable < players.size()) {
						lastPlayerAvailable++;
						return players.get(lastPlayerAvailable-1);
					// If no player found, try first player in next team...
					} else {
						lastTeamAvailable++;
						lastPlayerAvailable = 0;
					}
				}
			}
			return null;
		}
		
		/**
		 * The server change bus notifies all participants of a Game of changes
		 * that happens after commit (multiple barries reads).
		 * 
		 * <p>This class should not be created outside the GameServerController
		 * implementation. It is only public to allow some KryoNet hacks.
		 */
		public static class ServerChangeBus extends SimpleServerChangeBus {
			/** This is where changes are written. */
			IWritableChangeBus out;
			
			public ServerChangeBus(IWritableChangeBus out) {
				this.out = out;
			}
			
			/** Needed for Kryo serialization. */
			@SuppressWarnings("unused")
			private ServerChangeBus() {
			}
			
			/**
			 * {@inheritDoc}
			 */
			@Override
			public void send(IChange change) {
				out.send(change);
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void setChangeSetBarrier() {
				out.setChangeSetBarrier();
			}
		}
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	/**
	 * @return true if this game is a hotseated game, false if multi client.
	 */
	public boolean isHotseat() {
		return isHotseat;
	}

	/**
	 * Sets whether the game is hotseated (played on one client), or if multiple
	 * clients conenct to it.
	 * 
	 * @param isHotseat true if game is hotseated.
	 */
	public void setHotseat(boolean isHotseat) {
		this.isHotseat = isHotseat;
	}
}