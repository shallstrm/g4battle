package se.ninjabunnies.server.view;

import java.io.IOException;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import se.ninjabunnies.shared.changebus.IChange;
import se.ninjabunnies.shared.changebus.IChangeSet;
import se.ninjabunnies.shared.net.NinjaBunniesNetwork;

/**
 * The view used for server networking. A shim to the underlying networking
 * implementation. The <code>S</code> generic parameter is the type of which
 * sessions will be of.
 * 
 * @author Joel Severin
 */
public class NetServerView<S> {
	/** The KryoNet server handle. */
	private Server server;
	
	/** Event delivery to consumer (controller) interface instance. */
	private final ServerEventListener<S> serverEventListener;
	
	/**
	 * Constructs a new server view, interfacing with client over the network.
	 * 
	 * @param listener the server listener that will get networking event info.
	 * @see NetServerView#bind(int)
	 * @see NetServerView#start()
	 */
	public NetServerView(final ServerEventListener<S> listener) {
		this.serverEventListener = listener;
		
		server = new Server() {
			@Override
			protected Connection newConnection() {
				// We lose the type here, so we have to use "listener"...
				SessionLink link = new SessionLink();
				link.setSession(listener.onConnect(link));
				return link;
			}
		};
		NinjaBunniesNetwork.setupNetworkFor(server);// Kryo registration.
		server.addListener(new ServerListener());
	}
	
	/** 
	 * The session link is a KryoNet connection sub-class, which reverse-wraps a
	 * Ninja Bunnies session interface. It also makes it type-safe with generics
	 * (the unsafe conversion between Connection and SessionLink happens though,
	 * but just once and not everywhere...).
	 */
	private class SessionLink extends Connection implements ISessionActions {
		/** The session held, type safe. */
		private S session;
			
		/**
		 * Set the public session this local link wraps.
		 * @param session the session.
		 */
		public void setSession(S session) {
			this.session = session;
		}
		
		/** Get the public session this local linkage shim wraps. */
		public S getSession() {
			return session;
		}
		
		/**
		 * {@inheritDoc}
		 */
		@Override
		public void sendChangeSet(IChangeSet changeSet) {
			// For debugging serialization leakage problems...
			/*
			Log.set(Log.LEVEL_TRACE);
			Kryo kryo = new Kryo();
			kryo.setRegistrationRequired(true);
			NinjaBunniesNetwork.registerKryoClasses(kryo);
	        Output output = new Output(new byte[10000000]);
	        kryo.writeClassAndObject(output, changeSet);
	        System.out.println("Size = " + output.position());
	        */
			
			this.sendTCP(changeSet);
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void disconnect() {
			this.close();
		}
	}
	
	/**
	 * Make the server start listening for client connections on this port.
	 * 
	 * @param port the port to listen on.
	 * @throws IOException if connections fails.
	 */
	public void bind(int port) throws IOException {
		server.bind(port);
	}
	
	/**
	 * Start the server! (Make sure you call {@link NetServerView#bind(int)}
	 * first, or nothing will happen.)
	 */
	public void start() {
		server.start();
	}
	
	/**
	 * Stop the server, disconnecting all clients.
	 */
	public void stop() {
		server.stop();
	}
	
	/**
	 * A receiver of updates from the server, by any client (session based).
	 * 
	 * @param <S> S is the session type.
	 */
	public interface ServerEventListener<S> {
		/**
		 * Produces a session of the type specified.
		 * 
		 * @param sessionActions a session action object with networking tools.
		 * @return the session this method should create.
		 */
		public S onConnect(ISessionActions sessionActions);
		
		/**
		 * Called when the session has been setup by the networking framework.
		 * 
		 * @return session the session that was connected.
		 */
		public void onConnected(S session);
		
		/**
		 * Received a change from the client. Clients send changes, while we
		 * send change sets.
		 * 
		 * <!-- @todo We should make these commands or something... -->
		 * 
		 * @param change the change received.
		 * @param session the session that issued the change. Authenticate!
		 */
		public void onChange(IChange change, S session);
		
		/**
		 * The client belongning to the session disconnected.
		 */
		public void onDisconnect(S session);
	}
	
	/**
	 * The KryoNet event listener.
	 */
	private class ServerListener extends Listener {
		/**
		 * {@inheritDoc}
		 */
		@Override
		public void connected(Connection connection) {
			@SuppressWarnings("unchecked") // KryoNet...
			S session = ((SessionLink) connection).getSession();
			
			serverEventListener.onConnected(session);
		}
		
		/**
		 * {@inheritDoc}
		 */
		@Override
		public void received(Connection connection, Object object) {
			if(object instanceof IChange) {
				IChange change = (IChange) object;
				@SuppressWarnings("unchecked") // KryoNet...
				S session = ((SessionLink) connection).getSession();
				
				serverEventListener.onChange(change, session);
			}
		}
		
		/**
		 * {@inheritDoc}
		 */
		@Override
		public void disconnected(Connection connection) {
			System.out.println("[Server: Client disconnected]");
			@SuppressWarnings("unchecked") // KryoNet...
			S session = ((SessionLink) connection).getSession();
			serverEventListener.onDisconnect(session);
		}
	}
	
	/**
	 * Session networking toolbox that session implementations can use.
	 */
	public interface ISessionActions {
		/**
		 * Send a change set to the client the session action targets.
		 * @param changeSet the change set to send.
		 */
		public void sendChangeSet(IChangeSet changeSet);

		/** Disconenct the session. */
		public void disconnect();
	}
}