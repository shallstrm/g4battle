package se.ninjabunnies.server.net;

/**
 * A simple server change bus that just increments on the next*()-methods.
 * 
 * @see NullServerChangeBus
 * @author Joel Severin
 */
public abstract class SimpleServerChangeBus implements IServerChangeBus {
	private int lastFigureId = -1;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int nextFigureId() {
		lastFigureId++;
		return lastFigureId;
	}
}
