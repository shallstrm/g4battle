package se.ninjabunnies.server.net;

import se.ninjabunnies.shared.changebus.IChange;

/**
 * A bus that does nothing but returning null, and counting up for all next*().
 * All bus activity is discarded.
 * 
 * @author Joel Severin
 */
public class NullServerChangeBus extends SimpleServerChangeBus {
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void send(IChange change) {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setChangeSetBarrier() {
	}
}