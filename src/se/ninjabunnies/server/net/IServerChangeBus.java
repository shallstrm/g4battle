package se.ninjabunnies.server.net;

import se.ninjabunnies.shared.changebus.IWritableChangeBus;

/**
 * The bus to use when sending updates in the server models.
 * 
 * @author Joel Severin
 */
public interface IServerChangeBus extends IWritableChangeBus {
	/**
	 * Get a new unique figure id for this change bus context.
	 * 
	 * @return unique figure id for this bus.
	 */
	public int nextFigureId();
}