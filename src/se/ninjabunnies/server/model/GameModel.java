package se.ninjabunnies.server.model;

import java.util.ArrayList;
import java.util.List;

import se.ninjabunnies.server.net.IServerChangeBus;
import se.ninjabunnies.shared.descriptor.IMapDescriptor;
import se.ninjabunnies.shared.model.IReadonlyGameModel;
import se.ninjabunnies.shared.net.state.LegacyGameModelSync;
import se.ninjabunnies.shared.util.Position;

/**
 * @author Simone Damaschke (re-hacked by Joel Severin)
 * @author labbGrupp4
 * @version 0.1 The class that can be defined as the game itself to contain the
 *          pieces of the game and help them work together.
 */

/**
 * The <code>GameModel</code> keeps track of a whole game set, from loading a
 * map to winners and losers are reported. Map change means a new Game Model.
 */
public class GameModel implements IGameModel {
	/** The Board holds all the Tiles and Figures that is played with. */
	private IBoard board;

	/** The team list holds all teams that is fed to the Game Model. */
	private List<ITeam> teams;

	/** Which team's turn it is right now. */
	private int currentTeam = 0;

	/** The winner, if the game has been won. */
	private ITeam winner = null;

	/** The change bus used for this game instance. */
	private IServerChangeBus changeBus;

	/**
	 * Creates a new game.
	 * 
	 * @param map
	 *            the map to play.
	 * @param teams
	 *            the pre-made teams.
	 * @param changeBus
	 *            the change bus used for this game instance.
	 * @throws IllegalArgumentException
	 *             if the player count in the teams don't match the map.
	 */
	public GameModel(IMapDescriptor map, List<ITeam> teams,
			IServerChangeBus changeBus) {
		this.changeBus = changeBus;
		
		// Validate that player count is atleast MIN_PLAYER_COUNT.
		int playerCount = 0;
		for (ITeam team : teams) {
			playerCount += team.getPlayerCount();
			if(playerCount >= IReadonlyGameModel.MIN_PLAYER_COUNT) {
				break;
			}
		}
		if (playerCount < IReadonlyGameModel.MIN_PLAYER_COUNT) {
			throw new IllegalArgumentException(
					"player count in all teams is less than MIN_PLAYER_COUNT");
		}

		this.teams = teams;

		// Create the board from the MapDescriptor, mapping out Players
		// to player classes for figures in the MapDescriptor.
		board = new Board(map, getPlayers(), changeBus);

		// Start the turn for the first player.
		getActivePlayer().startTurn();
		
		// Whatever... Let's just send the whole thing...
		// This is so bad, that it should be refactored away ASAP! A S A P ! ! !
		changeBus.send(LegacyGameModelSync.getInstance(this));
	}
	
	/** Needed for Kryo serialization. */
	@SuppressWarnings("unused")
	private GameModel() {
	}

	/**
	 * A method to get all players for all teams in one list.
	 * 
	 * @return List<Player> list of all players in the game.
	 */
	private List<IPlayer> getPlayers() {
		List<IPlayer> players = new ArrayList<IPlayer>();
		for (ITeam team : teams) {
			players.addAll(team.getPlayers());
		}
		return players;
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public void nextActiveTeam() {
		int oldTeam = currentTeam;
		while (true) {
			// Rotate turn.
			currentTeam++;
			if (currentTeam >= teams.size()) {
				currentTeam = 0;
			}

			// If we get back to the old turn, give up.
			// If we find an active team, make it current.
			if (oldTeam == currentTeam || teams.get(currentTeam).isActive()) {
				return;
			}
		}
	}

	/**
	 * Returns the number of active teams.
	 * 
	 * @return the number of active teams.
	 */
	private int getActiveTeamCount() {
		int teamCount = 0;
		for (ITeam team : teams) {
			if (team.isActive()) {
				teamCount++;
			}
		}
		return teamCount;
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public ITeam getActiveTeam() {
		return teams.get(currentTeam);
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public IBoard getBoard() {
		return board;
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public IPlayer getActivePlayer() {
		return getActiveTeam().getCurrentPlayer();
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public void tileActionPerformed(Position position) {

		ITile tile = board.getTile(position);
		IFigure figure = tile.getFigure();
		IPlayer currentPlayer = getActivePlayer();

		if (tile.hasFigure()) {
			// Player choose one of it's own figures, change to it.
			if (currentPlayer.equals(figure.getPlayer()) && !figure.isUsed()) {
				currentPlayer.changeActiveFigure(figure);
				// Player choose a figure of another team, attack it.
			} else if (!figure.inTeam(getActiveTeam())) {
				IFigure activeFigure = currentPlayer.getActiveFigure();
				ITile figureTile = board.getTile(activeFigure);
				if (activeFigure.canAttackBetween(figureTile, tile)) {
					activeFigure.attackIt(figure);

					if (currentPlayer.isAllFiguresUsed()) {
						nextTurn();
					} else {
						currentPlayer.changeNextActiveFigure();
					}

				}

				if (!figure.isAlive()) {
					// Remove the figure from the tile if it died.
					tile.setFigure(null);

					// Dead figures might mean that we have a winner!
					if (getActiveTeamCount() < 2) {
						winner = getActiveTeam();
					}
				}

			}
			// No figure on selected tile, move there if possible.
		} else {
			IFigure activeFigure = currentPlayer.getActiveFigure();
			ITile oldTile = board.getTile(activeFigure);
			
			
			int moveLength = activeFigure.getMovePathLength(oldTile, tile);
			if (moveLength > 0
					&& moveLength <= activeFigure.distanceAbleToMove()) {
				oldTile.moveFigureTo(tile, moveLength);
			}
			
			if (currentPlayer.isAllFiguresUsed()) {
				nextTurn();
			}
		}
		
		// Whatever... Let's just send the whole thing...
		// This is so bad, that it should be refactored away ASAP! A S A P ! ! !
		changeBus.send(LegacyGameModelSync.getInstance(this));
	}

	/**
	 * Try to move to the next turn. If that fails, we stay where we are.
	 */
	private void nextTurn() {
		// Move the team roll forward.
		nextActiveTeam();

		// Move the player roll forward, for the team set above.
		getActiveTeam().nextActivePlayer();

		getActivePlayer().startTurn();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasWinner() {
		return (winner != null);
	}

	/**
	 * {@inheritDoc}
	 */
	public ITeam getWinner() {
		return winner;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<? extends ITeam> getLosers() {
		// Make losers = teams-winner.
		List<ITeam> losers = new ArrayList<ITeam>(teams);
		losers.remove(winner);
		return losers;
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public void endTurn() {
		getActivePlayer().endTurn();
		nextTurn();

		// Whatever... Let's just send the whole thing...
		// This is so bad, that it should be refactored away ASAP! A S A P ! ! !
		changeBus.send(LegacyGameModelSync.getInstance(this));
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public IFigure getActiveFigure() {
		return getActivePlayer().getActiveFigure();
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public List<? extends ITeam> getTeams() {
		return teams;
	}
}
