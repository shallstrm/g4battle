package se.ninjabunnies.server.model;

import se.ninjabunnies.shared.model.IImmutableWeapon;

public class Weapon implements IImmutableWeapon {
	private String weaponName;
	private int weaponDamage;
	private int weaponSpeed;
	private int weaponRange;

	public Weapon() {
		weaponName = "Club";
		weaponDamage = 1;
		weaponSpeed = 1;
		weaponRange = 1;
	}

	public Weapon(String weaponName) {
		this.weaponName = weaponName;
		weaponDamage = 1;
		weaponSpeed = 1;
		weaponRange = 1;
	}

	public Weapon(String weaponName, int weaponDamage, int weaponSpeed,
			int weaponRange) {
		this.weaponName = weaponName;
		this.weaponDamage = weaponDamage;
		this.weaponSpeed = weaponSpeed;
		this.weaponRange = weaponRange;
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public String getWeaponName() {
		return weaponName;
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public int getWeaponDamage() {
		return weaponDamage;
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public int getWeaponSpeed() {
		return weaponSpeed;
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public int getWeaponRange() {
		return weaponRange;
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public String toString() {
		return  weaponName + System.getProperty("line.separator") +"Damage="
				+ weaponDamage + System.getProperty("line.separator") + "Speed=" + weaponSpeed
				+ System.getProperty("line.separator") + "Range=" + weaponRange ;
	}

}
