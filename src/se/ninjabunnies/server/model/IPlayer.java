package se.ninjabunnies.server.model;

import se.ninjabunnies.shared.model.IReadonlyPlayer;

public interface IPlayer extends IReadonlyPlayer {
	/**
	 * Adds a <code>Figure</code> that the player is supposed to own.
	 * 
	 * <p>This is done for performance reasons.
	 * 
	 * @param figure the figure to own.
	 */
	public void addFigure(IFigure figure);

	/** Initiates a turn
	 * sets all figures ready, checks if any are dead
	 * if all dead, returns false, if at least one
	 * is alive, returns true
	 */
	public boolean startTurn();

	/**
	 * Changes the active figure to the next unused one.
	 * Will not change active Figure if all are used.
	 */
	public void changeNextActiveFigure();

	/**
	 * Changes the active figure to a specific figure in line.
	 * @param index The index of the figure wanted.
	 * @throws IndexOutOfBoundsExeption when the given index does not exist
	 */
	public void changeActiveFigure(int index);

	/**
	 * Change active figure to specific figure.
	 * If the figure is null or there are no figures to be active the active figure will not be changed.
	 * @param figure the figure to become the active figure
	 */
	public void changeActiveFigure(IFigure figure);

	/**
	 * Set the active team for a player.
	 * @param team the team.
	 */
	public void setTeam(ITeam team);

	/**
	 * Notify this player that the <code>Figure</code> was killed.
	 * @param figure the Figure that isn't alive anymore.
	 */
	public void notifyFigureNotAlive(Figure figure);
	
	/**
	 * {@inheritDoc}
	 * Note: Changes return type to a sub-type of super's implementation.
	 */
	public IFigure getActiveFigure();
	
	/**
	 * {@inheritDoc}
	 * Note: Changes return type to a sub-type of super's implementation.
	 */
	@Override
	public ITeam getTeam();
	
	/**
	 * Used when player presses End Turn-button. Sets all figures to used.
	 */
	public void endTurn();

	/**
	 * Sets the starting position the player will have when spawned on a map.
	 * 
	 * @param startPosition the zero-based position index to request.
	 * @throws IllegalArgumentException if startingPosition &lt; 0.
	 */
	public void setStartingPosition(int startingPosition);
}