package se.ninjabunnies.server.model;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import se.ninjabunnies.server.net.IServerChangeBus;
import se.ninjabunnies.shared.descriptor.IMapDescriptor;
import se.ninjabunnies.shared.descriptor.TileFigurePair;
import se.ninjabunnies.shared.model.IReadonlyFigure;
import se.ninjabunnies.shared.util.Position;

/**
 * @author Simone Damaschke
 * @author labbGrupp4
 * @version 0.1 A class to represent the board or table of the game.
 */

public class Board implements IBoard {
	private Tile[][] tiles;
	
	/** A cached width of the Board. */
	private final int width;
	
	/** A cached height of the board. */
	private final int height;

	/** The change bus used for this game instance. */
	@SuppressWarnings("unused") // We will use this later!
	private IServerChangeBus changeBus;
	
	/**
	 * Create a new <code>Board</code>, which represents a game area with
	 * <code>Tile</code>s (which sometimes have <code>Figure</code>s on them).
	 * 
	 * @param mapDescriptor
	 *            the <code>MapDescriptor</code> to base the <code>Board</code>
	 *            of.
	 * 
	 * @param players
	 *            a list of <code>Player</code>s that should receive
	 *            notifications about owning new <code>Figure</code> through {
	 *            {@link Player#addFigure(Figure)}. The players must have unique
	 *            starting positions.
	 *            
	 * @param changeBus
	 *            the change bus to use for this game instance.
	 *            
	 * @throws IllegalArgumentException if two players share starting position.
	 */
	public Board(IMapDescriptor mapDescriptor, List<IPlayer> players,
			IServerChangeBus changeBus) {
		this.changeBus = changeBus;
		
		TileFigurePair[][] map = mapDescriptor.getMap();
		width = mapDescriptor.getWidth();
		height = mapDescriptor.getHeight();

		// Assign starting positions and if added with all players.
		class PlayerMeta {
			IPlayer player;
			boolean added = false;
			
			PlayerMeta(IPlayer player) {
				this.player = player;
			}
		}
		Map<Integer, PlayerMeta> startingPositions =
				new HashMap<Integer, PlayerMeta>(players.size());
		for(IPlayer player : players) {
			int startingPosition = player.getStartingPosition();
			if(startingPositions.get(startingPosition) == null) {
				startingPositions.put(startingPosition, new PlayerMeta(player));
			} else {
				throw new IllegalArgumentException(
						"player starting position already in use");
			}
		}
		
		tiles = new Tile[width][height];
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				TileFigurePair tileFigurePair = map[x][y];

				// Goes through map and creates corresponding tiles.
				tiles[x][y] = new Tile(tileFigurePair.getTileDescriptor());

				// If there is a figure, we want to create it as well.
				if (tileFigurePair.hasFigure()) {
					PlayerMeta playerMeta = startingPositions.get(
							tileFigurePair.getPlayerId());
					// If no player requests this starting position, skip figure
					if(playerMeta != null) {
						IPlayer player = playerMeta.player;
						playerMeta.added = true;
						
						IFigure figure = new Figure(tileFigurePair
								.getFigureDescriptor(), player, changeBus);
						tiles[x][y].setFigure(figure);
	
						// Notify player that it owns a new figure (performance)
						player.addFigure(figure);
					} else {
						// This position has no player.
					}
				}
			}
		}
		
		// Make sure every player was indeed added. Hard to track error!
		for(PlayerMeta playerMeta : startingPositions.values()) {
			if(!playerMeta.added) {
				throw new IllegalArgumentException(
						"unused players not matching any starting position");
			}
		}
		
		// Generate the neighbor linking between Tiles.
		generateNeighborLinks();
	}
	
	/** Needed for Kryo serialization. */
	@SuppressWarnings("unused")
	private Board() {
		// Kryo will overwrite these...
		width = 0;
		height = 0;
	}
		
	/**
	 * {@inheritDoc}
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * {@inheritDoc}
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * Generates the neighbor linking between all Tiles.
	 */
	private void generateNeighborLinks() {
		for(int x = 0; x < width; x++) {
			for(int y = 0; y < height; y++) {
				// Those are just positions, if they "exist" is checked below.
				Position[] neighborPositions = getNeighbors(new Position(x, y));
				
				// post-null-padded neighbor Tiles, if our Tile has <6 neighbors.
				Tile[] realNeighbors = new Tile[MAX_NEIGHBORS];
				int realNeighborCount = 0;
				
				for(Position position : neighborPositions) {
					final int neighborX = position.getX();
					final int neighborY = position.getY();
					if(neighborX >= 0 && neighborX < width
							&& neighborY >= 0 && neighborY < height) {
						realNeighbors[realNeighborCount] =
								tiles[neighborX][neighborY];
						realNeighborCount++;
					}
				}
				
				// Make the new array nicer, without trailing nulls...
				if(realNeighborCount < MAX_NEIGHBORS) {
					realNeighbors = Arrays.copyOf(realNeighbors,
						realNeighborCount);
				}
				
				tiles[x][y].setNeighbors(realNeighbors);
			}
		}
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public ITile getTile(Position position) {
		return tiles[position.getX()][position.getY()];
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public ITile getTile(IReadonlyFigure figure) {
		//@todo Fix performance
		for (ITile[] listOfTiles : tiles) {
			for (ITile tile : listOfTiles) {
				if (figure.equals(tile.getFigure())) {
					return tile;
				}
			}

		}

		return null;
	}

	/**
	 * Gets an array with 6 elements containing all neighbors to a position in
	 * the hexagon grid used by Board.
	 * Note that neighbors with negative coordinates might get returned.
	 */
	private static Position[] getNeighbors(Position position) {
		// To get neighbors in a hexagon grid with hexagons pointing upwards,
		// we have to get the positions (x+-1, y), (x, y+-1) and for odd rows
		// (x+1, y+-1) - in contrast to (x-1, y+-1) for even ones.
		int x = position.getX();
		int y = position.getY();
		int push = y%2 == 0 ? -1 : 1;
		return new Position[] {
			new Position(x+1, y),
			new Position(x-1, y),
			new Position(x, y+1),
			new Position(x, y-1),
			new Position(x+push, y+1),
			new Position(x+push, y-1)
		};
	}
	
}
