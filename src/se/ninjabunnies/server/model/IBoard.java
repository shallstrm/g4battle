package se.ninjabunnies.server.model;

import se.ninjabunnies.shared.model.IReadonlyBoard;
import se.ninjabunnies.shared.model.IReadonlyFigure;
import se.ninjabunnies.shared.util.Position;

public interface IBoard extends IReadonlyBoard {
	/**
	 * {@inheritDoc}
	 * Note: Changes return type to a sub-type of super's implementation.
	 */
	@Override
	public abstract ITile getTile(Position position);

	/**
	 * {@inheritDoc}
	 * Note: Changes return type to a sub-type of super's implementation.
	 */
	@Override
	public abstract ITile getTile(IReadonlyFigure figure);
}