package se.ninjabunnies.server.model;

import java.util.ArrayList;
import java.util.List;

import se.ninjabunnies.shared.model.IReadonlyPlayer;
import se.ninjabunnies.shared.model.IReadonlyTeam;


/**
 * @author Simone Damaschke (re-hacked by Joel Severin)
 * @author labbGrupp4
 * @version 0.1
 * A class to represent the teams competing in the game.
 */
public class Team implements ITeam {
	/** The team name */
	private String name;
	
	/** The players in the team. */
	private List<IPlayer> players;
	
	/** The current player in the queue. */
	private int currentPlayer = 0;
	
	/**
	 * Create a team with these <code>Player</code>s.
	 * 
	 * @param String the name of the team. (Consider using "Team xxx".)
	 * @param List<Player> the players in the team. Cannot be empty.
	 * @throws IllegalArgumentException if <code>players</code> is empty.
	 */
	public Team(String name, List<IPlayer> players) {
		this.name = name;
		
		if(players.size() <= 0) {
			throw new IllegalArgumentException("players cannot be empty");
		}
		
		this.players = players;
		
		// This is a performance optimization for the Player class.
		for(IPlayer player : players) {
			player.setTeam(this);
		}
	}
	
	/**
	 * Create a new team from a <code>IReadonlyTeam</code>.
	 * @param readonly
	 */
	public Team(IReadonlyTeam readonly) {
		this(readonly.getName(), copyPlayers(readonly.getPlayers()));
	}
	
	/**
	 * Constructor helper method for creating players from readonly list.
	 * 
	 * @param readonly the <code>IReadonlyPlayer</code> list.
	 * @return the new real IPlayer list.
	 */
	private static List<IPlayer> copyPlayers(
			List<? extends IReadonlyPlayer> readonly) {
		List<IPlayer> newPlayers = new ArrayList<IPlayer>(readonly.size());
		for(IReadonlyPlayer readonlyPlayer : readonly) {
			newPlayers.add(new Player(readonlyPlayer));
		}
		return newPlayers;
	}
	
	/** Needed for Kryo serialization. */
	@SuppressWarnings("unused")
	private Team() {
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public void nextActivePlayer() {
		int oldPlayer = currentPlayer;
		while(true) {
			// Rotate turn.
			currentPlayer++;
			if(currentPlayer >= players.size()) {
				currentPlayer = 0;
			}
			
			// If we get back to the old turn, give up.
			// If we find an active player, make it current.
			if(oldPlayer == currentPlayer
					|| players.get(currentPlayer).isActive()) {
				return;
			}
		}
	}
	
	/**
	 * {{@inheritDoc}
	 */
	@Override
	public IPlayer getCurrentPlayer() {
		return players.get(currentPlayer);
	}
	
	/**
	 * {{@inheritDoc}
	 */
	@Override
	public List<IPlayer> getPlayers() {
		return players;
	}
	
	/**
	 * {{@inheritDoc}
	 */
	@Override
	public int getPlayerCount() {
		return players.size();
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public boolean isActive() {
		// @todo make optimization, like having a boolean maybeActive = true;
		for(IPlayer player : players) {
			if(player.isActive()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public String getName() {
		return name;
	}
	
	/**
	 * {{@inheritDoc}
	 */
	@Override
	public void addPlayer(IPlayer player) {
		players.add(player);
		player.setTeam(this);
	}

}
