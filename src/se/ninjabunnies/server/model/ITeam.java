package se.ninjabunnies.server.model;

import java.util.List;

import se.ninjabunnies.shared.model.IReadonlyTeam;

public interface ITeam extends IReadonlyTeam {
	/**
	 * Sets turn to the next active player in line, if any.
	 */
	public abstract void nextActivePlayer();
	
	/**
	 * {@inheritDoc}
	 * Note: Changes return type to a sub-type of super's implementation.
	 */
	@Override
	public IPlayer getCurrentPlayer();

	/**
	 * {@inheritDoc}
	 * Note: Changes return type to a sub-type of super's implementation.
	 */
	@Override
	public List<? extends IPlayer> getPlayers();
	
	/**
	 * Adds a player
	 */
	public void addPlayer(IPlayer player);
}
