package se.ninjabunnies.server.model;

import java.util.List;

import se.ninjabunnies.shared.model.IReadonlyGameModel;
import se.ninjabunnies.shared.util.Position;

public interface IGameModel extends IReadonlyGameModel {
	/**
	 * Sets turn to the next active team in line.
	 */
	public abstract void nextActiveTeam();

	/**
	 * {@inheritDoc}
	 * Note: Changes return type to a sub-type of super's implementation.
	 */
	@Override
	public ITeam getActiveTeam();

	/**
	 * {@inheritDoc}
	 * Note: Changes return type to a sub-type of super's implementation.
	 */
	@Override
	public IBoard getBoard();

	/**
	 * Perform an action in the tile specified by <code>position</code>.
	 * 
	 * <p>This method performs all actions on tiles. Actions can be thought as a
	 * click or enter-press by the user. This method decides if anything should
	 * be done at all, and if it should, performs that actions.
	 * 
	 * @param position the position of the Tile that received the action.
	 */
	public abstract void tileActionPerformed(Position position);
	
	/**
	 * {@inheritDoc}
	 * Note: Changes return type to a sub-type of super's implementation.
	 */
	@Override
	public ITeam getWinner();
	
	/**
	 * {@inheritDoc}
	 * Note: Changes return type to a sub-type of super's implementation.
	 */
	@Override
	public List<? extends ITeam> getLosers();
	
	/**
	 * Ends the current turn.
	 */
	public void endTurn();

	/**
	 * {@inheritDoc}
	 * Note: Changes return type to a sub-type of super's implementation.
	 */
	@Override
	public IPlayer getActivePlayer();
	
	/**
	 * {@inheritDoc}
	 * Note: Changes return type to a sub-type of super's implementation.
	 */
	@Override
	public IFigure getActiveFigure();
	
	/**
	 * {@inheritDoc}
	 * Note: Changes return type to a sub-type of super's implementation.
	 */
	@Override
	public List<? extends ITeam> getTeams();
}