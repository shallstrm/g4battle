package se.ninjabunnies.server.model;

import se.ninjabunnies.shared.model.IReadonlyFigure;
import se.ninjabunnies.shared.model.IImmutableWeapon;

public interface IFigure extends IReadonlyFigure {
	/**
	 * set's the figure to be used
	 */
	public abstract void setUsed();

	/**
	 * set's the figure to be ready for use
	 */
	public abstract void setReady();

	/**
	 * @param IFigure
	 * throws an attack against the given IFigure
	 * if they're the same team it will sulk and throw axes
	 * @throws IndexOutOfBoundException
	 * 
	 */
	public abstract void attackIt(IFigure figure);
	
	/** Given a weapon, the figure will hurt itself with it
	 *  should not be called outside IFigure implementers
	 *  it is only declared here so that the Figures properly 
	 *  can be built against the IFigure interface, avoiding trouble in the future
	 * @param weapon
	 */
	public abstract void recieveAttack(IImmutableWeapon weapon);

	/**
	 * Given a distance, it tells the figure that it has moved that distance
	 * if distance is bigger than the allowed movement (movement - the distance moved
	 * since the last figure.setReady() call), it throws an IllegalArguementException
	 * @param distance
	 * 					the distance to move.
	 * @throws IllegalArguementException
	 * 										Thrown if distance > (movement - distanceMoved)
	 */
	public void move(int distance);
	
	/**
	 * {@inheritDoc}
	 * Note: Changes return type to a sub-type of super's implementation.
	 */
	@Override
	public IPlayer getPlayer();
	
	
}
