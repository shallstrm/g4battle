package se.ninjabunnies.server.model;

import java.util.ArrayList;
import java.util.List;

import se.ninjabunnies.shared.model.IReadonlyPlayer;
import se.ninjabunnies.shared.util.ColorGenerator.ColorDescriptor;

/**
 * @author Simone Damaschke
 * @author labbGrupp4
 * @version 0.1 A class to represent the players to play the game.
 */

public class Player implements IPlayer {
	private List<IFigure> figures = new ArrayList<IFigure>();
	private int activeFigure = 0;
	private String name;
	private ITeam team;
	private boolean active = true;
	private ColorDescriptor color = ColorDescriptor.BLUE;
	
	/** The starting position the player requests to take when added on a map */
	private int startingPosition = -1;

	/**
	 * Player Constructor
	 * @param name the players name
	 * @param color the color to represent the player
	 */
	public Player(String name, ColorDescriptor color) {
		this.name = name;
		if(color == null) {
			throw new IllegalArgumentException("color cannot be null");
		}
		this.color = color;
	}
	
	/**
	 * Create a new player from a <code>IReadonlyPlayer</code> instance.
	 * 
	 * @param readonly the <code>IReadonlyPlayer</code> template.
	 */
	public Player(IReadonlyPlayer readonly) {
		this(readonly.getName(), readonly.getColor());
		startingPosition = readonly.getStartingPosition();
	}
	
	/** Needed for Kryo serialization. */
	@SuppressWarnings("unused")
	private Player() {
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public void addFigure(IFigure figure) {
		figures.add(figure);
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public boolean startTurn() {
		boolean anyoneAlive = false;
		for (IFigure figure : figures) {
			if (figure.isAlive()) {
				figure.setReady();
				anyoneAlive = true;
			}
		}
		
		// If the (old) current figure is used, change to another one.
		if(anyoneAlive && getActiveFigure().isUsed()) {
			changeNextActiveFigure();
		}

		return anyoneAlive;
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public void changeNextActiveFigure() {
		// Check for figure after current
		for (int i = activeFigure + 1; i < figures.size(); i++) {
			if (!figures.get(i).isUsed()) {
				activeFigure = i;
				return;
			}
		}
		// Check for figure before current
		for (int i = activeFigure - 1; i >= 0; i--) {
			if (!figures.get(i).isUsed()) {
				activeFigure = i;
				return;
			}
		}
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public void changeActiveFigure(int index) {
		if (index < figures.size() && index >= 0) {
			activeFigure = index;
		} else {
			throw new IndexOutOfBoundsException(
					"there are only figures form 0 to " + (figures.size() - 1));
		}
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public void changeActiveFigure(IFigure figure) {
		if (figures.size() != 0 && figure != null) {
			for (int i = 0; i < figures.size(); ++i) {
				IFigure temp = figures.get(i);
				if (temp == figure) {
					activeFigure = i;
					return;
				}
			}
		}
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public IFigure getActiveFigure() {
		if (figures.size() != 0) {
			return figures.get(activeFigure);
		}
		return null;
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public ITeam getTeam() {
		return team;
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public void setTeam(ITeam team) {
		this.team = team;
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public boolean isActive() {
		return active;
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public void notifyFigureNotAlive(Figure figure) {
		// If the active figure was killed, change to next if possible.
		if (figure.equals(getActiveFigure())) {
			changeNextActiveFigure();
		}

		// @todo probably break out as separate List.
		// We do this right now to finish iter. 1.
		// If we find none alive players, mark us as inactive.
		boolean foundSomeoneAlive = false;
		for (IFigure currentFigure : figures) {
			if (currentFigure.isAlive()) {
				foundSomeoneAlive = true;
				break;
			}
		}

		if (!foundSomeoneAlive) {
			this.active = false;
		}
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public boolean isAllFiguresUsed() {
		boolean foundSomeoneUnused = false;
		for (IFigure figure : figures) {
			if (!figure.isUsed()) {
				foundSomeoneUnused = true;
				break;
			}
		}
		return !foundSomeoneUnused;
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public ColorDescriptor getColor() {
		return color;
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public void endTurn() {
	
		for(IFigure figure: figures) {
			figure.setUsed();
		}
		
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public int getStartingPosition() {
		return startingPosition;
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public void setStartingPosition(int startingPosition) {
		if(startingPosition < 0) {
			throw new IllegalArgumentException("starting position set below 0");
		}
		this.startingPosition = startingPosition;
	}
}
