package se.ninjabunnies.server.model;

import se.ninjabunnies.shared.model.IReadonlyTile;

public interface ITile extends IReadonlyTile {
	/**
	 * Sets which figure is standing on the tile. If there is none this should
	 * be set to <code>null</code>.
	 * 
	 * @param figure2
	 *            The figure to stand on the tile.
	 */
	public void setFigure(IFigure figure2);

	/**
	 * Move the figure standing on this tile to the <code>other</code> tile,
	 * which will be counted as it taking <code>moveLength</code> steps. If the
	 * step is smaller than 1, the figure will not be notified at all.
	 * 
	 * @param tile the tile to move to.
	 * @param moveLength the length the figure will be notified of moving.
	 */
	public void moveFigureTo(ITile other, int moveLength);

	/**
	 * Sets the neighboring tiles. This should be run after construction.
	 * 
	 * @param neighbors
	 *            the neighbors.
	 */
	public void setNeighbors(ITile[] neighbors);
	
	/**
	 * {@inheritDoc}
	 * Note: Changes return type to a sub-type of super's implementation.
	 */
	@Override
	public IFigure getFigure();
}