package se.ninjabunnies.server.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import se.ninjabunnies.shared.descriptor.TileDescriptor;
import se.ninjabunnies.shared.model.IReadonlyTile;
import se.ninjabunnies.shared.util.ICondition;


/**
 * @author Simone Damaschke
 * @author labbGrupp4
 * @version 0.1 A class to represent a single tile to put on the board.
 */

public class Tile implements ITile {
	private se.ninjabunnies.server.model.IFigure figure = null;
	private TileDescriptor tileDescriptor;

	/** Cache of neighboring Tiles. */
	private ITile[] neighbors;

	/**
	 * Construct a new Tile. Make sure setNeighbors() is run afterwards.
	 * 
	 * @param type
	 *            the tile type.
	 */
	public Tile(TileDescriptor type) {
		this.tileDescriptor = type;
	}
	
	/** Needed for Kryo serialization. */
	@SuppressWarnings("unused")
	private Tile() {
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public void setFigure(IFigure figure2) {
		this.figure = figure2;
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public IFigure getFigure() {
		return figure;
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public boolean hasFigure() {
		return figure != null;
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public void moveFigureTo(ITile tile, int moveLength) {
		IFigure figure = getFigure();
		setFigure(null);
		tile.setFigure(figure);
		
		if(moveLength > 0) {
			figure.move(moveLength);
		}
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public void setNeighbors(ITile[] neighbors) {
		this.neighbors = neighbors;
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public int getPathLength(IReadonlyTile target, int maxLength,
			final ICondition<IReadonlyTile> condition) {
		//                  other is ourselves, path length 0.
		if(maxLength < 1 || this.equals(target)) {
			return 0;
		}
		
		// This holds Tiles and cached info about whether it meets Condition.
		class TileMeta {
			IReadonlyTile tile;
			boolean meetsCondition;
			
			public TileMeta(IReadonlyTile tile) {
				this(tile, condition.isMetFor(tile));
			}
			
			public TileMeta(IReadonlyTile tile, boolean meetsCondition) {
				this.tile = tile;
				this.meetsCondition = meetsCondition;
			}
		}
		
		// Hold all length rings, and the 0 ring too.
		// Avoid the temptation to make it a Set (ITile.equals(TileMeta)...)
		List<List<TileMeta>> rings = new ArrayList<List<TileMeta>>(maxLength+1);
		
		// Seed with ourselves (checked that this != target above).
		// this should be marked as *always* meeting the condition.
		List<TileMeta> ring0 = new ArrayList<TileMeta>(1);
		ring0.add(new TileMeta(this, true));
		rings.add(ring0);
		
		for(int level = 1; level <= maxLength; level++) {
			// Create a new ring. 6*level is the node count in the ring.
			List<TileMeta> ring = new ArrayList<TileMeta>(6*level);
			rings.add(ring);
			
			// Get the two previous rings (might be the ring with "this" in, and
			// in that case, an empty ring too).
			List<TileMeta> ringBefore1 = rings.get(level-1);
			List<TileMeta> ringBefore2;
			if(level > 1) {
				ringBefore2 = rings.get(level-2);
			} else {
				ringBefore2 = new ArrayList<TileMeta>(0);
			}
			
			// Get neighbors of the previous ring. If they don't exist in the
			// previous ring or the ring before that (2 before), add them to
			// the new ring. This exploits the nature of the hexagon grid.
			for(TileMeta ringBefore1Tile : ringBefore1) {
				// Way not possible. Abort checking this Tile's neighbors.
				if(!ringBefore1Tile.meetsCondition) {
					continue;
				}
				
				for(IReadonlyTile neighbor : ringBefore1Tile.tile.getNeighbors()) {
					// We found the target within the allowed length! Return!
					if(target.equals(neighbor)) {
						if(condition.isMetFor(neighbor)) {
							return level;
						} else {
							return 0;
						}
					}
					
					// Add the neighbor, if not present in any previous 2 rings.
					boolean isFound = false;
					for(TileMeta tileMeta : ringBefore2) {
						if(neighbor.equals(tileMeta.tile)) {
							isFound = true;
							break;
						}
					}
					if(!isFound) {
						for(TileMeta tileMeta : ringBefore1) {
							// @todo Get rid of duplicate code.
							if(neighbor.equals(tileMeta.tile)) {
								isFound = true;
								break;
							}
						}
					}
					if(!isFound) {
						ring.add(new TileMeta(neighbor));
					}
				}
			}
		}

		return 0;
	}
	
	/**
	 * {{@inheritDoc}}
	 */
	@Override
	public List<List<? extends IReadonlyTile>> getNeighborRings(
			final ICondition<IReadonlyTile> condition, int length) {
		if(length < 1) {
			return Collections.emptyList();
		}
		
		// @todo we should move this class out from here (code duplication).
		// This holds Tiles and cached info about whether it meets Condition.
		class TileMeta {
			IReadonlyTile tile;
			boolean meetsCondition;
			
			public TileMeta(IReadonlyTile tile) {
				this(tile, condition.isMetFor(tile));
			}
			
			public TileMeta(IReadonlyTile tile, boolean meetsCondition) {
				this.tile = tile;
				this.meetsCondition = meetsCondition;
			}
		}
		
		// Hold all length rings, and the 0 ring too.
		// Avoid the temptation to make it a Set (ITile.equals(TileMeta)...)
		List<List<TileMeta>> rings = new ArrayList<List<TileMeta>>(length+1);
		
		// @todo Ring could be an object, featuring this property...
		// Cache of how many tiles that meet a condition in a certain ring.
		List<Integer> tilesMeetingConditionInRing =
				new ArrayList<Integer>(length+1);
		
		// Seed with ourselves.
		// this should be marked as *always* meeting the condition.
		List<TileMeta> ring0 = new ArrayList<TileMeta>(1);
		ring0.add(new TileMeta(this, true));
		rings.add(ring0);
		tilesMeetingConditionInRing.add(1);
		
		
		for(int level = 1; level <= length; level++) {
			// Create a new ring. 6*level is the node count in the ring.
			List<TileMeta> ring = new ArrayList<TileMeta>(6*level);
			int meetingConditionCount = 0;
			
			// Get the two previous rings (might be the ring with "this" in, and
			// in that case, an empty ring too).
			List<TileMeta> ringBefore1 = rings.get(level-1);
			List<TileMeta> ringBefore2;
			if(level > 1) {
				ringBefore2 = rings.get(level-2);
			} else {
				ringBefore2 = new ArrayList<TileMeta>(0);
			}
			
			// Get neighbors of the previous ring. If they don't exist in the
			// previous ring or the ring before that (2 before), add them to
			// the new ring. This exploits the nature of the hexagon grid.
			for(TileMeta ringBefore1Tile : ringBefore1) {
				// Way not possible. Abort checking this Tile's neighbors.
				if(!ringBefore1Tile.meetsCondition) {
					continue;
				}
				
				for(IReadonlyTile neighbor : ringBefore1Tile.tile.getNeighbors()) {
					// Add the neighbor, if not present in any previous 2 rings.
					boolean isFound = false;
					for(TileMeta tileMeta : ringBefore2) {
						if(neighbor.equals(tileMeta.tile)) {
							isFound = true;
							break;
						}
					}
					if(!isFound) {
						for(TileMeta tileMeta : ringBefore1) {
							// @todo Get rid of duplicate code.
							if(neighbor.equals(tileMeta.tile)) {
								isFound = true;
								break;
							}
						}
					}
					if(!isFound) {
						ring.add(new TileMeta(neighbor));
						meetingConditionCount++;
					}
				}
			}
			
			// If there were any tiles meeting condition in the outermost ring.
			if(meetingConditionCount > 0) {
				rings.add(ring);
				tilesMeetingConditionInRing.add(meetingConditionCount);
			} else {
				// We should really give up here...
				break;
			}
		}

		// We have to return clean data, i.e. strip the first ring, and then
		// unwrap TileMeta objects if they meet the condition. Tiles that don't
		// meet the condition is not returned. ring0 (this) it not returned.
		List<List<? extends IReadonlyTile>> cleanRings =
				new ArrayList<List<? extends IReadonlyTile>>(rings.size()-1);
		int ringLevel = 0;
		for(List<TileMeta> tileMetas : rings) {
			// Skip the 0 ring, containing only this tile...
			if(ringLevel != 0) {
				List<IReadonlyTile> cleanRing = new ArrayList<IReadonlyTile>(
						tilesMeetingConditionInRing.get(ringLevel));
				for(TileMeta tileMeta : tileMetas) {
					if(tileMeta.meetsCondition) {
						cleanRing.add(tileMeta.tile);
					}
				}
				cleanRings.add(cleanRing);
			}
			ringLevel++;
		}
		
		return cleanRings;
	}

	/**
	 * {{@inheritDoc}}
	 */
	public TileDescriptor getTileDescriptor() {
		return tileDescriptor;
	}
		
	/**
	 * {{@inheritDoc}}
	 */
	@Override
	public ITile[] getNeighbors() {
		return neighbors;
	}
}