package se.ninjabunnies.server.model;

import se.ninjabunnies.server.net.IServerChangeBus;
import se.ninjabunnies.server.net.NullServerChangeBus;
import se.ninjabunnies.shared.descriptor.FigureDescriptor;
import se.ninjabunnies.shared.model.IImmutableHealth;
import se.ninjabunnies.shared.model.IReadonlyTeam;
import se.ninjabunnies.shared.model.IReadonlyTile;
import se.ninjabunnies.shared.model.IImmutableWeapon;
import se.ninjabunnies.shared.net.state.FigureHealthChange;
import se.ninjabunnies.shared.util.ICondition;


/**
 * @author Simon Hallström
 * @author labbGrupp4
 * Version 0.2
 * An object class for building Figures
 * each figure has health, weapon, armour and movement
 */

public class Figure implements IFigure {
	private final FigureDescriptor figureDescriptor;
	private Health health;
	private int movable;
	private final IPlayer player;
	private boolean used = true;
	private boolean alive = true;
	
	/** The change bus used for this game instance. */
	IServerChangeBus changeBus;
	
	/** The figure id associated with the change bus. */
	private final int figureId;
	
	/**
	 * Standard constructor, try to avoid using it. Uses Ninja Bunny.
	 */
	public Figure(IPlayer player) {
		this(FigureDescriptor.NINJA_BUNNY, player, new NullServerChangeBus());
	}
	
	/**
	 * @param FigureDescriptor
	 * 					the FigureDescriptor describing this Figure type.
	 * @param Player
	 * 					the player a figure belongs to
	 * @param changeBus	the change bus used for this game instance.
	 */
	public Figure(FigureDescriptor figureDescriptor, IPlayer player,
			IServerChangeBus changeBus) {
		this.changeBus = changeBus;
		this.figureId = changeBus.nextFigureId();
		
		this.figureDescriptor = figureDescriptor;
		this.health = new Health(figureDescriptor.getMaxHealth());
		this.player = player;
		this.movable = figureDescriptor.getMovement();
	}
	
	/** Needed for Kryo serialization. */
	@SuppressWarnings("unused")
	private Figure() {
		// Kryo will overwrite these...
		figureDescriptor = null;
		player = null;
		figureId = 0;
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public IImmutableHealth getHealth() {
		return health;
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public IImmutableWeapon getWeapon() {
		return new Weapon(figureDescriptor.getWeaponName(),
		                  figureDescriptor.getWeaponDamage(),
		                  figureDescriptor.getWeaponSpeed(),
		                  figureDescriptor.getWeaponRange());
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public int getArmour() {
		return figureDescriptor.getArmour();
	}
	/**
	 * 
	 * @param attacking weapon
	 * attacks the unit using the specific weapon, damage done is (weaponDamage - armour) * weaponSpeed
	 */
	public void recieveAttack(IImmutableWeapon attack) {
		int armour = getArmour();
		int i2 = attack.getWeaponSpeed();
		if (attack.getWeaponDamage() < armour) {
			health = new Health(health.getCurrentHealth() - i2, health.getMaxHealth());
		} else {
			for (int i = 0; i < i2; i++) {
				health = new Health(health.getCurrentHealth() - (attack.getWeaponDamage() - armour), health.getMaxHealth());
			}
		}
		if ( health.getCurrentHealth() <= 0) {
			setAlive(false);
			
		}
		changeBus.send(FigureHealthChange.getInstance(figureId,
				health.getCurrentHealth()));
	}
	/**
	 * {{@inheritDoc}
	 */
	@Override
	public int getMovement() {
		return figureDescriptor.getMovement();
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public boolean isUsed() {
		return used;
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public void setUsed() {
		used = true;
	}
	/**
	 * {{@inheritDoc}
	 */
	@Override
	public void setReady() {
		used = false;
		movable = figureDescriptor.getMovement();
	}
	
	/**
	 * {{@inheritDoc}
	 */
	@Override
	public IPlayer getPlayer() {
		return player;
	}
	
	/**
	 * Mark the Figure as alive or not.
	 */
	private void setAlive(boolean alive) {
		this.alive = alive;
		
		// Optimization for the Player class.
		if(alive == false) {
			player.notifyFigureNotAlive(this);
		}
	}
	
	public boolean isAlive() {
		return alive;
	}
	/**
	 * {{@inheritDoc}
	 */
	@Override
	public void attackIt(IFigure figure) {
		if (this.isUsed() == true) {
			
		} else {
			figure.recieveAttack(getWeapon());
			this.setUsed();
		}
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public boolean inTeam(IReadonlyTeam team) {
		return team.equals(player.getTeam());
	}
	/**
	 * {{@inheritDoc}
	 */
	@Override
	public void move(int distance) {
		if(used) {
			throw new IllegalArgumentException("figure is used and can thus not move");
		} else if (distance > (movable)) {
			throw new IllegalArgumentException("distance not beween 0 and gigure.getMovement-figure.moved");
		} else {
			movable = movable - distance;
		}
	}
	/**
	 * {{@inheritDocs}
	 */
	@Override
	public int distanceAbleToMove() {
		return movable;
	}
	
	/**
	 * {{@inheritDoc}
	 */
	@Override
	public boolean canAttackBetween(IReadonlyTile first, IReadonlyTile second) {
		// @todo Add real condition, checking if the figure actually can attack.
		ICondition<IReadonlyTile> condition = new ICondition<IReadonlyTile>() {
			@Override
			public boolean isMetFor(IReadonlyTile tile) {
				return true;
			}
		};
		return (first.getPathLength(second, getWeapon().getWeaponRange(),
				condition) > 0);
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public int getMovePathLength(IReadonlyTile first,
			IReadonlyTile second) {
		return first.getPathLength(second, getMovement(), getMoveCondition());
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public boolean canTraverse(IReadonlyTile tile) {
		return figureDescriptor.canTraverseTile(tile.getTileDescriptor());
	}

	/**
	 * {{@inheritDoc}
	 */
	@Override
	public FigureDescriptor getFigureDescriptor() {
		return figureDescriptor;
	}
	
	/**
	 * {{@inheritDoc}
	 */
	@Override
	public ICondition<IReadonlyTile> getMoveCondition() {
		
		ICondition<IReadonlyTile> condition = new ICondition<IReadonlyTile>() {
			//@Override
			public boolean isMetFor(IReadonlyTile tile) {
				return !tile.hasFigure()&&Figure.this.canTraverse(tile);
			}
		};
		
		return condition;
		
	}
}
