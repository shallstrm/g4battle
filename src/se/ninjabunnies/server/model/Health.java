package se.ninjabunnies.server.model;

import se.ninjabunnies.shared.model.IImmutableHealth;


public class Health implements IImmutableHealth {
		private int maxHealth;
		private int currentHealth;
		
		/* Basic constructor
		 * Gives HP/maxHP at a value of 1
		 */
		public Health () {
			maxHealth = 1;
			currentHealth = 1;
		}
		/* Preferred constructor
		 * Gives HP/maxHP the same as the given value 
		 */
		public Health (int health) {
			maxHealth = health;
			currentHealth = health;
		}
		/** Special constructor
		 * First value is the current HP, and the second is
		 * max HP, used for special circumstances.
		 * if currentHealth > maxHealth, both current and max will be set at the given maxHealth value
		 */
		public Health (int currentHealth, int maxHealth) {
			if (currentHealth > 0 && maxHealth > 0) {
				if (currentHealth <= maxHealth) {
					this.currentHealth = currentHealth;
					this.maxHealth = maxHealth;
				} else {
					this.currentHealth = maxHealth;
					this.maxHealth = maxHealth;
				}
			}
		}
		
		/**
		 * Create a new <code>Health</code> object clone, based of health.
		 * 
		 * @param health the Health to clone.
		 */
		public Health(IImmutableHealth health) {
			this(health.getCurrentHealth(), health.getMaxHealth());
		}
		
		/* (non-Javadoc)
		 * @see figureHelpers.IHealth#getCurrentHealth()
		 */
		@Override
		public int getCurrentHealth() {
			return currentHealth;
		}

		/* (non-Javadoc)
		 * @see figureHelpers.IHealth#getMaxHealth()
		 */
		@Override
		public int getMaxHealth() {
			return maxHealth;
		}
		
		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return currentHealth + "/"
					+ maxHealth;
		}


}