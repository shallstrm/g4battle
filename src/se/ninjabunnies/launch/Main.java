package se.ninjabunnies.launch;

import javax.swing.SwingUtilities;

import se.ninjabunnies.client.controller.DirectGameController;
import se.ninjabunnies.client.controller.MenuController;
import se.ninjabunnies.server.controller.GameServerController;

/**
 * The launcher which starts the appropriate controller, based on environment
 * and input arguments. Currently, only supplying none input arguments is
 * supported.
 * 
 * @author Joel Severin
 */
public class Main {
	public static void main(final String[] args) {
		// Right now we allow hostname and then port.
		if(args.length >= 2) {
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					// @todo add some basic checks...
					DirectGameController.open(args[0],
							Integer.parseInt(args[1], 10));
				}
			});
		} else if(args.length > 0) {
			// Future improvements:
			// I'm thinking of things like opening files as first arg (standard
			// in OS) and having a ninjabunnies://server.se:1337/game16 etc.
			
			// One arg means launch a local headless game server listening on
			// the port specified on the command line.
			
			System.out.println("Warning: While there is support for multiple " +
					"games in the server, the clients cannot choose which " +
					"game to connect to. Map and teams have to be choosen by " +
					"the first connecting client. Subsequent connecting " +
					"clients will be assigned to an unoccupied player slot, " +
					"or disconnected if there is none left.");
			
			// @todo add some basic checks...
			GameServerController server = new GameServerController();
			server.setPort(Integer.parseInt(args[0], 10));
			server.start();
		} else {
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					MenuController.open();
				}
			});
		}
	}
}
