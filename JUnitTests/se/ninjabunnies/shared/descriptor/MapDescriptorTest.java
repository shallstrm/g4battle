package se.ninjabunnies.shared.descriptor;

import static org.junit.Assert.*;

import org.junit.Test;

import se.ninjabunnies.shared.descriptor.FigureDescriptor;
import se.ninjabunnies.shared.descriptor.MapDescriptor;
import se.ninjabunnies.shared.descriptor.TileDescriptor;
import se.ninjabunnies.shared.descriptor.TileFigurePair;



/**
 * Test MapDescriptor.
 * @author joel
 *
 */
public class MapDescriptorTest {

	/**
	 * Test MapDescriptor construction and parsing.
	 */
	@Test
	public void testMapDescriptor() {
		TileFigurePair[][] map = new TileFigurePair[][] {
				{
					new TileFigurePair(TileDescriptor.WATER),
					new TileFigurePair(TileDescriptor.GRASS,
						FigureDescriptor.NINJA_BUNNY, 0)
				},
				{
					new TileFigurePair(TileDescriptor.WATER),
					new TileFigurePair(TileDescriptor.GRASS,
						FigureDescriptor.NINJA_BUNNY, 1)
				},
		};
		
		IMapDescriptor mapDescriptor = new MapDescriptor(map);
		
		assertTrue(mapDescriptor.getWidth() == 2);
		assertTrue(mapDescriptor.getHeight() == 2);
		assertTrue(mapDescriptor.getPlayerCount() == 2);
	}
	
	/**
	 * Make sure we get exception with different heights on different rows.
	 */
	@Test
	public void testAsymmetricMap() {
		TileFigurePair tile = new TileFigurePair(TileDescriptor.WATER);
		Boolean thrown = false;
		try {
		new MapDescriptor(new TileFigurePair[][] { {tile, tile}, {tile} });
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "map height varies on rows");
			thrown = true;
		}
		assertTrue(thrown);
	}
	
	/**
	 * Test PairTileFigure generally.
	 */
	@Test
	public void testTileFigurePair() {
		TileFigurePair tile1 = new TileFigurePair(TileDescriptor.WATER);
		TileFigurePair tile2 = new TileFigurePair(TileDescriptor.MOUNTAIN,
				FigureDescriptor.NINJA_BUNNY, 1337);
		
		assertTrue(tile1.getTileDescriptor() == TileDescriptor.WATER);
		assertTrue(tile2.getTileDescriptor() == TileDescriptor.MOUNTAIN);
		
		assertFalse(tile1.hasFigure());
		assertTrue(tile2.hasFigure());
		assertTrue(tile2.getFigureDescriptor() == FigureDescriptor.NINJA_BUNNY);
		assertTrue(tile2.getPlayerId() == 1337);
	}
	
	/**
	 * Test the TileFigurePair for null tile descriptors.
	 */
	@Test(expected = NullPointerException.class)
	public void testTileFigurePairNullTile() {
		new TileFigurePair(null);
	}
	
	/**
	 * Test the TileFigurePair for IllegalStateException non-figure getPlayerId.
	 */
	@Test(expected = IllegalStateException.class)
	public void testTileFigurePairNonFigurePlayerId() {
		TileFigurePair tile = new TileFigurePair(TileDescriptor.WATER);
		tile.getPlayerId();
	}
}