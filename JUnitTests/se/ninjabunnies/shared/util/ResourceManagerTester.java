package se.ninjabunnies.shared.util;

import java.io.IOException;

import org.junit.Test;

import se.ninjabunnies.shared.util.MapDescriptorTemplate;
import se.ninjabunnies.shared.util.ResourceManager;

public class ResourceManagerTester {

	@Test
	public void mapReaderTest() {
		ResourceManager RM = ResourceManager.getInstance();
		String mapString = null;
		try {
			mapString = RM.getMapString(RM.getMapNames()[0]);
		} catch (IOException e) {
			e.printStackTrace();
		}
		MapDescriptorTemplate MDT = MapDescriptorTemplate.stringToMapParser(mapString);
		MDT.getMapDescriptorCompatibleFormat();
	}


}
