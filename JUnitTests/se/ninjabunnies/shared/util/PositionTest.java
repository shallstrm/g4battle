package se.ninjabunnies.shared.util;

import static org.junit.Assert.*;

import org.junit.Test;

import se.ninjabunnies.shared.util.Position;

public class PositionTest {

	/**
	 * General test for <code>Position</code>.
	 */
	@Test
	public void test() {
		Position position1 = new Position(13, 37);
		Position position2 = new Position(10, 30);
		Position position3 = new Position(13, 37);
		
		assertEquals(position2.getX(), 10);
		assertEquals(position2.getY(), 30);
		
		assertEquals(position1, position1);
		assertEquals(position1, position3);
		assertFalse(position1.equals(position2));
		assertFalse(position1.equals(null));
	}
}