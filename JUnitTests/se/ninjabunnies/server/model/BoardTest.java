package se.ninjabunnies.server.model;

import static org.junit.Assert.*;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

import se.ninjabunnies.server.model.Board;
import se.ninjabunnies.server.model.Figure;
import se.ninjabunnies.server.model.IFigure;
import se.ninjabunnies.server.model.IPlayer;
import se.ninjabunnies.server.model.ITeam;
import se.ninjabunnies.server.net.NullServerChangeBus;
import se.ninjabunnies.shared.descriptor.FigureDescriptor;
import se.ninjabunnies.shared.descriptor.IMapDescriptor;
import se.ninjabunnies.shared.descriptor.TileDescriptor;
import se.ninjabunnies.shared.descriptor.TileFigurePair;
import se.ninjabunnies.shared.util.ColorGenerator.ColorDescriptor;
import se.ninjabunnies.shared.util.Position;



public class BoardTest {
	//Mock up for IMapDescriptor
	private class MockMapDes implements IMapDescriptor {

		@Override
		public int getPlayerCount() {
			return 2;
		}

		@Override
		public int getWidth() {
			return 3;
		}

		@Override
		public int getHeight() {
			return 3;
		}

		@Override
		public TileFigurePair[][] getMap() {
			return new TileFigurePair[][]{
				{new TileFigurePair(TileDescriptor.GRASS), new TileFigurePair(TileDescriptor.GRASS, FigureDescriptor.NINJA_BUNNY, 0),new TileFigurePair(TileDescriptor.GRASS)},
				{new TileFigurePair(TileDescriptor.GRASS), new TileFigurePair(TileDescriptor.GRASS), new TileFigurePair(TileDescriptor.GRASS)},
				{new TileFigurePair(TileDescriptor.GRASS), new TileFigurePair(TileDescriptor.GRASS, FigureDescriptor.NINJA_BUNNY, 1),new TileFigurePair(TileDescriptor.GRASS)}
			};
		}
		
	}
	
	//Mock up for IPlayer
	private int globalStartingPosition = -1;
	private class MockPlayer implements IPlayer {
		private final int startingPosition;
		
		public MockPlayer() {
			globalStartingPosition++;
			startingPosition = globalStartingPosition;
		}
		
		@Override
		public void addFigure(IFigure figure) {
			;
		}

		@Override
		public boolean startTurn() {
			throw new UnsupportedOperationException();
		}

		@Override
		public void changeNextActiveFigure() {
			throw new UnsupportedOperationException();
		}

		@Override
		public void changeActiveFigure(int index) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void changeActiveFigure(IFigure figure) {
			throw new UnsupportedOperationException();
		}

		@Override
		public IFigure getActiveFigure() {
			throw new UnsupportedOperationException();
		}

		@Override
		public String getName() {
			throw new UnsupportedOperationException();
		}

		@Override
		public ITeam getTeam() {
			throw new UnsupportedOperationException();
		}

		@Override
		public void setTeam(ITeam team) {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean isActive() {
			throw new UnsupportedOperationException();
		}

		@Override
		public void notifyFigureNotAlive(Figure figure) {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean isAllFiguresUsed() {
			throw new UnsupportedOperationException();
		}

		@Override
		public ColorDescriptor getColor() {
			throw new UnsupportedOperationException();
		}

		@Override
		public void endTurn() {
			throw new UnsupportedOperationException();
			
		}

		@Override
		public void setStartingPosition(int startPosition) {
		}

		@Override
		public int getStartingPosition() {
			return startingPosition;
		}
	}
	
	/**
	 * Makes correct IllegalArgumentException is thrown if players do not match the maps playerCount.
	 */
	@Test
	public void boardTest() {
		IMapDescriptor mapDes = new MockMapDes();
		List<IPlayer> list = new LinkedList<IPlayer>();
		try {
		@SuppressWarnings("unused")
		Board board = new Board(mapDes, list, new NullServerChangeBus());
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "players.size() does not match the map's player count"); 
		}
		//make sure it is not thrown otherwise
		list.add(new MockPlayer());
		list.add(new MockPlayer());
		@SuppressWarnings("unused")
		Board board = new Board(mapDes, list, new NullServerChangeBus());
	}

	/**
	 * Tests if the getTile variations return the correct tile.
	 */
	@Test
	public void getTileTest() {
		IMapDescriptor mapDes = new MockMapDes();
		List<IPlayer> list = new LinkedList<IPlayer>();
		list.add(new MockPlayer());
		list.add(new MockPlayer());
		Board board = new Board(mapDes, list, new NullServerChangeBus());
		//test getTile(Position)
		assertEquals(board.getTile(new Position(0,1)), board.getTile(new Position(0, 1)));
		//test getTile(IFigure)
		IFigure figure = board.getTile(new Position(0, 1)).getFigure();
		assertEquals(board.getTile(figure), board.getTile(new Position(0, 1)));
	}

}
