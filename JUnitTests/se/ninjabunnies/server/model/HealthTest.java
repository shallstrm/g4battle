package se.ninjabunnies.server.model;

import static org.junit.Assert.*;

import org.junit.Test;

import se.ninjabunnies.server.model.Health;
import se.ninjabunnies.shared.model.IImmutableHealth;


public class HealthTest {

	/**
	 * Tests for correct default values
	 */
	@Test
	public void testHealth() {
		IImmutableHealth health = new Health();
		assertEquals(health.getCurrentHealth(), 1);
		assertEquals(health.getMaxHealth(), 1);
	}

	/**
	 * Tests for correct startvalues
	 */
	@Test
	public void testHealthInt() {
		IImmutableHealth health = new Health(5);
		assertEquals(health.getCurrentHealth(), 5);
		assertEquals(health.getMaxHealth(), 5);
	}

	/**
	 * Tests for correct startvalues
	 */
	@Test
	public void testHealthIntInt() {
		IImmutableHealth health = new Health(5, 10);
		assertEquals(health.getCurrentHealth(), 5);
		assertEquals(health.getMaxHealth(), 10);
	}

	/**
	 * Tests if current health is set correctly
	 */
	@Test
	public void testSetCurrentHealth() {
		Health health = new Health(5);
		assertEquals(health.getCurrentHealth(), 5);
	}

	/**
	 * Tests if to string works correctly
	 */
	@Test
	public void testToString() {
		// Make sure we get no exceptions, and it isn't null.
		assertNotNull(new Health(5, 10).toString());
	}
}
