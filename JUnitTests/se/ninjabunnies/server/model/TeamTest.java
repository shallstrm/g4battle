package se.ninjabunnies.server.model;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import se.ninjabunnies.server.model.Figure;
import se.ninjabunnies.server.model.IFigure;
import se.ninjabunnies.server.model.IPlayer;
import se.ninjabunnies.server.model.ITeam;
import se.ninjabunnies.server.model.Player;
import se.ninjabunnies.server.model.Team;
import se.ninjabunnies.shared.util.ColorGenerator;
import se.ninjabunnies.shared.util.ColorGenerator.ColorDescriptor;




public class TeamTest {

	//Mock up for Player
	private class MockPlayer implements IPlayer {
		Boolean active= true;
		@Override
		public void addFigure(IFigure figure) {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean startTurn() {
			throw new UnsupportedOperationException();
		}

		@Override
		public void changeNextActiveFigure() {
			throw new UnsupportedOperationException();
		}

		@Override
		public void changeActiveFigure(int index) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void changeActiveFigure(IFigure figure) {
			throw new UnsupportedOperationException();
		}

		@Override
		public IFigure getActiveFigure() {
			throw new UnsupportedOperationException();
		}

		@Override
		public String getName() {
			throw new UnsupportedOperationException();
		}

		@Override
		public ITeam getTeam() {
			throw new UnsupportedOperationException();
		}

		@Override
		public void setTeam(ITeam team) {
			;
		}

		@Override
		public boolean isActive() {
			return active;
		}

		@Override
		public void notifyFigureNotAlive(Figure figure) {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean isAllFiguresUsed() {
			throw new UnsupportedOperationException();
		}
		
		@SuppressWarnings("unused")
		public void setActive(Boolean active) {
			this.active = active;
		}

		@Override
		public ColorDescriptor getColor() {
			return null;
		}

		@Override
		public void endTurn() {
			throw new UnsupportedOperationException();
			
		}

		@Override
		public void setStartingPosition(int startPosition) {
		}

		@Override
		public int getStartingPosition() {
			return 0;
		}
	}
	//make sure there is an exeption for creating team without players
	@Test 
	public void testTeam() {
		List<IPlayer> players = new ArrayList<IPlayer>();
		Boolean thrown = false;
		try {
			new Team("A team", players);
		} catch (IllegalArgumentException e) {
			thrown = true;
			assertEquals(e.getMessage(), "players cannot be empty");
		}
		assertTrue(thrown);
	}
	
	//Test iteration of players
	@Test
	public void testNextQueueNumberTest() {
		Player player1 = new Player("Test player 1", ColorGenerator.generateColor(1));
		Player player2 = new Player("Test player 2", ColorGenerator.generateColor(2));
		
		List<IPlayer> players = new ArrayList<IPlayer>();
		players.add(player1);
		players.add(player2);
		ITeam team = new Team("B team", players);
		
		assertEquals(team.getCurrentPlayer(), player1);
		team.nextActivePlayer();
		assertEquals(team.getCurrentPlayer(), player2);
		team.nextActivePlayer();
		assertEquals(team.getCurrentPlayer(), player1);
	}

	//Test if is Active returns correct value
	@Test
	public void isAvtiveTest() {
		IPlayer player1 = new MockPlayer();
		IPlayer player2 = new MockPlayer();
		
		List<IPlayer> players = new ArrayList<IPlayer>();
		players.add(player1);
		players.add(player2);
		ITeam team = new Team("C team", players);
		
		assertTrue(team.isActive());
		
	}
}
