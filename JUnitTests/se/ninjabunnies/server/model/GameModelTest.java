package se.ninjabunnies.server.model;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import se.ninjabunnies.server.net.IServerChangeBus;
import se.ninjabunnies.shared.changebus.IChange;
import se.ninjabunnies.shared.descriptor.IMapDescriptor;
import se.ninjabunnies.shared.descriptor.MapDescriptor;
import se.ninjabunnies.shared.util.ColorGenerator;
import se.ninjabunnies.shared.util.MapDescriptorTemplate;
import se.ninjabunnies.shared.util.ResourceManager;

public class GameModelTest {
	private class MockupBus implements IServerChangeBus {

		@Override
		public void send(IChange change) {
//			throw new UnsupportedOperationException();
			
		}

		@Override
		public void setChangeSetBarrier() {
			throw new UnsupportedOperationException();
			
		}

		@Override
		public int nextFigureId() {
			return 0;
		}
		
	}

	@Test
	public void testNextTurn() throws IOException {
		ResourceManager resourceManager = ResourceManager.getInstance();
		MapDescriptorTemplate template = MapDescriptorTemplate.stringToMapParser(resourceManager.getMapString("desert.map"));
		IMapDescriptor map = new MapDescriptor(template.getMapDescriptorCompatibleFormat());
		List<ITeam> teams = new ArrayList<ITeam>();
		for (int i = 0; map.getPlayerCount() > i; i++) {
			List<IPlayer> players = new ArrayList<IPlayer>();
			IPlayer player = new Player(" " + i,
					ColorGenerator.generateColor(i));
			player.setStartingPosition(i);
			players.add(player);
			teams.add(new Team("" + i, players));
		}
		IServerChangeBus changeBus = new MockupBus();
		GameModel model = new GameModel(map, teams, changeBus);
		assertTrue(model.getActivePlayer().equals(teams.get(0).getPlayers().get(0)));
		model.endTurn();
		assertTrue(model.getActivePlayer().equals(teams.get(1).getPlayers().get(0)));
		model.endTurn();
		assertTrue(model.getActivePlayer().equals(teams.get(2).getPlayers().get(0)));
		model.endTurn();
		assertTrue(model.getActivePlayer().equals(teams.get(3).getPlayers().get(0)));
		model.endTurn();
		assertTrue(model.getActivePlayer().equals(teams.get(0).getPlayers().get(0)));
	}
	@Test
	public void testNextFigure() throws IOException {
		ResourceManager resourceManager = ResourceManager.getInstance();
		MapDescriptorTemplate template = MapDescriptorTemplate.stringToMapParser(resourceManager.getMapString("desert.map"));
		IMapDescriptor map = new MapDescriptor(template.getMapDescriptorCompatibleFormat());
		List<ITeam> teams = new ArrayList<ITeam>();
		for (int i = 0; map.getPlayerCount() > i; i++) {
			List<IPlayer> players = new ArrayList<IPlayer>();
			IPlayer player = new Player(" " + i,
					ColorGenerator.generateColor(i));
			player.setStartingPosition(i);
			players.add(player);
			teams.add(new Team("" + i, players));
		}
		IServerChangeBus changeBus = new MockupBus();
		GameModel model = new GameModel(map, teams, changeBus);
		assertTrue(model.getActiveFigure().equals(teams.get(0).getPlayers().get(0).getActiveFigure()));
		IFigure figure = model.getActivePlayer().getActiveFigure();
		model.getActivePlayer().changeNextActiveFigure();
		assertTrue((model.getActiveFigure().equals(teams.get(0).getPlayers().get(0).getActiveFigure())) 
				 && !(model.getActiveFigure() == figure));

	}

}
