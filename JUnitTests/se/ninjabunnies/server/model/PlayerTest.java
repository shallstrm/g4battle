package se.ninjabunnies.server.model;

import static org.junit.Assert.*;

import org.junit.Test;

import se.ninjabunnies.server.model.IFigure;
import se.ninjabunnies.server.model.IPlayer;
import se.ninjabunnies.server.model.Player;
import se.ninjabunnies.shared.descriptor.FigureDescriptor;
import se.ninjabunnies.shared.model.IImmutableHealth;
import se.ninjabunnies.shared.model.IReadonlyTeam;
import se.ninjabunnies.shared.model.IReadonlyTile;
import se.ninjabunnies.shared.model.IImmutableWeapon;
import se.ninjabunnies.shared.util.ColorGenerator;
import se.ninjabunnies.shared.util.ICondition;




public class PlayerTest {

	//Mockup for IFigure
	private class MockFigure implements IFigure {

		private boolean used = false;
		private boolean alive = true;

		public void die() {
			alive = false;
		}

		@Override
		public IImmutableHealth getHealth() {
			throw new UnsupportedOperationException();
		}

		@Override
		public IImmutableWeapon getWeapon() {
			throw new UnsupportedOperationException();
		}

		@Override
		public int getArmour() {
			throw new UnsupportedOperationException();
		}

		@Override
		public int getMovement() {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean isUsed() {
			return used;
		}

		@Override
		public void setUsed() {
			used = true;
		}

		@Override
		public void setReady() {
			used = false;
		}

		@Override
		public IPlayer getPlayer() {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean isAlive() {
			return alive;
		}

		@Override
		public void attackIt(IFigure figure) {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean inTeam(IReadonlyTeam team) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void recieveAttack(IImmutableWeapon weapon) {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean canAttackBetween(IReadonlyTile first, IReadonlyTile second) {
			throw new UnsupportedOperationException();
		}

		@Override
		public int getMovePathLength(IReadonlyTile first, IReadonlyTile second) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void move(int distance) {
			throw new UnsupportedOperationException();
			
		}

		@Override
		public int distanceAbleToMove() {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean canTraverse(IReadonlyTile tile) {
			throw new UnsupportedOperationException();
		}

		@Override
		public FigureDescriptor getFigureDescriptor() {
			throw new UnsupportedOperationException();
		}

		@Override
		public ICondition<IReadonlyTile> getMoveCondition() {
			throw new UnsupportedOperationException();
		}

	}

	//Test if figures are added and the active figure can be obtainen
	@Test
	public void addFigureGetActiveFigureTest() {
		IPlayer player = new Player("Name", ColorGenerator.generateColor(1));
		assertNull(player.getActiveFigure());
		IFigure figure = new MockFigure();
		player.addFigure(figure);
		assertEquals(player.getActiveFigure(), figure);
	}

	//Tests that start Turn returns the right value and sets all figures to unused
	@Test
	public void startTurnTest() {
		IPlayer player = new Player("Name", ColorGenerator.generateColor(1));
		IFigure figure = new MockFigure();
		player.addFigure(figure);
		figure.setUsed();
		assertTrue(player.startTurn());
		assertFalse(figure.isUsed());
		MockFigure fig = (MockFigure)figure;
		fig.die();
		assertFalse(player.startTurn());
	}

	//tests if changeNextActiveFigure iterates through the figures correctly and skips unavailable figures 
	@Test
	public void changeNextActiveFigureTest() {
		IPlayer player = new Player("Name", ColorGenerator.generateColor(1));
		IFigure figure1 = new MockFigure();
		IFigure figure2 = new MockFigure();
		player.addFigure(figure1);
		player.addFigure(figure2);
		//iterates correctly?
		assertEquals(player.getActiveFigure(), figure1);
		player.changeNextActiveFigure();
		assertEquals(player.getActiveFigure(), figure2);
		player.changeNextActiveFigure();
		assertEquals(player.getActiveFigure(), figure1);
		//skips used figures?
		figure2.setUsed();
		player.changeNextActiveFigure();
		assertEquals(player.getActiveFigure(), figure1);
		//stays at figure if all are used
		figure1.setUsed();
		player.changeNextActiveFigure();
		assertEquals(player.getActiveFigure(), figure1);
		figure2.setReady();
		player.changeNextActiveFigure();
		assertEquals(player.getActiveFigure(), figure2);
		figure2.setUsed();
		player.changeNextActiveFigure();
		assertEquals(player.getActiveFigure(), figure2);
	}

	//Test if changeActiveFigure changes to the right figure throwing an exception if index is out of bounds
	@Test
	public void changeActiveFigureIndexTest() {
		IPlayer player = new Player("Name", ColorGenerator.generateColor(1));
		IFigure figure1 = new MockFigure();
		IFigure figure2 = new MockFigure();
		player.addFigure(figure1);
		player.addFigure(figure2);
		//changes to the right figure
		assertEquals(player.getActiveFigure(), figure1);
		player.changeActiveFigure(1);
		assertEquals(player.getActiveFigure(), figure2);
		player.changeActiveFigure(0);
		assertEquals(player.getActiveFigure(), figure1);
		//does not accept index that is out of bounds
		Boolean thrown = false;
		try {
			player.changeActiveFigure(2);
		} catch (IndexOutOfBoundsException e) {
			thrown = true;
			assertEquals(e.getMessage(), ("there are only figures form 0 to 1"));
		}
		assertTrue(thrown);
	}

	//Test if the method changes the active figure to the one given to it
	@Test
	public void changeActiveFigureFigureTest() {
		IPlayer player = new Player("Name", ColorGenerator.generateColor(1));
		IFigure figure1 = new MockFigure();
		IFigure figure2 = new MockFigure();
		player.addFigure(figure1);
		player.addFigure(figure2);
		assertEquals(player.getActiveFigure(), figure1);
		player.changeActiveFigure(null);
		assertEquals(player.getActiveFigure(), figure1);
		player.changeActiveFigure(figure2);
		assertEquals(player.getActiveFigure(), figure2);
	}

	//Test that correct value is returned
	@Test
	public void isAllFiguresUsedTest() {
		IPlayer player = new Player("Name", ColorGenerator.generateColor(1));
		IFigure figure = new MockFigure();
		player.addFigure(figure);
		assertFalse(player.isAllFiguresUsed());
		figure.setUsed();
		assertTrue(player.isAllFiguresUsed());
	}
	
	/** Test starting position. */
	@Test
	public void testStartingPosition() {
		// Test default
		IPlayer player = new Player("Name", ColorGenerator.generateColor(1));
		assertEquals(player.getStartingPosition(), -1);
		
		// Test set
		player.setStartingPosition(3);
		assertEquals(player.getStartingPosition(), 3);
		
		// Test illegal set (should maintain old value)
		boolean thrown = false;
		try {
			player.setStartingPosition(-1);
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "starting position set below 0");
			thrown = true;
		}
		assertTrue(thrown);
		assertEquals(player.getStartingPosition(), 3);
	}
}
