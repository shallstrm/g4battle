package se.ninjabunnies.server.model;

import static org.junit.Assert.*;

import org.junit.Test;

import se.ninjabunnies.server.model.IFigure;
import se.ninjabunnies.server.model.IPlayer;
import se.ninjabunnies.server.model.ITile;
import se.ninjabunnies.server.model.Tile;
import se.ninjabunnies.shared.descriptor.FigureDescriptor;
import se.ninjabunnies.shared.descriptor.TileDescriptor;
import se.ninjabunnies.shared.model.IImmutableHealth;
import se.ninjabunnies.shared.model.IReadonlyTeam;
import se.ninjabunnies.shared.model.IReadonlyTile;
import se.ninjabunnies.shared.model.IImmutableWeapon;
import se.ninjabunnies.shared.util.ICondition;

/**
 * Test <code>Tile</code>s.
 * 
 * @author Joel Severin
 */
public class TileTest {
	// Mock up for IFigure
	private class MockFigure implements IFigure {

		@Override
		public IImmutableHealth getHealth() {
			throw new UnsupportedOperationException();
		}

		@Override
		public IImmutableWeapon getWeapon() {
			throw new UnsupportedOperationException();
		}

		@Override
		public int getArmour() {
			throw new UnsupportedOperationException();
		}

		@Override
		public int getMovement() {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean isUsed() {
			throw new UnsupportedOperationException();
		}

		@Override
		public void setUsed() {
			throw new UnsupportedOperationException();
		}

		@Override
		public void setReady() {
			throw new UnsupportedOperationException();
		}

		@Override
		public IPlayer getPlayer() {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean isAlive() {
			throw new UnsupportedOperationException();
		}

		@Override
		public void attackIt(IFigure figure) {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean inTeam(IReadonlyTeam team) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void recieveAttack(IImmutableWeapon weapon) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void move(int distance) {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean canAttackBetween(IReadonlyTile first,
				IReadonlyTile second) {
			throw new UnsupportedOperationException();
		}

		@Override
		public int getMovePathLength(IReadonlyTile first, IReadonlyTile second) {
			throw new UnsupportedOperationException();
		}

		@Override
		public int distanceAbleToMove() {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean canTraverse(IReadonlyTile tile) {
			throw new UnsupportedOperationException();
		}

		@Override
		public FigureDescriptor getFigureDescriptor() {
			throw new UnsupportedOperationException();
		}

		@Override
		public ICondition<IReadonlyTile> getMoveCondition() {
			throw new UnsupportedOperationException();
		}

	}

	/**
	 * Tests if <code>getFigure</code> is <code>null</code> as default.
	 */
	@Test
	public void testTileFigure() {
		ITile tile = new Tile(TileDescriptor.WATER);
		assertNull(tile.getFigure());
	}

	/**
	 * Tests if <code>setFigure</code> sets the given figure correctly.
	 */
	@Test
	public void setFigureTest() {
		ITile tile = new Tile(TileDescriptor.WATER);
		IFigure figure = new MockFigure();
		assertFalse(tile.hasFigure());
		// set figure sticks
		tile.setFigure(figure);
		assertTrue(tile.hasFigure());
		assertNotNull(tile.getFigure());
		assertEquals(figure, tile.getFigure());
		// set to null sticks
		tile.setFigure(null);
		assertFalse(tile.hasFigure());
		assertNull(tile.getFigure());
	}

	/**
	 * Tests if <code>moveFigure</code> moves the figure correctly.
	 */
	@Test
	public void moveFigureToTest() {
		ITile tile1 = new Tile(TileDescriptor.WATER);
		ITile tile2 = new Tile(TileDescriptor.WATER);
		IFigure figure = new MockFigure();
		tile1.setFigure(figure);
		tile2.setFigure(null);
		tile1.moveFigureTo(tile2, 0);
		assertTrue(tile2.hasFigure());

	}

	/**
	 * Tests <code>getTileDescriptor</code>.
	 */
	@Test
	public void getTileDescriptorTest() {
		ITile tile = new Tile(TileDescriptor.WATER);
		assertNotNull(tile.getTileDescriptor());
		assertEquals(tile.getTileDescriptor(), TileDescriptor.WATER);

	}

	/**
	 * Tests <code>getNeigbors</code>.
	 */
	@Test
	public void getNeigborsTest() {
		ITile tile = new Tile(TileDescriptor.WATER);
		assertNull(tile.getNeighbors());
		
	}
	
	/**
	 * Tests <code>setNeigbors</code>.
	 */
	@SuppressWarnings("deprecation")
	@Test
	public void setNeigborsTest() {
		ITile tile1 = new Tile(TileDescriptor.WATER);
		ITile tile2 = new Tile(TileDescriptor.GRASS);
		ITile tile3 = new Tile(TileDescriptor.MUD);
		ITile[] tiles = {tile1, tile2};
		tile3.setNeighbors(tiles);
		assertEquals(tile3.getNeighbors(), tiles);
		
	}
	

	/**
	 * Tests <code>getNeigborRings</code>.
	 */
	@Test
	public void getNeigborRingsTest() {

		
	}
	
	
}
