/**
 * @author Simon Hallstr�m
 *
 */
package se.ninjabunnies.server.model;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import se.ninjabunnies.server.model.Figure;
import se.ninjabunnies.server.model.IFigure;
import se.ninjabunnies.server.model.IPlayer;
import se.ninjabunnies.server.model.ITeam;
import se.ninjabunnies.server.net.NullServerChangeBus;
import se.ninjabunnies.shared.descriptor.FigureDescriptor;
import se.ninjabunnies.shared.util.ColorGenerator.ColorDescriptor;

public class FigureTest {

	//Mock up for IPlayer
	private class MockPlayer implements IPlayer {
		private ITeam team;
		
		@Override
		public void addFigure(IFigure figure) {
			;
		}

		@Override
		public boolean startTurn() {
			throw new UnsupportedOperationException();
		}

		@Override
		public void changeNextActiveFigure() {
			throw new UnsupportedOperationException();
		}

		@Override
		public void changeActiveFigure(int index) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void changeActiveFigure(IFigure figure) {
			throw new UnsupportedOperationException();
		}

		@Override
		public IFigure getActiveFigure() {
			throw new UnsupportedOperationException();
		}

		@Override
		public String getName() {
			throw new UnsupportedOperationException();
		}

		@Override
		public ITeam getTeam() {
			return team;
		}

		@Override
		public void setTeam(ITeam team) {
			this.team = team;
		}

		@Override
		public boolean isActive() {
			throw new UnsupportedOperationException();
		}

		@Override
		public void notifyFigureNotAlive(Figure figure) {
			;
		}

		@Override
		public boolean isAllFiguresUsed() {
			throw new UnsupportedOperationException();
		}

		@Override
		public ColorDescriptor getColor() {
			throw new UnsupportedOperationException();
		}

		@Override
		public void endTurn() {
			throw new UnsupportedOperationException();
			
		}

		@Override
		public void setStartingPosition(int startPosition) {
		}

		@Override
		public int getStartingPosition() {
			return 0;
		}
	}

	//Mock up for ITeam 
	private class MockTeam implements ITeam {

		public MockTeam(ArrayList<IPlayer> playerList) {
			for(IPlayer player: playerList) {
				player.setTeam(this);
			}
		}

		@Override
		public void nextActivePlayer() {
			throw new UnsupportedOperationException();
		}

		@Override
		public IPlayer getCurrentPlayer() {
			throw new UnsupportedOperationException();
		}

		@Override
		public List<IPlayer> getPlayers() {
			throw new UnsupportedOperationException();
		}

		@Override
		public int getPlayerCount() {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean isActive() {
			throw new UnsupportedOperationException();
		}

		@Override
		public String getName() {
			throw new UnsupportedOperationException();
		}

		@Override
		public void addPlayer(IPlayer player) {
			throw new UnsupportedOperationException();
		}
	}
	
	/**
	 * Tests if the figure reduces another's health and kills the figure if it has no health left after attack.
	 */
	@Test
	public void attackTest() {
		IPlayer player1 = new MockPlayer();
		IPlayer player2 = new MockPlayer();
		Figure player1Figure1 = new Figure(FigureDescriptor.NINJA_BUNNY,
				player1, new NullServerChangeBus());
		Figure player2Figure1 = new Figure(FigureDescriptor.NINJA_BUNNY,
				player2, new NullServerChangeBus());

		player1.addFigure(player1Figure1);
		ArrayList<IPlayer>  playerList = new ArrayList<IPlayer>();
		playerList.add(player1);
		new MockTeam(playerList);

		player2.addFigure(player2Figure1);
		ArrayList<IPlayer>  playerList2 = new ArrayList<IPlayer>();
		playerList2.add(player1);
		new MockTeam(playerList2);

		assertEquals(player1Figure1.getPlayer(), player1);
		assertEquals(player2Figure1.getPlayer(), player2);
		assertEquals(player2Figure1.getHealth().getCurrentHealth(), 100);
		//Attack reduced health
		player1Figure1.setReady();
		player1Figure1.attackIt(player2Figure1);
		assertEquals(player2Figure1.getHealth().getCurrentHealth(), 66);
		//Kills if no health left after attack
		for(int i = 80; i > 0; i = i-20) {
			player1Figure1.setReady();
			player1Figure1.attackIt(player2Figure1);
		}
		assertEquals(player2Figure1.getHealth().getCurrentHealth(), 0);
		assertFalse(player2Figure1.isAlive());
	}

	/**
	 * Tests if figures isUsed status changes correctly, starting with true.
	 */
	@Test
	public void isUsedTest() {
		IPlayer player1 = new MockPlayer();
		Figure player1Figure1 = new Figure(player1);
		assertTrue(player1Figure1.isUsed());
		player1Figure1.setReady();
		assertFalse(player1Figure1.isUsed());

	}

	/**
	 * Tests if inTeam returns correct value.
	 */
	@Test
	public void inTeamTest() {
		IPlayer player1 = new MockPlayer();
		Figure player1Figure1 = new Figure(player1);
		player1.addFigure(player1Figure1);
		
		ArrayList<IPlayer> playerList = new ArrayList<IPlayer>();
		playerList.add(player1);
		ITeam team = new MockTeam(playerList);

		assertTrue(player1Figure1.inTeam(team));
	} 
	
	/**
	 * Tests if move move throws correct exception or else increases moved correctly.
	 */
	@Test
	public void moveTest() {
		IPlayer player1 = new MockPlayer();
		Figure player1Figure1 = new Figure(player1);
		player1Figure1.setReady();
		assertEquals(player1Figure1.getMovement(), 2);
		assertEquals(player1Figure1.distanceAbleToMove(), 2);
		//if value is to high
		Boolean thrown = false;
		try {
		player1Figure1.move(3);
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "distance not beween 0 and gigure.getMovement-figure.moved");
			thrown = true;
		}
		assertTrue(thrown);
		//if figure is used
		player1Figure1.setUsed();
		Boolean thrown2 = false;
		try {
		player1Figure1.move(3);
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "figure is used and can thus not move");
			thrown2 = true;
		}
		assertTrue(thrown2);
		//if value is ok
		player1Figure1.setReady();
		player1Figure1.move(1);
		assertEquals(player1Figure1.distanceAbleToMove(), 1);
	}
}

