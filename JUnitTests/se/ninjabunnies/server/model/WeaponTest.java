package se.ninjabunnies.server.model;

import static org.junit.Assert.*;

import org.junit.Test;

import se.ninjabunnies.server.model.Weapon;
import se.ninjabunnies.shared.model.IImmutableWeapon;


public class WeaponTest {
	private IImmutableWeapon weapon;
	@Test
	public void testWeapon() {
		weapon = new Weapon();
		assertTrue(weapon.getWeaponDamage() == 1);
		assertTrue(weapon.getWeaponSpeed() == 1);
		assertTrue(weapon.getWeaponRange() == 1);
		assertTrue(weapon.getWeaponName().equals("Club"));


		
	}


	@Test
	public void testWeaponStringIntIntInt() {
		weapon = new Weapon("Lolrus", 5, 10, 15);
		assertTrue(weapon.getWeaponDamage() == 5);
		assertTrue(weapon.getWeaponSpeed() == 10);
		assertTrue(weapon.getWeaponRange() == 15);
		assertTrue(weapon.getWeaponName().equals("Lolrus"));	
	}

}
